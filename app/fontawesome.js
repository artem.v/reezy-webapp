import { library } from '@fortawesome/fontawesome-svg-core'
import { faBuilding, faTimesCircle } from '@fortawesome/free-regular-svg-icons'

library.add(faBuilding, faTimesCircle)
