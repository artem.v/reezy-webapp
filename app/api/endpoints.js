import apisauce from 'apisauce'

const authToken = window.sessionStorage.getItem('authToken')

const api = apisauce.create({
  baseURL: `${process.env.API_URL || ''}/api/v1`,
  headers: {
    Authorization: authToken && `Bearer ${authToken}`,
  },
})

api.addMonitor(response => {
  if (
    response.status === 401 &&
    !/\/(session|login)$/.test(response.config.url)
  ) {
    window.location.href = '/signin'
  }
})

export const instance = api

export const signIn = data => api.post('/auth/login', data)
export const checkSession = () => api.get('/auth/session')
export const destroySession = () => api.get('/auth/logout')

export const authBankAccount = data => api.post('/plaid/auth', data)

export const createAccount = (data, headers) =>
  api.post('/auth/create-user', data, headers)

export const createProperty = (data, headers) =>
  api.post('/properties', data, headers)

export const deleteProperties = propertyId =>
  api.delete(`/properties/${propertyId}`)

export const getPropertiesList = () => api.get(`/properties`)

export const getProperty = propertyId => api.get(`/properties/${propertyId}`)

export const getPropertyTransactions = (propertyId, params) =>
  api.get(`/properties/${propertyId}/transactions`, params)

export const getPropertyTransactionsSummary = (propertyId, params) =>
  api.get(`/properties/${propertyId}/transactions/summary`, params)

export const getPropertyLeases = propertyId =>
  api.get(`/properties/${propertyId}/leases`)

export const getPropertyPaymentHistory = (propertyId, params) =>
  api.get(`/properties/${propertyId}/history`, params)

export const getTransactionSummary = () => api.get('/transactions/summary')

export const updateTransactionsProperty = ({
  categoryId,
  propertyId,
  transactionId,
}) => api.put(`/transactions/${transactionId}`, { categoryId, propertyId })

export const createLeases = (data, headers) =>
  api.post('/leases', data, headers)

export const getBankAccounts = () => api.get('/bankaccounts')
