import { injectGlobal } from 'styled-components'

/* eslint no-unused-expressions: 0 */
injectGlobal`
  html,
  body {
    height: 100%;

    width: 100%;
    font-size: 13px;
    font-weight: 300;
  }

  body {
    font-family: 'Poppins', sans-serif;
    color: #a9a5b6;
  }

  #app {
    background-color: #fff;
    min-height: 100%;
    min-width: 100%;
  }

  h2, h3 {
    font-size: 2rem;
    color: #67666e;
  }

  button:focus {
    outline: none;
  }
`
