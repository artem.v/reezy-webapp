/*
 *
 * LanguageProvider actions
 *
 */

export const CHANGE_LOCALE = 'app/LanguageToggle/CHANGE_LOCALE'
export const changeLocale = languageLocale => ({
  type: CHANGE_LOCALE,
  locale: languageLocale,
})
