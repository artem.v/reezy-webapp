/**
 *
 * DashboardPage
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { injectIntl, intlShape } from 'react-intl'
import { compose } from 'redux'
import injectSaga from 'utils/injectSaga'
import injectReducer from 'utils/injectReducer'
import { createStructuredSelector } from 'reselect'
import reducer from './reducer'
import { makeSelectSummary } from './selectors'
import saga from './saga'
import messages from './messages'
import Header from '../Header'
import { getPropertiesList, getTransactionsSummary } from './actions'
import IncomeExpensesChart from '../IncomeExpensesChart'
import ExpenseCategoriesChart from '../ExpenseCategoriesChart'
import AccountSummary from '../AccountSummary'
import Properties from '../Properties'
import injectSession from '../CheckSessionHOC'

import { Wrapper, PageTitle } from '../../components/styled'

import {
  PageHeaderWrapper,
  // Button,
  SmallContainer,
  Container,
  DashboardContent,
  ContentWrapper,
  IncomeExpensesChartWrapper,
  ChartTitle,
} from './styled'

class DashboardPage extends React.PureComponent {
  state = {
    mobileRendered: false,
  }

  componentDidMount = () => {
    this.getProperties()
    window.addEventListener('resize', this.checkCurrentWindowSize)
  }

  componentWillUnmount = () => {
    window.removeEventListener('resize', this.checkCurrentWindowSize)
  }

  getProperties = async () => {
    await Promise.all([
      this.props.getPropertiesList(),
      this.props.getTransactionsSummary(),
    ])
  }

  checkCurrentWindowSize = () => {
    if (window.innerWidth < 768 && !this.state.mobileRendered) {
      this.setState({ mobileRendered: true })
    } else if (window.innerWidth >= 768 && this.state.mobileRendered) {
      this.setState({ mobileRendered: false })
    }
  }

  renderDesktopContent = () => (
    <React.Fragment>
      <SmallContainer>
        <AccountSummary />
        <Properties />
      </SmallContainer>
      <Container>
        <IncomeExpensesChartWrapper>
          <ChartTitle>
            {this.props.intl.formatMessage(messages.incomeExpensesChartTitle)}
          </ChartTitle>
          <IncomeExpensesChart summary={this.props.summary} />
        </IncomeExpensesChartWrapper>

        <ExpenseCategoriesChart />
      </Container>
    </React.Fragment>
  )

  renderMobileContent = () => (
    <React.Fragment>
      <Container>
        <AccountSummary />
        <IncomeExpensesChartWrapper>
          <ChartTitle>
            {this.props.intl.formatMessage(messages.incomeExpensesChartTitle)}
          </ChartTitle>
          <IncomeExpensesChart summary={this.props.summary} />
        </IncomeExpensesChartWrapper>

        <Properties />
        <ExpenseCategoriesChart />
      </Container>
    </React.Fragment>
  )

  render() {
    const { intl } = this.props

    return (
      <Wrapper>
        <Header />
        <ContentWrapper>
          <PageHeaderWrapper>
            <PageTitle>{intl.formatMessage(messages.title)}</PageTitle>
            {/* <Button>{intl.formatMessage(messages.onlineHelp)}</Button> */}
          </PageHeaderWrapper>
          <DashboardContent>
            {window.innerWidth < 768
              ? this.renderMobileContent()
              : this.renderDesktopContent()}
          </DashboardContent>
        </ContentWrapper>
      </Wrapper>
    )
  }
}

DashboardPage.propTypes = {
  intl: intlShape.isRequired,
  getPropertiesList: PropTypes.func,
  getTransactionsSummary: PropTypes.func,
  summary: PropTypes.object,
}

const mapDispatchToProps = {
  getPropertiesList,
  getTransactionsSummary,
}
const mapStateToProps = createStructuredSelector({
  summary: makeSelectSummary(),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

const withReducer = injectReducer({ key: 'dashboard', reducer })
const withSaga = injectSaga({ key: 'dashboardpage', saga })
const withSession = injectSession()

export default compose(
  withReducer,
  withSaga,
  withSession,
  withConnect,
  injectIntl
)(DashboardPage)
