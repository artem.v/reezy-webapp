/*
 *
 * DashboardPage actions
 *
 */

export const DEFAULT_ACTION = 'app/DashboardPage/DEFAULT_ACTION'
export const defaultAction = () => ({
  type: DEFAULT_ACTION,
})
// Properties list
export const GET_PROPERTIES_LIST = 'app/DashboardPage/GET_PROPERTIES_LIST'
export const getPropertiesList = () => ({
  type: GET_PROPERTIES_LIST,
})

export const GET_PROPERTIES_LIST_SUCCESS =
  'app/DashboardPage/GET_PROPERTIES_LIST_SUCCESS'
export const getPropertiesListSuccess = list => ({
  type: GET_PROPERTIES_LIST_SUCCESS,
  list,
})

export const GET_PROPERTIES_LIST_FAILED =
  'app/DashboardPage/GET_PROPERTIES_LIST_FAILED'
export const getPropertiesListFailed = error => ({
  type: GET_PROPERTIES_LIST_FAILED,
  error,
})

// Transactions summary
export const GET_TRANSACTIONS_SUMMARY =
  'app/DashboardPage/GET_TRANSACTIONS_SUMMARY'
export const getTransactionsSummary = () => ({
  type: GET_TRANSACTIONS_SUMMARY,
})

export const GET_TRANSACTIONS_SUMMARY_SUCCESS =
  'app/DashboardPage/GET_TRANSACTIONS_SUMMARY_SUCCESS'
export const getTransactionsSummarySuccess = list => ({
  type: GET_TRANSACTIONS_SUMMARY_SUCCESS,
  list,
})

export const GET_TRANSACTIONS_SUMMARY_FAILED =
  'app/DashboardPage/GET_TRANSACTIONS_SUMMARY_FAILED'
export const getTransactionsSummaryFailed = error => ({
  type: GET_TRANSACTIONS_SUMMARY_FAILED,
  error,
})
