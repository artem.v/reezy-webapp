import { takeLatest, call, put, all } from 'redux-saga/effects'
import groupBy from 'lodash/groupBy'
import set from 'lodash/set'
import { push } from 'react-router-redux'
import {
  getTransactionsSummarySuccess,
  GET_TRANSACTIONS_SUMMARY,
  getPropertiesListSuccess,
  GET_PROPERTIES_LIST,
  getPropertiesListFailed,
  getTransactionsSummaryFailed,
} from './actions'
import api from '../../api'
import { CATEGORIES, EXPENSE_CATEGORIES, INCOME_CATEGORIES } from './constants'
import { TRANSACTIONS_FILTERS } from '../../constants'

const getData = transactions => {
  let income = 0
  let expenses = 0
  let cashFlow = 0

  transactions.forEach(transaction => {
    if (transaction.categoryId === CATEGORIES.RENT) {
      income += transaction.amount
    } else {
      expenses += transaction.amount
    }
  })
  cashFlow = income - expenses
  return {
    income,
    expenses,
    cashFlow,
  }
}

const summaryData = transactions => {
  const income = {
    total: 0,
    data: [],
  }
  const expenses = {
    total: 0,
    data: [],
  }
  let cashFlow = 0

  let mortgage = 0
  let repair = 0
  let propertyTax = 0
  let maintenanceFees = 0

  const incomeTransactions = transactions.filter(transaction =>
    INCOME_CATEGORIES.includes(transaction.categoryId)
  )

  const incomeByMonth = groupBy(
    incomeTransactions,
    transaction => transaction.utcDate
  )

  income.data = Object.keys(incomeByMonth).map(key => {
    const utcDate = parseInt(key, 10)
    const total = incomeByMonth[utcDate].reduce(
      (sum, incomeTransaction) => sum + incomeTransaction.total,
      0
    )

    income.total += total
    return [
      utcDate,
      total,
      {
        y: total,
        date: utcDate,
        diff: total,
      },
    ]
  })

  const expensesTransactions = transactions
    .filter(transaction => EXPENSE_CATEGORIES.includes(transaction.categoryId))
    .map(transaction => set(transaction, 'total', transaction.total * -1))

  const expensesByMonth = groupBy(
    expensesTransactions,
    transaction => transaction.utcDate
  )

  expenses.data = Object.keys(expensesByMonth).map(key => {
    const utcDate = parseInt(key, 10)
    const total = expensesByMonth[utcDate].reduce(
      (sum, expenseTransaction) => sum + expenseTransaction.total,
      0
    )

    expenses.total += total
    return [
      utcDate,
      total,
      {
        y: total,
        date: utcDate,
        diff: total,
      },
    ]
  })

  transactions.forEach(transaction => {
    if (transaction.categoryId === CATEGORIES.MORTGAGE) {
      mortgage += transaction.total
    } else if (transaction.categoryId === CATEGORIES.REPAIR) {
      repair += transaction.total
    } else if (transaction.categoryId === CATEGORIES.PROPERTY_TAX) {
      propertyTax += transaction.total
    } else if (transaction.categoryId === CATEGORIES.MAINTENANCE_FEES) {
      maintenanceFees += transaction.total
    }
  })

  cashFlow = income.total - expenses.total

  return {
    income,
    expenses,
    cashFlow,
    mortgage,
    repair,
    propertyTax,
    maintenanceFees,
  }
}

function* getPropertiesList() {
  try {
    const response = yield call(api.getPropertiesList)

    if (response.ok) {
      if (!response.data.response.length) {
        yield put(push('/signup'))
      }

      const propertyValues = yield Promise.all(
        response.data.response.map(async property => {
          const propertyTransactionResponse = await api.getPropertyTransactions(
            property.id,
            {
              filter: TRANSACTIONS_FILTERS.RECOGNIZED.toLowerCase(),
            }
          )

          if (!propertyTransactionResponse.ok) {
            return property
          }

          const values = getData(propertyTransactionResponse.data.transactions)

          return { ...values, ...property }
        })
      )

      yield put(getPropertiesListSuccess(propertyValues))
    }
  } catch (error) {
    yield put(getPropertiesListFailed(error))
  }
}

function* getTransactionsSummary() {
  try {
    const response = yield call(api.getTransactionSummary)

    if (response.ok) {
      const summary = summaryData(response.data.response)

      yield put(getTransactionsSummarySuccess(summary))
    }
  } catch (error) {
    yield put(getTransactionsSummaryFailed(error))
  }
}

export default function* defaultSaga() {
  yield all([
    yield takeLatest(GET_PROPERTIES_LIST, getPropertiesList),
    yield takeLatest(GET_TRANSACTIONS_SUMMARY, getTransactionsSummary),
  ])
}
