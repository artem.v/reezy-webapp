/*
 * SignInPage Messages
 *
 * This contains all the text for the SignInPage component.
 */

import { defineMessages } from 'react-intl'
import { createIntlMessage } from 'common'

const PAGE_ID = 'DashboardPage'

export default defineMessages({
  ...createIntlMessage(PAGE_ID, 'title'),
  ...createIntlMessage(PAGE_ID, 'incomeExpensesChartTitle'),
  ...createIntlMessage(PAGE_ID, 'accountSummary'),
  ...createIntlMessage(PAGE_ID, 'properties'),
  ...createIntlMessage(PAGE_ID, 'onlineHelp'),
  ...createIntlMessage(PAGE_ID, 'showMore'),
  ...createIntlMessage(PAGE_ID, 'addProperty'),
  ...createIntlMessage(PAGE_ID, 'ytdIncome'),
  ...createIntlMessage(PAGE_ID, 'ytdCashFlow'),
})
