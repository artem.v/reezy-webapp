/*
 *
 * DashboardPage reducer
 *
 */

import Immutable from 'seamless-immutable'
import {
  DEFAULT_ACTION,
  GET_TRANSACTIONS_SUMMARY_SUCCESS,
  GET_PROPERTIES_LIST_SUCCESS,
} from './actions'
import { SESSION_DESTROY_SUCCESS } from '../App/actions'

export const initialState = Immutable({
  properties: [],
  summary: {
    income: {},
    expenses: {},
    categories: [],
  },
})

function dashboardPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state
    case GET_PROPERTIES_LIST_SUCCESS:
      return state.set('properties', action.list)
    case GET_TRANSACTIONS_SUMMARY_SUCCESS:
      return state.set('summary', action.list)
    case SESSION_DESTROY_SUCCESS:
      return state
    default:
      return state
  }
}

export default dashboardPageReducer
