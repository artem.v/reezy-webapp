import { createSelector } from 'reselect'
import { initialState } from './reducer'

/**
 * Direct selector to the dashboardPage state domain
 */

const selectDashboardPageDomain = state => state.dashboard || initialState

/**
 * Other specific selectors
 */

const makeSelectProperties = () =>
  createSelector(
    selectDashboardPageDomain,
    dashboardPage => dashboardPage.properties
  )
const makeSelectSummary = () =>
  createSelector(
    selectDashboardPageDomain,
    dashboardPage => dashboardPage.summary
  )

// FIXME , get normal select
const makeSelectSignupError = () =>
  createSelector(
    selectDashboardPageDomain,
    dashboardPage => dashboardPage.error
  )
/**
 * Default selector used by DashboardPage
 */

const makeSelectDashboardPage = () =>
  createSelector(selectDashboardPageDomain, substate => substate)

export default makeSelectDashboardPage
export { selectDashboardPageDomain }

export { makeSelectProperties, makeSelectSummary, makeSelectSignupError }
