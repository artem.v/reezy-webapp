import Immutable from 'seamless-immutable'
import dashboardPageReducer from '../reducer'

describe('dashboardPageReducer', () => {
  it('returns the initial state', () => {
    expect(dashboardPageReducer(undefined, {})).toEqual(Immutable({}))
  })
})
