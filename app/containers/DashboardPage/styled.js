import styled from 'styled-components'
import { COLORS } from '../../constants'
import { Content } from '../../components/styled'

export const DashboardContent = styled(Content)`
  flex-direction: row;
`

export const ContentWrapper = styled.div`
  display: flex;
  flex: 1;
  background-color: ${COLORS.BACKGROUND};
  width: 100%;
  flex-direction: column;
`

export const PageHeaderWrapper = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 30px auto;
  background-color: ${COLORS.BACKGROUND};
  max-width: 1270px;
  width: 100%;
  padding: 0 40px;
  @media (max-width: 767px) {
    padding: 0 18px;
  }
`
export const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`
export const SmallContainer = styled(Container)`
  max-width: 370px;
  margin-right: 30px;
`

export const Button = styled.button`
  border: none;
  background-color: ${COLORS.PRIMARY};
  padding: 8px 24px;
  font-size: 18px;
  line-height: 23px;
  font-weight: 600;
  color: ${COLORS.WHITE};
  border-radius: 2px;
  @media (max-width: 767px) {
    display: none;
  }
`

export const IncomeExpensesChartWrapper = styled.div`
  padding: 40px 10px 30px 10px;
  background-color: ${COLORS.WHITE};
  box-shadow: 2px 2px 7px 0 rgba(8, 39, 64, 0.1);
  max-width: 800px;
  margin-bottom: 30px;
  border-radius: 2px;
  @media (max-width: 767px) {
    padding: 15px 0;
  }
`

export const ChartTitle = styled.h2`
  margin: 0 0 0 10px;
  font-size: 18px;
  line-height: 23px;
  color: ${COLORS.TITLE};
  font-weight: 600;
`
