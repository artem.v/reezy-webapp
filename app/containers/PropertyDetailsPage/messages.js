/*
 * PropertyDetailsPage Messages
 *
 * This contains all the text for the Properties component.
 */

import { defineMessages } from 'react-intl'
import { createIntlMessage } from 'common'

const PAGE_ID = 'propertyDetailsPage'

export default defineMessages({
  ...createIntlMessage(PAGE_ID, 'rentHistory'),
  ...createIntlMessage(PAGE_ID, 'rentDetails'),
  ...createIntlMessage(PAGE_ID, 'expenses'),
  ...createIntlMessage(PAGE_ID, 'date'),
  ...createIntlMessage(PAGE_ID, 'description'),
  ...createIntlMessage(PAGE_ID, 'amount'),
  ...createIntlMessage(PAGE_ID, 'transactions'),
  ...createIntlMessage(PAGE_ID, 'income'),
  ...createIntlMessage(PAGE_ID, 'expense'),
  ...createIntlMessage(PAGE_ID, 'propertyTaxes'),
  ...createIntlMessage(PAGE_ID, 'repair'),
  ...createIntlMessage(PAGE_ID, 'maintenanceFees'),
  ...createIntlMessage(PAGE_ID, 'mortgage'),
  ...createIntlMessage(PAGE_ID, 'noTransactions'),
  ...createIntlMessage(PAGE_ID, 'dueDate'),
  ...createIntlMessage(PAGE_ID, 'nextPayment'),
  ...createIntlMessage(PAGE_ID, 'backToProperty'),
  ...createIntlMessage(PAGE_ID, 'billingCycleDaily'),
  ...createIntlMessage(PAGE_ID, 'billingCycleWeekly'),
  ...createIntlMessage(PAGE_ID, 'billingCycleBiWeekly'),
  ...createIntlMessage(PAGE_ID, 'billingCycleMonthly'),
})
