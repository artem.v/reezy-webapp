import { createSelector } from 'reselect'
import { initialState } from './reducer'

/**
 * Direct selector to the PropertyDetailsPage state domain
 */

const selectPropertyDetailsPageDomain = state =>
  state.propertyDetailsPage || initialState

const makeSelectSelectedCategory = () =>
  createSelector(
    selectPropertyDetailsPageDomain,
    state => state.selectedCategory
  )

const makeSelectTransactions = () =>
  createSelector(selectPropertyDetailsPageDomain, state => state.transactions)

const makeSelectTransactionsPending = () =>
  createSelector(
    selectPropertyDetailsPageDomain,
    state => state.transactionsPending
  )

const makeSelectTransactionsDateStart = () =>
  createSelector(
    selectPropertyDetailsPageDomain,
    state => state.transactionsDateStart
  )

const makeSelectTransactionsDateEnd = () =>
  createSelector(
    selectPropertyDetailsPageDomain,
    state => state.transactionsDateEnd
  )

const makeSelectSummary = () =>
  createSelector(selectPropertyDetailsPageDomain, state => state.summary)

const makeSelectProperty = () =>
  createSelector(selectPropertyDetailsPageDomain, state => state.property)

const makeSelectPropertyLease = () =>
  createSelector(selectPropertyDetailsPageDomain, state => state.propertyLease)

const makeSelectPropertyDateStart = () =>
  createSelector(
    selectPropertyDetailsPageDomain,
    state => state.propertyDateStart
  )

const makeSelectPropertyDateEnd = () =>
  createSelector(
    selectPropertyDetailsPageDomain,
    state => state.propertyDateEnd
  )

const makeSelectPaymentHistory = () =>
  createSelector(selectPropertyDetailsPageDomain, state => state.paymentHistory)

export {
  makeSelectSelectedCategory,
  makeSelectTransactions,
  makeSelectTransactionsPending,
  makeSelectTransactionsDateStart,
  makeSelectTransactionsDateEnd,
  makeSelectSummary,
  makeSelectProperty,
  makeSelectPropertyLease,
  makeSelectPropertyDateStart,
  makeSelectPropertyDateEnd,
  makeSelectPaymentHistory,
}
