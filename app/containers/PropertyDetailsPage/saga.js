import { takeLatest, all, call, put, select } from 'redux-saga/effects'
import groupBy from 'lodash/groupBy'
import set from 'lodash/set'
import moment from 'moment'
import { API_DATE_FORMAT, DATE_FORMAT } from '../../constants'
import { EXPENSE_CATEGORIES, INCOME_CATEGORIES } from './constants'
import { initialState } from './reducer'
import {
  FETCH_TRANSACTIONS_SUMMARY,
  FETCH_PROPERTY_EXPENSES,
  fetchTransactionsSummarySuccess,
  fetchTransactionsSummaryFailed,
  SELECT_SHOW_CATEGORY,
  CHANGE_TRANSACTIONS_START_DATE,
  CHANGE_TRANSACTIONS_END_DATE,
  CHANGE_PROPERTY_END_DATE,
  CHANGE_PROPERTY_START_DATE,
  FETCH_PROPERTY,
  fetchPropertySuccess,
  fetchPropertyFailed,
  FETCH_PROPERTY_LEASE,
  fetchPropertyLeaseSuccess,
  fetchPropertyLeaseFailed,
  fetchPropertyExpensesFailed,
  fetchPropertyExpensesSuccess,
  fetchPaymentHistorySuccess,
  fetchPaymentHistoryFailed,
  FETCH_PAYMENT_HISTORY,
} from './actions'
import api from '../../api'
const selectPropertyPageDomain = state => state.propertyPage || initialState

const summaryData = transactions => {
  const income = {
    total: 0,
    data: [],
  }
  const expenses = {
    total: 0,
    data: [],
  }

  const incomeTransactions = transactions.filter(transaction =>
    INCOME_CATEGORIES.includes(transaction.categoryId)
  )

  const incomeByMonth = groupBy(
    incomeTransactions,
    transaction => transaction.utcDate
  )

  income.data = Object.keys(incomeByMonth).map(key => {
    const utcDate = parseInt(key, 10)

    const total = incomeByMonth[key].reduce(
      (sum, incomeTransaction) => sum + incomeTransaction.total,
      0
    )

    income.total += total
    return [
      utcDate,
      total,
      {
        y: total,
        date: utcDate,
        diff: total,
      },
    ]
  })

  const expensesTransactions = transactions
    .filter(transaction => EXPENSE_CATEGORIES.includes(transaction.categoryId))
    .map(transaction => set(transaction, 'total', transaction.total * -1))

  const expensesByMonth = groupBy(
    expensesTransactions,
    transaction => transaction.utcDate
  )

  expenses.data = Object.keys(expensesByMonth).map(key => {
    const utcDate = parseInt(key, 10)

    const total = expensesByMonth[key].reduce(
      (sum, expenseTransaction) => sum + expenseTransaction.total,
      0
    )

    expenses.total += total
    return [
      utcDate,
      total,
      {
        y: total,
        date: utcDate,
        diff: total,
      },
    ]
  })
  return {
    income,
    expenses,
  }
}

function* fetchTransactionsSummary(action) {
  try {
    const { propertyDateStart, propertyDateEnd, property } = yield select(
      state => state.propertyDetailsPage
    )
    const propertyId = action.id || property.id

    const queryKeys = {
      start_date: moment(propertyDateStart, DATE_FORMAT).format(
        API_DATE_FORMAT
      ),
      end_date: moment(propertyDateEnd, DATE_FORMAT).format(API_DATE_FORMAT),
    }
    const query = {}

    Object.entries(queryKeys)
      .filter(([, value]) => value)
      .forEach(([key, value]) => {
        query[key] = value
      })

    const response = yield call(
      api.getPropertyTransactionsSummary,
      propertyId,
      query
    )

    const summary = summaryData(response.data.response)

    if (response.ok) {
      yield put(fetchTransactionsSummarySuccess(summary))
    } else {
      yield put(fetchTransactionsSummaryFailed(response.problem))
    }
  } catch (error) {
    yield put(fetchTransactionsSummaryFailed(error))
  }
}

function* fetchPropertyExpenses(action) {
  try {
    const {
      selectedCategory,
      property,
      transactionsDateStart,
      transactionsDateEnd,
    } = yield select(state => state.propertyDetailsPage)

    const propertyId = action.id || property.id

    const queryKeys = {
      filter: selectedCategory,
      start_date: moment(transactionsDateStart, DATE_FORMAT).format(
        API_DATE_FORMAT
      ),
      end_date: moment(transactionsDateEnd, DATE_FORMAT).format(
        API_DATE_FORMAT
      ),
    }
    const query = {}

    Object.entries(queryKeys)
      .filter(([, value]) => value)
      .forEach(([key, value]) => {
        query[key] = value
      })
    const response = yield call(api.getPropertyTransactions, propertyId, query)

    if (response.ok) {
      yield put(fetchPropertyExpensesSuccess(response.data.transactions))
    } else {
      yield put(fetchPropertyExpensesFailed(response.problem))
    }
  } catch (error) {
    yield put(fetchPropertyExpensesFailed(error))
  }
}

function* fetchProperty({ id }) {
  try {
    const response = yield call(api.getProperty, id)

    if (response.ok) {
      yield put(fetchPropertySuccess(response.data.response))
    } else {
      yield put(fetchPropertyFailed(response.problem))
    }
  } catch (error) {
    yield put(fetchPropertyFailed(error))
  }
}

function* fetchPropertyLease({ id }) {
  try {
    const response = yield call(api.getPropertyLeases, id)

    const propertyLeases =
      response.data.response[response.data.response.length - 1] || {}

    if (response.ok) {
      yield put(fetchPropertyLeaseSuccess(propertyLeases))
    } else {
      yield put(fetchPropertyLeaseFailed(response.problem))
    }
  } catch (error) {
    yield put(fetchPropertyLeaseFailed(error))
  }
}

function* fetchPaymentHistory({ id }) {
  try {
    const state = yield select(selectPropertyPageDomain)
    const { propertyDateStart, propertyDateEnd, property } = state

    const propertyId = id || property.id

    const queryKeys = {
      start_date: moment(propertyDateStart, DATE_FORMAT).format(
        API_DATE_FORMAT
      ),
      end_date: moment(propertyDateEnd, DATE_FORMAT).format(API_DATE_FORMAT),
    }
    const query = {}

    Object.entries(queryKeys)
      .filter(([, value]) => value)
      .forEach(([key, value]) => {
        query[key] = value
      })

    const response = yield call(api.getPropertyPaymentHistory, propertyId)

    if (response.ok) {
      const paymentHistory = {}

      response.data.response.rent.forEach(history => {
        const year = moment(history.date, 'YYYY-MM').format('YYYY')
        const month = moment(history.date, 'YYYY-MM').format('M')

        if (!paymentHistory[year]) {
          paymentHistory[year] = {}
        }

        paymentHistory[year][month] = history.paid
      })

      yield put(fetchPaymentHistorySuccess(paymentHistory))
    } else {
      yield put(fetchPaymentHistoryFailed(response.problem))
    }
  } catch (error) {
    yield put(fetchPaymentHistoryFailed(error))
  }
}
export default function* defaultSaga() {
  yield all([
    yield takeLatest(FETCH_PROPERTY, fetchProperty),
    yield takeLatest(FETCH_PROPERTY_LEASE, fetchPropertyLease),
    yield takeLatest(
      [
        FETCH_PROPERTY_EXPENSES,
        SELECT_SHOW_CATEGORY,
        CHANGE_TRANSACTIONS_START_DATE,
        CHANGE_TRANSACTIONS_END_DATE,
      ],
      fetchPropertyExpenses
    ),
    yield takeLatest(
      [
        FETCH_TRANSACTIONS_SUMMARY,
        CHANGE_PROPERTY_START_DATE,
        CHANGE_PROPERTY_END_DATE,
      ],
      fetchTransactionsSummary
    ),
    yield takeLatest(FETCH_PAYMENT_HISTORY, fetchPaymentHistory),
  ])
}
