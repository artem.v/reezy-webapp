import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { COLORS } from '../../constants'

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  background-color: ${COLORS.BACKGROUND};
  font-family: PT Sans, sans-serif;
  min-height: 100vh;
`
export const Content = styled.div`
  padding: 0 40px 20px;
  max-width: 1270px;
  margin: 30px auto 0;
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  @media (max-width: 767px) {
    padding: 0 18px 20px;
  }
`
export const PageHeader = styled.header`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  margin: 40px 0;
  @media (max-width: 767px) {
    margin: 0 0 22px;
    flex-direction: column;
    align-items: flex-start;
  }
`
//

export const BackButton = styled(Link)`
  color: ${COLORS.PRIMARY};
  display: flex;
  align-items: center;
  font-weight: 600;
  font-size: 18px;
  cursor: pointer;
  line-height: 21px;
  &:hover {
    text-decoration: none;
    color: ${COLORS.PRIMARY};
  }
`

export const ChartHeader = styled.header`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 30px;
`

export const ChartLabel = styled.h1`
  margin: 0;
  padding: 0;
  font-size: 24px;
  line-height: 29px;
  color: ${COLORS.TITLE};
  font-weight: 600;
`

export const Paper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 40px 20px 30px;
  flex-grow: 1;
  background-color: ${COLORS.WHITE};
  box-shadow: 2px 2px 9px 0 rgba(95, 95, 95, 0.2);
  margin-bottom: 30px;
  @media (max-width: 991px) {
    padding: 30px 10px 20px;
    width: 100%;
    margin-bottom: 20px;
  }

  @media (max-width: 767px) {
    padding: 15px;
  }
`

export const HistoryPaper = styled(Paper)`
  width: 100%;
`

export const DetailsPaper = styled(Paper)`
  flex-grow: 2;
  max-width: 480px;
  min-width: 480px;
  margin-left: 30px;
  @media (max-width: 1228px) {
    min-width: initial;

    max-width: initial;
    margin-left: 0;
  }
`

export const Row = styled.div`
  display: flex;
  justify-content: space-between;
  @media (max-width: 1228px) {
    flex-wrap: wrap;
  }
`

export const HideSm = styled.div`
  @media (max-width: 767px) {
    display: none;
  }
`

export const Label = styled.h3`
  margin: 0 0 20px 0;
  font-size: 18px;
  line-height: 22px;
  font-weight: 600;
  color: ${COLORS.TITLE};
`

export const LightBlock = styled.div`
  display: flex;
  flex-direction: column;
  background-color: ${COLORS.LIGHT};
  padding: 17px;
  @media (max-width: 767px) {
    padding: 10px;
  }
`

export const DetailsRow = styled(Row)`
  justify-content: flex-start;
  align-items: center;
  margin-bottom: 10px;
  flex-wrap: nowrap;
`

export const DetailsText = styled.div`
  display: inline-flex;
  color: ${COLORS.TITLE};
  font-size: 15px;
  line-height: 18px;
  @media (max-width: 767px) {
    font-size: 12px;
    line-height: 15px;
  }
`
export const DetailsIcon = styled.img`
  width: 24px;
  min-width: 24px;
  height: 24px;
  margin-right: 10px;
`

export const HistoryMonth = styled(Row)`
  justify-content: flex-start;
  margin-bottom: 10px;
  padding-left: 60px;
  position: relative;
  align-items: flex-end;
  @media (max-width: 767px) {
    padding-left: 40px;
    justify-content: flex-start;
  }
`

export const HistoryEmptyMonth = styled.div`
  width: 44px;
`

export const MonthLabel = styled.span`
  color: ${COLORS.TEXT};
  font-size: 12px;
  display: flex;
  justify-content: center;
  line-height: 18px;
  width: 24px;
  overflow: hidden;
  margin-right: 20px;
  @media (max-width: 767px) {
    transform: rotate(-90deg);
    line-height: 16px;
    font-size: 13px;
    margin-right: 0;
    width: 23px;
    text-align: left;
    margin-bottom: 5px;
  }
`

export const YearLabel = styled.span`
  line-height: 18px;
  font-size: 16px;
  font-weight: 600;
  color: ${COLORS.TITLE};
  position: absolute;
  left: 0;
  @media (max-width: 767px) {
    line-height: 17px;
    font-size: 14px;
    justify-content: flex-start;
  }
`

export const DetailsPrice = styled.span`
  color: ${COLORS.SUCCESS};
  font-size: 18px;
  font-weight: 600;
  @media (max-width: 767px) {
    font-size: 15px;
  }
`

export const DetailsDate = styled(DetailsPrice)`
  color: ${COLORS.PRIMARY};
`
export const BackArrow = styled.img`
  margin-right: 3px;
`
export const DateColumn = styled.div`
  display: flex;
  flex-direction: column;
`
