import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { injectIntl, intlShape, FormattedMessage } from 'react-intl'
import camelCase from 'lodash/camelCase'
import injectSaga from 'utils/injectSaga'
import injectReducer from 'utils/injectReducer'
import formatter from 'utils/formatter'
import moment from 'moment'

import reducer from './reducer'
import saga from './saga'

import { BILLING_CYCLES } from '../../constants'
import { calculateNextPaymentDue } from '../../common'

import injectSession from '../CheckSessionHOC'

import Header from '../Header'
import PropertyTransactions from '../PropertyTransactions'
import IncomeExpensesChart from '../IncomeExpensesChart'
import PropertyPaymentHistory from '../PropertyPaymentHistory'
import DateTimePicker from '../../components/TimePicker'

import {
  changePropertyStartDate,
  changePropertyEndDate,
  fetchTransactionsSummary,
  fetchPropertyExpenses,
  fetchPropertyLease,
  clearData,
  fetchProperty,
  fetchPaymentHistory,
} from './actions'

import {
  makeSelectSummary,
  makeSelectProperty,
  makeSelectPropertyLease,
  makeSelectPropertyDateStart,
  makeSelectPropertyDateEnd,
} from './selectors'

import BackArrowIcon from '../../images/backArrow.svg'
import CuponIcon from '../../images/cupon.svg'
import ClockIcon from '../../images/clock.svg'

import { Wrapper, Content, PageHeader } from '../../components/styled'

import {
  BackButton,
  Row,
  Paper,
  HistoryPaper,
  DetailsPaper,
  Label,
  ChartLabel,
  ChartHeader,
  LightBlock,
  DetailsText,
  DetailsRow,
  DetailsIcon,
  DetailsPrice,
  DetailsDate,
  HideSm,
  BackArrow,
} from './styled'

import messages from './messages'

export class PropertyDetailsPage extends PureComponent {
  static propTypes = {
    intl: intlShape.isRequired,
    summary: PropTypes.object,
    property: PropTypes.object,
    propertyLease: PropTypes.object,
    dateStart: PropTypes.string,
    dateEnd: PropTypes.string,
    changePropertyStartDate: PropTypes.func,
    changePropertyEndDate: PropTypes.func,
    match: PropTypes.shape({
      params: PropTypes.object,
    }),
    fetchTransactionsSummary: PropTypes.func,
    clearData: PropTypes.func,
    fetchProperty: PropTypes.func,
    fetchPropertyLease: PropTypes.func,
    fetchPropertyExpenses: PropTypes.func,
    fetchPaymentHistory: PropTypes.func,
  }

  componentDidMount = () => {
    this.getInitialData()
  }

  getInitialData = () => {
    const { id } = this.props.match.params

    this.props.fetchProperty(id)
    this.props.fetchPropertyLease(id)
    this.props.fetchTransactionsSummary(id)
    this.props.fetchPaymentHistory(id)
    this.props.fetchPropertyExpenses(id)
  }

  componentWillUnmount = () => {
    this.props.clearData()
  }

  changeStartDate = date => {
    this.props.changePropertyStartDate(date)
  }

  changeEndDate = date => {
    this.props.changePropertyEndDate(date)
  }

  renderDueDate = () => {
    const { propertyLease } = this.props

    if (propertyLease.id) {
      const price = `$${formatter.separatePrice(propertyLease.rent_amount)}`
      const period = this.props.intl.formatMessage(
        messages[
          camelCase(
            `billingCycle${BILLING_CYCLES[propertyLease.billing_cycle]}`
          )
        ]
      )

      const rentDue = calculateNextPaymentDue(
        propertyLease.start_date,
        propertyLease.billing_cycle
      )

      if (!rentDue) {
        return null
      }

      const day = rentDue.diff(moment(), 'day')

      return (
        <React.Fragment>
          <DetailsRow>
            <DetailsIcon src={CuponIcon} />
            <DetailsText>
              <FormattedMessage
                {...messages.dueDate}
                values={{
                  price: <DetailsPrice>{price}</DetailsPrice>,
                  period,
                  date: (
                    <strong>
                      {new Date(propertyLease.start_date).getDate()}
                    </strong>
                  ),
                }}
              />
            </DetailsText>
          </DetailsRow>
          <DetailsRow>
            <DetailsIcon src={ClockIcon} />
            <DetailsText>
              <FormattedMessage
                {...messages.nextPayment}
                values={{
                  day: <DetailsDate>{day}</DetailsDate>,
                }}
              />
            </DetailsText>
          </DetailsRow>
        </React.Fragment>
      )
    }

    return null
  }

  render() {
    if (!this.props.property || !this.props.propertyLease) {
      return null
    }

    return (
      <Wrapper>
        <Header />
        <Content>
          <PageHeader>
            <BackButton to="/properties">
              <BackArrow src={BackArrowIcon} alt="" />
              {this.props.intl.formatMessage(messages.backToProperty)}
            </BackButton>
          </PageHeader>

          <Paper>
            <ChartHeader>
              <ChartLabel>{this.props.property.address}</ChartLabel>
              <HideSm>
                <DateTimePicker
                  dateStart={this.props.dateStart}
                  dateEnd={this.props.dateEnd}
                  onStartDateChange={this.changeStartDate}
                  onEndDateChange={this.changeEndDate}
                />
              </HideSm>
            </ChartHeader>
            <IncomeExpensesChart summary={this.props.summary} />
          </Paper>
          <Row>
            <HistoryPaper>
              <Label>
                {this.props.intl.formatMessage(messages.rentHistory)}
              </Label>
              <PropertyPaymentHistory />
            </HistoryPaper>
            <DetailsPaper>
              <Label>
                {this.props.intl.formatMessage(messages.rentDetails)}
              </Label>
              <LightBlock>{this.renderDueDate()}</LightBlock>
            </DetailsPaper>
          </Row>
          <Paper>
            <Label>{this.props.intl.formatMessage(messages.expenses)}</Label>
            <PropertyTransactions />
          </Paper>
        </Content>
      </Wrapper>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  summary: makeSelectSummary(),
  property: makeSelectProperty(),
  propertyLease: makeSelectPropertyLease(),
  dateStart: makeSelectPropertyDateStart(),
  dateEnd: makeSelectPropertyDateEnd(),
})

const mapDispatchToProps = {
  changePropertyStartDate,
  changePropertyEndDate,
  fetchTransactionsSummary,
  clearData,
  fetchProperty,
  fetchPropertyLease,
  fetchPropertyExpenses,
  fetchPaymentHistory,
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

const withReducer = injectReducer({ key: 'propertyDetailsPage', reducer })
const withSaga = injectSaga({ key: 'propertyDetailsPage', saga })
const withSession = injectSession()

export default compose(
  withConnect,
  withSaga,
  withReducer,
  withSession,
  injectIntl
  // withRouter
)(PropertyDetailsPage)
