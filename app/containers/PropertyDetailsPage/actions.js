/*
 *
 * PropertyDetailsPage actions
 *
 */

export const CHANGE_TRANSACTIONS_START_DATE =
  'app/PropertyDetailsPage/CHANGE_START_DATE'
export const changeTransactionsStartDate = date => ({
  type: CHANGE_TRANSACTIONS_START_DATE,
  date,
})

export const CHANGE_TRANSACTIONS_END_DATE =
  'app/PropertyDetailsPage/CHANGE_END_DATE'
export const changeTransactionsEndDate = date => ({
  type: CHANGE_TRANSACTIONS_END_DATE,
  date,
})

export const CHANGE_PROPERTY_START_DATE =
  'app/PropertyDetailsPage/CHANGE_PROPERTY_START_DATE'
export const changePropertyStartDate = date => ({
  type: CHANGE_PROPERTY_START_DATE,
  date,
})

export const CHANGE_PROPERTY_END_DATE =
  'app/PropertyDetailsPage/CHANGE_PROPERTY_END_DATE'
export const changePropertyEndDate = date => ({
  type: CHANGE_PROPERTY_END_DATE,
  date,
})

export const SELECT_SHOW_CATEGORY =
  'app/PropertyDetailsPage/SELECT_SHOW_CATEGORY'
export const selectShowCategory = categoryId => ({
  type: SELECT_SHOW_CATEGORY,
  categoryId,
})
// fetch transaction list
export const FETCH_PROPERTY_EXPENSES =
  'app/PropertyDetailsPage/FETCH_PROPERTY_EXPENSES'
export const fetchPropertyExpenses = id => ({
  type: FETCH_PROPERTY_EXPENSES,
  id,
})

export const FETCH_PROPERTY_EXPENSES_SUCCESS =
  'app/PropertyDetailsPage/FETCH_PROPERTY_EXPENSES_SUCCESS'
export const fetchPropertyExpensesSuccess = transactions => ({
  type: FETCH_PROPERTY_EXPENSES_SUCCESS,
  transactions,
})

export const FETCH_PROPERTY_EXPENSES_FAILED =
  'app/PropertyDetailsPage/FETCH_PROPERTY_EXPENSES_FAILED'
export const fetchPropertyExpensesFailed = error => ({
  type: FETCH_PROPERTY_EXPENSES_FAILED,
  error,
})
// fetch property transaction
export const FETCH_TRANSACTIONS_SUMMARY =
  'app/PropertyDetailsPage/FETCH_TRANSACTIONS_SUMMARY'
export const fetchTransactionsSummary = id => ({
  type: FETCH_TRANSACTIONS_SUMMARY,
  id,
})

export const FETCH_TRANSACTIONS_SUMMARY_SUCCESS =
  'app/PropertyDetailsPage/FETCH_TRANSACTIONS_SUMMARY_SUCCESS'
export const fetchTransactionsSummarySuccess = summary => ({
  type: FETCH_TRANSACTIONS_SUMMARY_SUCCESS,
  summary,
})

export const FETCH_TRANSACTIONS_SUMMARY_FAILED =
  'app/PropertyDetailsPage/FETCH_TRANSACTIONS_SUMMARY_FAILED'
export const fetchTransactionsSummaryFailed = () => ({
  type: FETCH_TRANSACTIONS_SUMMARY_FAILED,
})

// fetch property
export const FETCH_PROPERTY = 'app/PropertyDetailsPage/FETCH_PROPERTY'
export const fetchProperty = id => ({
  type: FETCH_PROPERTY,
  id,
})

export const FETCH_PROPERTY_SUCCESS =
  'app/PropertyDetailsPage/FETCH_PROPERTY_SUCCESS'
export const fetchPropertySuccess = property => ({
  type: FETCH_PROPERTY_SUCCESS,
  property,
})

export const FETCH_PROPERTY_FAILED =
  'app/PropertyDetailsPage/FETCH_PROPERTY_FAILED'
export const fetchPropertyFailed = () => ({
  type: FETCH_PROPERTY_FAILED,
})

// fetch property lease
export const FETCH_PROPERTY_LEASE =
  'app/PropertyDetailsPage/FETCH_PROPERTY_LEASE'
export const fetchPropertyLease = id => ({
  type: FETCH_PROPERTY_LEASE,
  id,
})

export const FETCH_PROPERTY_LEASE_SUCCESS =
  'app/PropertyDetailsPage/FETCH_PROPERTY_LEASE_SUCCESS'
export const fetchPropertyLeaseSuccess = propertyLease => ({
  type: FETCH_PROPERTY_LEASE_SUCCESS,
  propertyLease,
})

export const FETCH_PROPERTY_LEASE_FAILED =
  'app/PropertyDetailsPage/FETCH_PROPERTY_LEASE_FAILED'
export const fetchPropertyLeaseFailed = () => ({
  type: FETCH_PROPERTY_LEASE_FAILED,
})

//  clear data
export const CLEAR_DATA = 'app/PropertyDetailsPage/CLEAR_DATA'
export const clearData = () => ({
  type: CLEAR_DATA,
})

// fetch payment history
export const FETCH_PAYMENT_HISTORY =
  'app/PropertyDetailsPage/FETCH_PAYMENT_HISTORY'
export const fetchPaymentHistory = id => ({
  type: FETCH_PAYMENT_HISTORY,
  id,
})

export const FETCH_PAYMENT_HISTORY_SUCCESS =
  'app/PropertyDetailsPage/FETCH_PAYMENT_HISTORY_SUCCESS'
export const fetchPaymentHistorySuccess = paymentHistory => ({
  type: FETCH_PAYMENT_HISTORY_SUCCESS,
  paymentHistory,
})

export const FETCH_PAYMENT_HISTORY_FAILED =
  'app/PropertyDetailsPage/FETCH_PAYMENT_HISTORY_FAILED'
export const fetchPaymentHistoryFailed = () => ({
  type: FETCH_PAYMENT_HISTORY_FAILED,
})
