/*
 *
 * PropertyDetailsPage reducer
 *
 */

import moment from 'moment'
import Immutable from 'seamless-immutable'
import {
  CHANGE_TRANSACTIONS_END_DATE,
  CHANGE_TRANSACTIONS_START_DATE,
  CHANGE_PROPERTY_END_DATE,
  CHANGE_PROPERTY_START_DATE,
  SELECT_SHOW_CATEGORY,
  FETCH_TRANSACTIONS_SUMMARY_SUCCESS,
  FETCH_PROPERTY_SUCCESS,
  FETCH_PROPERTY_LEASE_SUCCESS,
  FETCH_PROPERTY_EXPENSES_SUCCESS,
  FETCH_PAYMENT_HISTORY_SUCCESS,
  CLEAR_DATA,
} from './actions'
import { DATE_FORMAT } from '../../constants'
export const initialState = Immutable({
  summary: {
    income: {},
    expenses: {},
  },
  paymentHistory: {},
  selectedCategory: 'mortgage',
  property: {},
  transactions: [],
  transactionsPending: false,
  transactionsDateStart: moment()
    .subtract(12, 'months')
    .format(DATE_FORMAT),
  transactionsDateEnd: moment().format(DATE_FORMAT),

  propertyDateStart: moment()
    .subtract(12, 'months')
    .format(DATE_FORMAT),
  propertyDateEnd: moment().format(DATE_FORMAT),
})

function PropertyDetailsPageReducer(state = initialState, action) {
  switch (action.type) {
    case SELECT_SHOW_CATEGORY:
      return state.set('selectedCategory', action.categoryId)
    case CHANGE_TRANSACTIONS_END_DATE:
      return state.set('transactionsDateEnd', action.date)
    case CHANGE_TRANSACTIONS_START_DATE:
      return state.set('transactionsDateStart', action.date)

    case CHANGE_PROPERTY_START_DATE:
      return state.set('propertyDateStart', action.date)
    case CHANGE_PROPERTY_END_DATE:
      return state.set('propertyDateEnd', action.date)
    case FETCH_TRANSACTIONS_SUMMARY_SUCCESS:
      return state.set('summary', action.summary)
    case FETCH_PROPERTY_EXPENSES_SUCCESS:
      return state.set('transactions', action.transactions)
    case FETCH_PROPERTY_SUCCESS:
      return state.set('property', action.property)
    case FETCH_PROPERTY_LEASE_SUCCESS:
      return state.set('propertyLease', action.propertyLease)
    case CLEAR_DATA:
      return initialState
    case FETCH_PAYMENT_HISTORY_SUCCESS:
      return state.set('paymentHistory', action.paymentHistory)
    default:
      return state
  }
}

export default PropertyDetailsPageReducer
