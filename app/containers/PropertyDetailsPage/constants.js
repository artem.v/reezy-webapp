export const CATEGORIES = {
  RENT: 1,
  MORTGAGE: 2,
  REPAIR: 3,
  PROPERTY_TAX: 4,
  MAINTENANCE_FEES: 5,
}

export const EXPENSE_CATEGORIES = [
  CATEGORIES.MORTGAGE,
  CATEGORIES.REPAIR,
  CATEGORIES.PROPERTY_TAX,
  CATEGORIES.MAINTENANCE_FEES,
]
export const LAST_MONTHS_COUNT = 24
export const INCOME_CATEGORIES = [CATEGORIES.RENT]
