/**
 *
 * Sign-up: Create Account
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { FormattedMessage, injectIntl, intlShape } from 'react-intl'
import { createStructuredSelector } from 'reselect'
import { compose } from 'redux'

import {
  makeSelectSignupCreateAccountForm,
  makeSelectSignupStep,
  makeSelectSignupSteps,
} from '../../containers/SignupPage/selectors'
import {
  createAccountSubmit,
  setStep,
} from '../../containers/SignupPage/actions'
import messages from './messages'

import DynamicForm from '../../components/Forms/DynamicForm'
import { FORM_INPUT_TYPES } from '../../components/Forms/constants'
import { COLORS } from '../../constants'
import signupCreateAccountIcon from '../../images/signup-create-account.svg'
import SignupErrorContainer from '../SignupErrorContainer'
import {
  FormHeader,
  FormContent,
  FormFooter,
  IconDiv,
  LabelDiv,
  SubmitButton,
} from '../SignupPage/styled'

const textInputNormalStyle = {
  borderBottom: `2px solid ${COLORS.TEXT}`,
  borderRadius: '2px',
  fontFamily: 'Roboto',
}
const textInputActiveStyle = {
  borderBottom: `2px solid ${COLORS.PRIMARY}`,
  borderRadius: '2px',
  fontFamily: 'Roboto',
}
const textInputErrorStyle = {
  borderBottom: `2px solid ${COLORS.FAILED}`,
  borderRadius: '2px',
  fontFamily: 'Roboto',
}
const textInputCompletedStyle = {
  borderBottom: `2px solid ${COLORS.SUCCESS}`,
  borderRadius: '2px',
  fontFamily: 'Roboto',
}

const passwordsMatch = vals => {
  if (!vals.password || !vals.confirmpassword) {
    return true
  }

  return vals.password === vals.confirmpassword
}

export class SignupCreateAccount extends React.Component {
  constructor(props) {
    super(props)

    this.onSubmit = this.onSubmit.bind(this)
  }

  onSubmit = () => {
    this.props.createAccountSubmit()
  }

  getStyle = model => {
    if (model) {
      const { focus, touched, valid } = model

      if (focus) {
        return textInputActiveStyle
      } else if (valid) {
        return textInputCompletedStyle
      } else if (touched && !valid) {
        return textInputErrorStyle
      }
    }

    return textInputNormalStyle
  }

  getIcon = (model, icon) => {
    if (model) {
      return !model.focus && model.valid && model.touched ? icon : ''
    }

    return ''
  }

  backTask = () => {
    if (this.props.step - 1 >= 0) {
      this.props.setStep(this.props.step - 1)
    }
  }

  render() {
    const { intl, form } = this.props

    return (
      <React.Fragment>
        <FormHeader>
          <IconDiv>
            <img
              src={signupCreateAccountIcon}
              alt={intl.formatMessage(messages.alt)}
            />
          </IconDiv>
          <LabelDiv>{intl.formatMessage(messages.creatingAccount)}</LabelDiv>
        </FormHeader>
        <SignupErrorContainer />
        <FormContent>
          <DynamicForm
            formNS="forms"
            name="signupCreateAccount"
            formValidators={{
              '': {
                passwordsMatch,
              },
            }}
            errorMessages={{
              passwordsMatch: 'Confirm password and password should match',
            }}
            models={['name', 'email', 'password', 'confirmpassword']}
            modelsOptions={{
              name: {
                labelHidden: false,
                label: intl.formatMessage(messages.fullName),
                placeholder: intl.formatMessage(messages.fullName),
                noPadding: true,
                style: this.getStyle(form.name),
                fieldClassName: 'col-12 col-sm-6',
                validation: ['required'],
              },
              email: {
                type: FORM_INPUT_TYPES.EMAIL,
                labelHidden: false,
                label: intl.formatMessage(messages.emailPlaceholder),
                placeholder: intl.formatMessage(messages.emailPlaceholder),
                noPadding: true,
                style: this.getStyle(form.email),
                fieldClassName: 'col-12 col-sm-6',
                validation: ['required', 'isEmail'],
              },
              password: {
                type: FORM_INPUT_TYPES.PASSWORD,
                labelHidden: false,
                label: intl.formatMessage(messages.passwordPlaceholder),
                placeholder: intl.formatMessage(messages.passwordPlaceholder),
                noPadding: true,
                style: this.getStyle(form.password),
                fieldClassName: 'col-12 col-sm-6',
                validation: ['required', 'passwordLength'],
              },
              confirmpassword: {
                type: FORM_INPUT_TYPES.PASSWORD,
                labelHidden: false,
                label: intl.formatMessage(messages.passwordPlaceholder),
                placeholder: intl.formatMessage(messages.passwordPlaceholder),
                noPadding: true,
                style: this.getStyle(form.confirmpassword),
                fieldClassName: 'col-12 col-sm-6',
                validation: ['required', 'passwordLength'],
              },
            }}
            noSubmitButton
            onSubmit={this.onSubmit}
          >
            <FormFooter>
              <SubmitButton
                type="submit"
                disabled={!form.$form.valid || form.$form.pending}
              >
                <FormattedMessage {...messages.continue} />
              </SubmitButton>
            </FormFooter>
          </DynamicForm>
        </FormContent>
      </React.Fragment>
    )
  }
}

SignupCreateAccount.propTypes = {
  form: PropTypes.object,
  intl: intlShape.isRequired,
  createAccountSubmit: PropTypes.func.isRequired,
  step: PropTypes.number,
  setStep: PropTypes.func.isRequired,
  steps: PropTypes.arrayOf(
    PropTypes.shape({
      completed: PropTypes.bool,
    })
  ),
}

const mapStateToProps = createStructuredSelector({
  form: makeSelectSignupCreateAccountForm(),
  step: makeSelectSignupStep(),
  steps: makeSelectSignupSteps(),
})

const mapDispatchToProps = {
  createAccountSubmit,
  setStep,
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

export default compose(
  withConnect,
  injectIntl
)(SignupCreateAccount)
