import styled from 'styled-components'
import { COLORS, SIZES } from '../../constants'
import arrowDown from '../../images/arrowDown.png'

export const FormContainer = styled.div`
  width: 100%;
`
export const FormHeader = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-bottom: 35px;
`
export const FormContent = styled.div`
  padding: 0;
  display: flex;
  flex-direction: column;
  align-items: center;
`
export const FormFooter = styled.div`
  width: 100%;
  text-align: right;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  margin-top: 35px;
`
export const IconDiv = styled.div`
  img {
    width: 45px;
    height: 45px;
  }
`
export const LabelDiv = styled.div`
  margin-left: 20px;
  font-size: 18px;
  font-weight: 600;
  color: ${COLORS.TITLE};
  @media (max-width: 767px) {
    font-size: 16px;
  }
`
export const SubmitButton = styled.button`
  width: 160px;
  height: 40px;
  background-color: ${COLORS.PRIMARY};
  border-radius: 2px;
  font-weight: 600;
  font-size: ${SIZES.H2};
  text-align: center;
  color: ${COLORS.WHITE};
  @media (max-width: 767px) {
    width: 100%;
  }
  &:hover {
    cursor: pointer;
  }
  &:disabled {
    background-color: ${COLORS.TEXT};
  }
`

export const BackArrow = styled.button`
  width: 40px;
  display: none;
  background-image: url(${arrowDown});
  height: 40px;
  background-size: 15px;
  background-position: center;
  background-repeat: no-repeat;
  border-radius: 2px;
  transform: rotate(90deg);
  border: 1px solid ${COLORS.PRIMARY};
  margin: 0 10px 0 0;
  cursor: pointer;
  @media (max-width: 767px) {
    display: inline-flex;
  }
  &:disabled {
    display: none;
  }
`
