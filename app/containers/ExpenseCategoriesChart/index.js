/**
 *
 * BarChart
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import ReactHighcharts from 'react-highcharts'
import { injectIntl, intlShape } from 'react-intl'
import styled from 'styled-components'

import { createStructuredSelector } from 'reselect'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { makeSelectSummary } from '../DashboardPage/selectors'
import messages from './messages'
import { PROPERTY_COLORS } from './constants'
import { COLORS } from '../../constants'

const Wrapper = styled.div`
  padding: 40px 30px 30px 0;
  background-color: #ffffff;
  box-shadow: 2px 2px 7px 0 rgba(8, 39, 64, 0.1);
  max-width: 800px;
  border-radius: 2px;
  @media (max-width: 767px) {
    padding: 15px 0;
  }
`

class BarChart extends React.Component {
  getConfig = props => {
    const series = []
    const includesProperty = [
      'repair',
      'propertyTax',
      'maintenanceFees',
      'mortgage',
    ]

    includesProperty.forEach(property => {
      if (+props[property]) {
        series.push({
          name: this.props.intl.formatMessage(messages[property]),
          color: {
            linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
            stops: PROPERTY_COLORS[property],
          },
          data: [+props[property]],
        })
      }
    })

    return {
      chart: {
        renderTo: 'container',
        type: 'bar',
        marginLeft: 0,
        marginTop: 0,
        marginBottom: 40,
      },
      title: {
        text: `<h2 style="font-size: 18px; margin-left: 10px; line-height: 23px;color: #082740;font-weight: bold">${this.props.intl.formatMessage(
          messages.title
        )}</h2>`,
        align: 'left',
        margin: 15,
        useHTML: true,
      },
      credits: {
        enabled: false,
      },
      xAxis: {
        categories: [
          this.props.intl.formatMessage(messages.repair),
          this.props.intl.formatMessage(messages.propertyTax),
          this.props.intl.formatMessage(messages.maintenanceFees),
          this.props.intl.formatMessage(messages.mortgage),
        ],
        enabled: false,
        gridLineWidth: 0,
        visible: false,
        title: {
          enabled: false,
        },
        labels: {
          enabled: false,
        },
      },
      legend: {
        enabled: false,
      },
      tooltip: {
        enabled: false,
      },
      yAxis: {
        gridLineWidth: 0,
        title: {
          enabled: false,
        },
        labels: {
          formatter: this.renderBarXAxisLabel,
        },
      },
      series,
      plotOptions: {
        series: {
          pointPadding: 0.2,
          // groupPadding: 0.1,
          dataLabels: {
            allowOverlap: true,
            enabled: true,
            align: 'left',
            defer: true,
            color: '#000',
            useHTML: true,
            shape: 'ef',
            shadow: false,
            overflow: 'allow',
            formatter: this.renderBarLineLabel,
            inside: true,
            rotation: 0,
          },
        },
      },
    }
  }
  renderBarXAxisLabel() {
    return `<h3 style="color: #858E93; font-size: 14px; line-height: 18px; font-weight: bold;">${
      this.value ? this.value : ''
    }</h3>`
  }

  renderBarLineLabel() {
    return `<h3 style="margin-left: 10px; margin-bottom: 60px; font-weight: bold; font-size: 14px; line-height: 18px; color: ${
      COLORS.TEXT
    }">
      ${this.series.name}
      </h3>`
  }

  shouldComponentUpdate = props =>
    JSON.stringify(props.summary) !== JSON.stringify(this.props.summary)

  render() {
    return (
      <Wrapper>
        {Object.values(this.props.summary).length ? (
          <ReactHighcharts config={this.getConfig(this.props.summary)} />
        ) : null}
      </Wrapper>
    )
  }
}

BarChart.propTypes = {
  intl: intlShape.isRequired,
  summary: PropTypes.object,
}

BarChart.defaultProps = {
  summary: {},
}

const mapStateToProps = createStructuredSelector({
  summary: makeSelectSummary(),
})

const withConnect = connect(
  mapStateToProps,
  null
)
export default compose(
  withConnect,
  injectIntl
)(BarChart)
