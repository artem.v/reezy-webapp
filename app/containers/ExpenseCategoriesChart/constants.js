export const PROPERTY_COLORS = {
  repair: [[0, '#123FBA'], [1, '#5C8AF7']],
  propertyTax: [[0, '#F56343'], [1, '#F5C243']],
  maintenanceFees: [[0, '#F56343'], [1, '#F5C243']],
  mortgage: [[0, '#E860A9'], [1, '#E76067']],
}
