/*
 * SignInPage Messages
 *
 * This contains all the text for the SignInPage component.
 */

import { defineMessages } from 'react-intl'
import { createIntlMessage } from 'common'

const COMPONENT_ID = 'ExpensesCategoriesChart'

export default defineMessages({
  ...createIntlMessage(COMPONENT_ID, 'repair'),
  ...createIntlMessage(COMPONENT_ID, 'propertyTax'),
  ...createIntlMessage(COMPONENT_ID, 'maintenanceFees'),
  ...createIntlMessage(COMPONENT_ID, 'mortgage'),
  ...createIntlMessage(COMPONENT_ID, 'title'),
})
