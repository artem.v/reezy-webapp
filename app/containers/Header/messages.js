/*
 * Header Messages
 *
 * This contains all the text for the Header container.
 */

import { defineMessages } from 'react-intl'
import { createIntlMessage } from 'common'

const CONTAINER_ID = 'Header'

export default defineMessages({
  ...createIntlMessage(CONTAINER_ID, 'dashboard'),
  ...createIntlMessage(CONTAINER_ID, 'transactions'),
  ...createIntlMessage(CONTAINER_ID, 'properties'),
  ...createIntlMessage(CONTAINER_ID, 'settings'),
  ...createIntlMessage(CONTAINER_ID, 'signOut'),
  ...createIntlMessage(CONTAINER_ID, 'appName'),
  ...createIntlMessage(CONTAINER_ID, 'trademark'),
  ...createIntlMessage(CONTAINER_ID, 'aboutUs'),
  ...createIntlMessage(CONTAINER_ID, 'contact'),
  ...createIntlMessage(CONTAINER_ID, 'signInLink'),
})
