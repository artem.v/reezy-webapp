import styled from 'styled-components'
import { NavLink, Link } from 'react-router-dom'
import Avatar from 'react-avatar'

import mobileNavigationBg from '../../images/mobileNavigationBg.png'

import { COLORS } from '../../constants'

export const Wrapper = styled.div`
  background-color: #fff;
  display: flex;
  min-height: 80px;
  flex-direction: row;
  padding: 0 0 0 20px;
  align-items: center;
  justify-content: space-between;
  z-index: 2;
  @media (max-width: 767px) {
    min-height: 60px;
    padding: 10px 0 10px 10px;
    position: sticky;
    top: 0;
    box-shadow: 2px 2px 7px 0 rgba(8, 39, 64, 0.1);
  }
`

export const Navigation = styled.ul`
  display: flex;
  flex-grow: 1;
  justify-content: center;
  align-items: center;
  margin: 0;
  transition: 0.3s;
  padding: 0 15px;
  @media (max-width: 767px) {
    background-color: #5983f7;
    background-image: url('${mobileNavigationBg}');
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
    position: fixed;
    bottom: 0;
    min-width: 100vw;
    min-height: 100vh;
    top: 0;
    right: 0;
    left: 0;
    transform: translateX(100%);
    flex-wrap: wrap;
    padding: 180px 15px;
    &.active {
      transform: translateX(0);
    }
    h2 {
      color: #ffffff;
    }
  }
`

export const NavigationItem = styled.li`
  list-style: none;
  display: flex;
  flex-grow: 1;
  margin: 0 7.5px;
  max-width: 174px;
  font-family: 'PT Sans', sans-serif;
  @media (max-width: 767px) {
    width: 100%;
    max-width: initial;
  }
`
export const NavigationUserMenuItem = styled(NavigationItem)`
  display: none;
  @media (max-width: 767px) {
    display: flex;
  }
`

export const NavigationLink = styled(NavLink).attrs({
  activeClassName: 'active',
})`
  padding: 0;
  width: 100%;
  text-decoration: none;
  text-align: center;
  position: relative;

  &.active {
    h2 {
      color: #5983f7;
    }
    &:before {
      left: 0px;
      border-top-right-radius: 30px;
      border-top-left-radius: 30px;
      right: 0px;
      background: #5983f7;
    }
    &:hover {
      &:before {
        height: 5px;
        left: 0px;
        border-top-right-radius: 30px;
        border-top-left-radius: 30px;
        right: 0px;
        background: #5983f7;
      }
    }
  }
  &:before {
    content: '';
    height: 5px;
    position: absolute;
    bottom: -28px;
    left: 0;
    right: 0;
    display: flex;
  }
  &:hover {
    text-decoration: none;
    &:before {
      left: 3px;
      border-top-right-radius: 80px;
      border-top-left-radius: 80px;
      right: 3px;
      height: 3px;
      background: linear-gradient(
        to top,
        #5983f7 0%,
        rgba(89, 131, 247, 0) 100%
      );
    }
  }
  @media (max-width: 767px) {
    &:before {
      display: none;
    }
    &.active {
      h2 {
        color: #ffffff;
      }
    }
  }
`
export const NavigationText = styled.h2`
  font-size: 18px;
  font-weight: bold;
  line-height: 23px;
  color: #858e93;
  margin: 0;
  @media (max-width: 767px) {
    font-size: 24px;
    font-weight: bold;
    line-height: 31px;
  }
`

export const NavigationUserText = styled(NavigationText)`
  font-size: 18px;
  font-weight: bold;
  line-height: 23px;
  color: #858e93;
  margin: 0;
`

export const TinyLogo = styled.img`
  height: 40px;
  padding-right: 20px;
  display: none;
  @media (max-width: 991px) {
    display: inline-block;
  }
`

export const FullLogo = styled.img`
  height: 30px;
  margin-top: 5px;
  margin-left: 10px;
  display: inline-block;
  @media (max-width: 991px) {
    display: none;
  }
`

export const StyledAvatar = styled(Avatar)`
  .sb-avatar__text {
    border: ${props => (props.active ? '2px solid #5983f7' : 'none')};
  }
`

export const UserLogo = styled.img`
  border-radius: 50%;
  height: 40px;
  width: 40px;
  border: 2px solid #ffffff;
  &.active {
    border: 2px solid #5983f7;
  }
  @media (max-width: 991px) {
    border-width: 1px;
  }
`

export const UserName = styled.h3`
  color: #082740;
  line-height: 18px;
  font-size: 14px;
  font-style: bold;
  margin: 0 15px 0 0;
  &.active {
    color: #5983f7;
  }
  @media (max-width: 767px) {
    &.active {
      color: #ffffff;
    }
  }
`
export const AuthWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  position: relative;
  z-index: 2;
`

export const HeaderMobileMenu = styled.div`
  border-radius: 50%;
  z-index: 2;
  margin-left: 10px;
  min-width: 40px;
  height: 40px;
  border: 1px solid #ffffff;
  background-color: #5983f7;
  justify-content: center;
  cursor: pointer;
  align-items: center;
  display: none;
  flex-direction: column;
  transition: 0.4s;
  @media (max-width: 767px) {
    display: flex;
  }
  &.active {
    transform: rotate(45deg);
    span.dots-line {
      width: 4px;
      margin: 2px 0;
    }
  }
`
export const ButtonDotsLine = styled.span.attrs({
  className: 'dots-line',
})`
  display: flex;
  width: 20px;
  height: 4px;
  border-radius: 10px;
  margin: 1.5px 0;
  background-color: #ffffff;
  transition: 0.5s;
`

export const UserMenuWrapper = styled.div`
  position: absolute;
  right: 0;
  bottom: -90px;
  @media (max-width: 767px) {
    display: none;
  }
`

export const UserMenu = styled.div`
  border-radius: 2px;
  background-color: #ffffff;
  z-index: 1;
  box-shadow: 0px 1px 15px 1px rgba(69, 65, 78, 0.08);
  h3 {
    border-bottom: 1px solid #f5f7fc;
  }
  &:last-child {
    border-bottom: none;
  }
`

export const UserMenuTriangle = styled.span`
  width: 10px;
  height: 10px;
  display: flex;
  box-shadow: -4px 0 7px 0 rgba(8, 39, 64, 0.1);
  z-index: 0;
  position: absolute;
  right: 13px;
  background-color: #ffffff;
  top: -6px;
  transform: rotate(45deg);
`
export const UserMenuItem = styled.h3`
  padding: 10px 20px;
  cursor: pointer;
  margin: 0;
  text-align: left;
  font-weight: normal;
  font-size: 12px;
  line-height: 14px;
  color: #858e93;
`

export const UserMenuNavLink = styled(NavLink)`
  text-decoration: none;
  &:hover {
    h3 {
      font-weight: bold;
      font-size: 14px;
      line-height: 18px;
      color: #5983f7;
    }
    color: #67666e;
    text-decoration: none;
  }
`

export const RightControl = styled.div`
  display: flex;
  z-index: 2;
  position: relative;
  margin-right: 20px;
  @media (max-width: 767px) {
    margin-right: 10px;
  }
`

export const NotLoggedUserNavigation = styled(Navigation)`
  display: flex;
  justify-content: flex-end;
  align-items: center;

  li {
    border-left: 1px solid ${COLORS.BACKGROUND};
  }

  @media (max-width: 767px) {
    flex-direction: column;
    justify-content: center;
    li {
      border: none;
    }
  }
`

export const NotLoggedNavigationItem = styled.li`
  display: flex;
  height: 80px;
  padding: 30px;
`

export const NotLoggedNavigationText = styled(Link)`
  font-weight: 600;
  font-size: 14px;
  line-height: normal;
  color: ${COLORS.TITLE};
  &:hover {
    text-decoration: none;
  }
  @media (max-width: 767px) {
    font-size: 24px;
    color: ${COLORS.WHITE};
    font-weight: bold;
    &:hover {
      color: ${COLORS.WHITE};
    }
  }
`

export const NotLoggedControl = styled(RightControl)`
  display: none;
  @media (max-width: 767px) {
    display: flex;
  }
`
