/**
 *
 * Header
 *
 */

import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { injectIntl, intlShape } from 'react-intl'

import { withRouter } from 'react-router'

import { sessionDestroy } from '../App/actions'

import messages from './messages'

import logo from '../../images/logo.png'
import favicon from '../../images/favicon.png'

import {
  Wrapper,
  AuthWrapper,
  ButtonDotsLine,
  TinyLogo,
  FullLogo,
  NavigationLink,
  NotLoggedControl,
  NotLoggedNavigationItem,
  NotLoggedNavigationText,
  NotLoggedUserNavigation,
  UserMenuNavLink,
  UserMenu,
  UserMenuItem,
  UserMenuTriangle,
  UserMenuWrapper,
  UserName,
  NavigationUserMenuItem,
  NavigationUserText,
  HeaderMobileMenu,
  Navigation,
  NavigationItem,
  NavigationText,
  RightControl,
  StyledAvatar,
} from './styled'

export class Header extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      mobileMenuStatus: '',
      userMenuStatus: '',
    }

    this.mainLinksInternal = [
      {
        to: '/dashboard',
        label: this.props.intl.formatMessage(messages.dashboard),
      },
      {
        to: '/transactions',
        label: this.props.intl.formatMessage(messages.transactions),
      },
      {
        to: '/properties',
        label: this.props.intl.formatMessage(messages.properties),
      },
    ]

    this.mainLinksExternal = [
      {
        to: '/',
        label: this.props.intl.formatMessage(messages.aboutUs),
      },
      {
        to: '/',
        label: this.props.intl.formatMessage(messages.contact),
      },
      {
        to: '/signin',
        label: this.props.intl.formatMessage(messages.signInLink),
      },
    ]

    this.userLinks = [
      {
        to: '',
        label: this.props.intl.formatMessage(messages.settings),
      },
      {
        to: '/dashboard',
        onClick: this.props.sessionDestroy,
        label: this.props.intl.formatMessage(messages.signOut),
      },
    ]
  }

  toggleMobileMenu = () => {
    if (window.innerWidth < 767) {
      this.setState(state => ({
        mobileMenuStatus: state.mobileMenuStatus === 'active' ? '' : 'active',
      }))
    }
  }

  userMenuStatus = () => {
    if (window.innerWidth > 767) {
      this.setState(state => ({
        userMenuStatus: state.userMenuStatus === 'active' ? '' : 'active',
      }))
    }
  }

  renderUserMenuLink = link => (
    <UserMenuNavLink key={link.to} to={link.to} onClick={link.onClick}>
      <UserMenuItem>{link.label}</UserMenuItem>
    </UserMenuNavLink>
  )

  renderMenuControl = () => (
    <HeaderMobileMenu
      className={this.state.mobileMenuStatus}
      onClick={this.toggleMobileMenu}
    >
      <ButtonDotsLine />
      <ButtonDotsLine />
      <ButtonDotsLine />
    </HeaderMobileMenu>
  )

  renderAuth = () => (
    <AuthWrapper onClick={this.userMenuStatus}>
      <UserName
        className={this.state.mobileMenuStatus || this.state.userMenuStatus}
      >
        {this.props.user.firstName} {this.props.user.lastName}
      </UserName>

      <StyledAvatar
        name={`${this.props.user.firstName} ${this.props.user.lastName}`}
        round
        size={40}
        active={this.state.userMenuStatus === 'active'}
      />
      {this.state.userMenuStatus ? (
        <UserMenuWrapper>
          <UserMenu>{this.userLinks.map(this.renderUserMenuLink)}</UserMenu>
          <UserMenuTriangle />
        </UserMenuWrapper>
      ) : null}
    </AuthWrapper>
  )

  renderExternalNavigation = link => (
    <NotLoggedNavigationItem key={link.to}>
      <NotLoggedNavigationText to={link.to}>
        {link.label}
      </NotLoggedNavigationText>
    </NotLoggedNavigationItem>
  )

  renderInternalNavigation = link => (
    <NavigationItem onClick={this.toggleMobileMenu} key={link.to}>
      <NavigationLink to={link.to}>
        <NavigationText>{link.label}</NavigationText>
      </NavigationLink>
    </NavigationItem>
  )

  renderUserNavigation = link => (
    <NavigationUserMenuItem onClick={this.toggleMobileMenu} key={link.to}>
      <NavigationLink to={link.to} onClick={link.onClick}>
        <NavigationUserText>{link.label}</NavigationUserText>
      </NavigationLink>
    </NavigationUserMenuItem>
  )

  renderLoggedUserNavigation = () => (
    <React.Fragment>
      <Navigation className={this.state.mobileMenuStatus}>
        {this.props.hideMainMenu
          ? null
          : this.mainLinksInternal.map(this.renderInternalNavigation)}
        {!this.props.hideMainMenu
          ? null
          : this.mainLinksExternal.map(this.renderUserNavigation)}
        {this.userLinks.map(this.renderUserNavigation)}
      </Navigation>
      <RightControl>
        {this.renderAuth()}
        {this.renderMenuControl()}
      </RightControl>
    </React.Fragment>
  )

  renderNotLoggedUserNavigation = () => (
    <React.Fragment>
      <NotLoggedUserNavigation className={this.state.mobileMenuStatus}>
        {this.mainLinksExternal.map(this.renderExternalNavigation)}
      </NotLoggedUserNavigation>
      <NotLoggedControl>{this.renderMenuControl()}</NotLoggedControl>
    </React.Fragment>
  )

  render() {
    return (
      <Wrapper>
        <Link to="/">
          <TinyLogo
            src={favicon}
            alt={this.props.intl.formatMessage(messages.trademark)}
          />
          <FullLogo
            src={logo}
            alt={this.props.intl.formatMessage(messages.appName)}
          />
        </Link>
        {this.props.user
          ? this.renderLoggedUserNavigation()
          : this.renderNotLoggedUserNavigation()}
      </Wrapper>
    )
  }
}

Header.propTypes = {
  user: PropTypes.shape({
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
  }),
  hideMainMenu: PropTypes.bool,
  sessionDestroy: PropTypes.func.isRequired,
  intl: intlShape.isRequired,
}

Header.defaultProps = {
  hideMainMenu: false,
}

const mapStateToProps = state => ({
  user: state.app.user,
})

const mapDispatchToProps = {
  sessionDestroy,
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

export default compose(
  withConnect,
  withRouter,
  injectIntl
)(Header)
