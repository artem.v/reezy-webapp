import React from 'react'
import { FormattedMessage } from 'react-intl'
import { shallow } from 'enzyme'

import LangingPage from '../index'
import messages from '../messages'

describe('<LangingPage />', () => {
  it('should render the page message', () => {
    const renderedComponent = shallow(<LangingPage />)

    expect(
      renderedComponent.contains(<FormattedMessage {...messages.header} />)
    ).toEqual(true)
  })
})
