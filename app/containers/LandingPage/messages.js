/*
 * LangingPage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl'
import { createIntlMessage } from 'common'

const PAGE_ID = 'LangingPage'

export default defineMessages({
  ...createIntlMessage(PAGE_ID, 'reezy'),
  ...createIntlMessage(PAGE_ID, 'description'),
})
