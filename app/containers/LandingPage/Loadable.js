/**
 * Asynchronously loads the component for LangingPage
 */
import Loadable from 'react-loadable'

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
})
