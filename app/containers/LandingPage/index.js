import React from 'react'
import { compose } from 'redux'
import { injectIntl } from 'react-intl'
import injectSession from '../CheckSessionHOC'

import './style.css'

// import messages from './messages'
import logoImage from '../../images/landing/logo.svg'
import macbookImage from '../../images/landing/macbook.png'
import iconZero from '../../images/landing/icon.svg'
import iconFirst from '../../images/landing/icon-1.svg'
import iconSecond from '../../images/landing/icon-2.svg'
import iconThird from '../../images/landing/icon-3.svg'

/* eslint-disable react/prefer-stateless-function  */
/* eslint-disable react-intl/string-is-marked-for-translation */
class LangingPage extends React.PureComponent {
  render() {
    return (
      <div className="wrapper">
        {/* <!--header--> */}

        <header id="header">
          <div className="container-fluid">
            <nav className="navbar">
              <a className="navbar-brand" href="/">
                <img src={logoImage} alt="Reezy" />
              </a>
              {/* <div className="rigister-main ml-auto">
                <ul className="link-head">
                  <li><a href="#">You have an account?</a></li>
                  <li><a href="#">Sing in</a></li>
                  <li><a href="#">Registration</a></li>
                </ul>
              </div> */}
            </nav>
          </div>
        </header>

        {/* <!--premium-sec--> */}

        <section className="premium-sec">
          <div className="container">
            <div className="row">
              <div className="col-lg-5 col-md-12">
                <div className="text-hold">
                  <h1>
                    <span>A Seamless</span>{' '}
                    <span className="line">Financial</span>
                    <br />
                    <span className="line ">Tracking Solution</span>
                    <p>for Smart Real Estate Investors!</p>
                  </h1>
                  <div className="form-group m-0">
                    <div className="realtive">
                      <form
                        action={process.env.MAILCHIMP_FORM_URL}
                        method="post"
                        target="_blank"
                      >
                        <input
                          type="email"
                          name="EMAIL"
                          className="form-control"
                          placeholder="Your email adress"
                          required
                        />
                        <div
                          style={{ position: 'absolute', left: '-5000px' }}
                          aria-hidden="true"
                        >
                          <input
                            type="text"
                            name={process.env.MAILCHIMP_FORM_PROTECTED_CODE}
                            tabIndex="-1"
                            value=""
                          />
                        </div>
                        <button type="submit" className="get-btn">
                          Get started
                        </button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-7 col-md-12">
                <div className="macbook">
                  {' '}
                  <img src={macbookImage} alt="Reezy Dashboard" />{' '}
                </div>
              </div>
            </div>
          </div>
        </section>

        {/* <!--solution-sec--> */}

        <section className="solution-sec">
          <div className="container">
            <div className="row">
              <div className="col-lg-6 col-md-12">
                <div className="main-holder-one main-holder ">
                  <h2>
                    Start managing <span>easy</span>
                  </h2>
                  <p>
                    If you’re a real estate investor, you know that the
                    challenge of tracking and managing the myriad financial
                    factors involved in your investments can at times be an
                    almost insurmountable task.
                  </p>
                  <p>
                    Piles of paperwork, multiple spreadsheets and documents,
                    possibly several bank accounts attached to different
                    ventures or for different purposes – it can be all too easy
                    to get lost in the mix and lose sight of your overall
                    priorities.
                  </p>
                  <p>
                    So how can you track your progress and strategize forward
                    momentum without an overall view as to your financial
                    situation and account statuses?
                  </p>
                </div>
              </div>
              <div className="col-lg-6 col-md-12">
                <div className="main-holder">
                  <h2>
                    Reezy is the <span>ultimate solution</span>
                    <br />
                    you need!
                  </h2>
                  <p>
                    It’s a financial management and tracking solution that’s
                    tailor-made specifically for real estate investors. Reezy
                    has the edge on other popular financial management sites and
                    services when it comes to real estate, because it’s the only
                    financial management service of its kind – one made to
                    provide solutions to the unique challenges real estate
                    investors face.
                  </p>
                  <p>
                    From tracking when rent is due for all of your properties to
                    generating reports as to your properties’ performance at
                    will, Reezy provides myriad solutions designed to remove the
                    headache from your real estate investment endeavors.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>

        {/* <!--user-sec--> */}
        <section className="user-sec">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <div className="user-text-hold text-center">
                  <h2>
                    <span>User-friendly interface</span> designed<br />
                    to provide insights at a glance!
                  </h2>
                  <p>
                    Reezy was created by industry professionals who understand
                    what information real estate investors need to be able to
                    access immediately and at any time. That’s why the
                    easy-to-navigate interface displays all the most relevant
                    information at a glance, including but not limited to:
                  </p>
                </div>
              </div>
            </div>
            <div className="main-inner-hol">
              <div className="row">
                <div className="col-md-7 order-lg-1 order-md-1 order-sm-2 order-col-2">
                  <div className="user-caption">
                    {' '}
                    <span>01</span>
                    <h3>Account information</h3>
                    <p>
                      It’s likely that you hold myriad accounts across various
                      institutions, and those balances change on the regular
                      throughout the day.{' '}
                    </p>
                    <p className="mb-0">
                      Reezy offers an at-a-glance look at all of your accounts
                      on a single page so you know exactly where you stand and
                      can see at once if something requires immediate attention.
                    </p>
                  </div>
                </div>
                <div className="col-md-5 order-lg-2 order-md-2 order-sm-1 order-col-1">
                  <div className="icon-hold">
                    <img src={iconZero} alt="" />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-5">
                  <div className="icon-hold">
                    <img src={iconFirst} alt="" />
                  </div>
                </div>
                <div className="col-md-7">
                  <div className="user-caption">
                    {' '}
                    <span>02</span>
                    <h3>
                      Relevant data and<br />
                      transaction display
                    </h3>
                    <p>
                      Using intelligent and integrated measures, Reezy shows you
                      the information you need to see - without the clutter of
                      irrelevant information and reports. With just a couple
                      clicks or taps, you can generate reports based on the
                      metrics you wish to track to make sure your investments
                      are performing well and right on track.{' '}
                    </p>
                    <p className="mb-0">
                      Reezy keeps you up to date with regular cash flow reports
                      accessible via multiple avenues, including on your
                      homepage.
                    </p>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-7 order-lg-1 order-md-1 order-sm-2 order-col-2">
                  <div className="user-caption">
                    {' '}
                    <span>03</span>
                    <h3>
                      Powerful suite of rent<br />
                      management tools
                    </h3>
                    <p className="mb-0">
                      Every real estate owner and investor knows: one of the
                      biggest hassles of owning or managing property is staying
                      on top of the rent. Reezy sends you a notification every
                      time a rent payment has come up due so you’re never left
                      out of the loop and nothing ever falls through the cracks
                      again.
                    </p>
                  </div>
                </div>
                <div className="col-md-5 order-lg-2 order-md-2 order-sm-1 order-col-1">
                  <div className="icon-hold">
                    <img src={iconSecond} alt="" />
                  </div>
                </div>
              </div>
              <div className="row mb-0">
                <div className="col-md-5">
                  <div className="icon-hold">
                    <img src={iconThird} alt="" />
                  </div>
                </div>
                <div className="col-md-7">
                  <div className="user-caption">
                    {' '}
                    <span>04</span>
                    <h3>Expense management platform</h3>
                    <p>
                      Between scheduled maintenance measures and surprise
                      property repairs and everything in between, it can become
                      a burdensome task to manage the myriad expenses related to
                      your real estate properties.{' '}
                    </p>
                    <p className="mb-0">
                      Reezy makes it easy for you to track expenses, create and
                      adjust your budget and get a categorized breakdown of your
                      expenses so you can make informed decisions as to where to
                      allocate your funds and what expenses need attention the
                      most.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        {/* <!--platform-sec--> */}

        <section className="platform-sec">
          <div className="container">
            <div className="row">
              <div className="col-lg-6">
                <div className="user-text-hold ">
                  <h2 className="text-white">
                    <span className="text-white">Reezy</span> is the platform<br />
                    you need to set and<br />
                    reach your real estate<br />
                    goals!{' '}
                  </h2>
                </div>
              </div>
              <div className="col-lg-6">
                <p className="platform-caption">
                  As you seek to expand your real estate ventures and invest in
                  new properties, Reezy makes it possible for you to reach your
                  next major milestone. With the robust and thorough view of
                  your financial situation as a whole, your next goal may be
                  closer than you thought. Reezy enables you to make sound
                  financial decisions and plan your next major moves in the real
                  estate market.
                </p>
              </div>
            </div>
            {/* <div className="main-inner-hol">
              <div className="slider owl-carousel owl-theme">
                <div className="item">
                  <div className="review-box">
                    <div className="brand">
                      {' '}
                      <img src="images/brand.svg" alt="" />{' '}
                    </div>
                    <h4>Tamara</h4>
                    <span>CEO TopRealty, New York</span>
                    <p>
                      I recently signed up to service, the support team and my
                      agent especially Thanh has been amazing. I look forward to
                      using this service for many years to come...If any tradies
                      are looking to grow their business I would highly
                      recommend service and definitely ask for Thanh.
                    </p>
                  </div>
                </div>
                <div className="item">
                  <div className="review-box pt-0">
                    <div className="brand">
                      {' '}
                      <img src="images/brand-two.svg" alt="" />{' '}
                    </div>
                    <h4>Robert</h4>
                    <span>Manager, Atlanta</span>
                    <p>
                      Honest and real service from Jeremy. I’ve had the pleasure
                      in having Jeremy Veitch as my account manager and he has
                      been absolutely perfect!! Can not fault the guy he is a
                      great asset to the company, made me feel very welcome!
                    </p>
                  </div>
                </div>
                <div className="item">
                  <div className="review-box">
                    <div className="brand">
                      {' '}
                      <img src="images/brand.svg" alt="" />{' '}
                    </div>
                    <h4>Tamara</h4>
                    <span>CEO TopRealty, New York</span>
                    <p>
                      I recently signed up to service, the support team and my
                      agent especially Thanh has been amazing. I look forward to
                      using this service for many years to come...If any tradies
                      are looking to grow their business I would highly
                      recommend service and definitely ask for Thanh.
                    </p>
                  </div>
                </div>
              </div>
            </div> */}
          </div>
        </section>
        <footer id="footer">
          <div className="container">
            <div className="get-main-box text-center">
              <p>
                Get started today to see for yourself what<br />
                <span>Reezy</span> can do for your business
              </p>
              <div className="form-main">
                <form
                  action={process.env.MAILCHIMP_FORM_URL}
                  method="post"
                  target="_blank"
                >
                  <div className="form-group m-0">
                    <div className="realtive">
                      <input
                        type="name"
                        name="NAME"
                        className="form-control name-input"
                        placeholder="You name"
                        required
                      />
                      <input
                        type="email"
                        name="EMAIL"
                        className="form-control email-input"
                        placeholder="Your email adress"
                        required
                      />
                      <div
                        style={{ position: 'absolute', left: '-5000px' }}
                        aria-hidden="true"
                      >
                        <input
                          type="text"
                          name={process.env.MAILCHIMP_FORM_PROTECTED_CODE}
                          tabIndex="-1"
                          value=""
                        />
                      </div>
                      <button type="submit" className="get-btn">
                        Get started
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div className="bottom-bar">
            <div className="container-fluid">
              <div className="row">
                <div className="col-lg-8 col-md-9">
                  <div className="main-reezy">
                    {' '}
                    <a className="navbar-brand" href="/">
                      <img src={logoImage} alt="" />
                    </a>
                    {/* <ul className="privcy-link">
                      <li>
                        <a href="#">Terms of Service </a>
                      </li>
                      <li className="mr-0">
                        <a href="#">Privacy Policy</a>
                      </li>
                    </ul> */}
                  </div>
                </div>
                <div className="col-lg-4 col-md-3">
                  <p className="copyright">
                    &copy; {new Date().getFullYear()} Reezy
                  </p>
                </div>
              </div>
            </div>
          </div>
        </footer>
      </div>
    )
  }
}

LangingPage.propTypes = {
  // intl: intlShape.isRequired,
}

const withSession = injectSession({
  redirectSignedIn: '/dashboard',
})

export default compose(
  withSession,
  injectIntl
)(LangingPage)
