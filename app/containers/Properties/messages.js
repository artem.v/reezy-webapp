/*
 * SignInPage Messages
 *
 * This contains all the text for the SignInPage component.
 */

import { defineMessages } from 'react-intl'
import { createIntlMessage } from 'common'

const CONTAINER_ID = 'PropertiesContainer'

export default defineMessages({
  ...createIntlMessage(CONTAINER_ID, 'title'),
  ...createIntlMessage(CONTAINER_ID, 'ytdIncome'),
  ...createIntlMessage(CONTAINER_ID, 'ytdCashFlow'),
  ...createIntlMessage(CONTAINER_ID, 'showMore'),
  ...createIntlMessage(CONTAINER_ID, 'addProperty'),
})
