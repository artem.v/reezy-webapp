import styled from 'styled-components'
import { Link } from 'react-router-dom'

import { COLORS } from '../../constants'

export const Paper = styled.div`
  padding: 40px 30px 30px 30px;
  background-color: ${COLORS.WHITE};
  box-shadow: 2px 2px 7px 0 rgba(8, 39, 64, 0.1);
  max-width: 800px;
  margin-bottom: 30px;
  border-radius: 2px;
  @media (max-width: 767px) {
    padding: 15px 10px;
  }
`

export const Text = styled.h3`
  font-size: 12px;
  line-height: 14px;
  color: ${COLORS.TEXT};
  margin: 0;
`

export const PlankText = styled(Text)`
  color: ${COLORS.WHITE};
  font-size: 14px;
  line-height: 18px;
`

export const PlankTextBold = styled(PlankText)`
  font-weight: 600;
  margin-bottom: 10px;
`

export const Plank = styled.div`
  padding: 20px;
  border-top-right-radius: 2px;
  border-bottom-right-radius: 2px;
  background: linear-gradient(
    to left,
    ${props => `${props.from || COLORS.FROM}, ${props.to || COLORS.TO}`}
  );
`
export const PropertiesHeader = styled.header`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 0;
  &.active {
    margin-bottom: 30px;
  }
`

export const PropertyPlank = styled(Plank)`
  border-top-right-radius: 2px;
  border-top-left-radius: 2px;
  border-bottom-right-radius: 0;
  display: flex;
  flex-direction: column;
  cursor: pointer;

  background: linear-gradient(
    to left,
    ${props => (props.idx % 2 ? '#5C8AF7, #123FBA' : '#F5C243, #F56343 ')}
  );
`

export const PropertyContent = styled.div`
  max-height: 0;
  display: flex;
  flex-direction: column;
  overflow: hidden;
  padding: 0 2px;
  &.active {
    max-height: 10000px;
  }
`

export const PropertyControlWrapper = styled.div`
  margin-top: 30px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`

export const PropertyCostsWrapper = styled.div`
  border-bottom-right-radius: 2px;
  border-bottom-left-radius: 2px;
  padding: 20px;
  box-shadow: 2px 2px 7px 0 rgba(8, 39, 64, 0.1);
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`

export const PropertyWrapper = styled.div`
  display: flex;
  margin: 0;
  padding: 0;
  flex-direction: column;
  margin-bottom: 10px;
`

export const Title = styled.h2`
  color: ${COLORS.TITLE};
  font-size: 18px;
  line-height: 23px;
  font-weight: 600;
`
export const ShowMoreButton = styled(Link)`
  color: ${COLORS.PRIMARY};
  font-size: 14px;
  line-height: 18px;
  font-weight: 600;
  &:hover {
    text-decoration: none;
    color: ${COLORS.PRIMARY};
  }
`
export const AddPropertyButton = styled(ShowMoreButton)`
  font-size: 18px;
  line-height: 23px;
  text-align: center;
  padding: 8px 24px;
  border: 1px solid ${COLORS.PRIMARY};
  border-radius: 2px;
`

export const SmallText = styled.p`
  margin: 0;
  font-size: 12;
  line-height: 14px;
  color: ${COLORS.TEXT};
`

export const SmallPlankText = styled(SmallText)`
  color: ${COLORS.WHITE};
`

export const ToggleButton = styled.div`
  border-radius: 50%;
  margin-left: 10px;
  max-width: 40px;
  min-width: 40px;
  height: 40px;
  border: 1px solid ${COLORS.WHITE};
  background-color: ${COLORS.PRIMARY};
  justify-content: center;
  cursor: pointer;
  align-items: center;
  display: flex;
  flex-direction: column;
  transition: 0.4s;
  &.active {
    transform: rotate(90deg);
  }
`
export const Dot = styled.span`
  display: flex;
  width: 6px;
  height: 6px;
  border-radius: 10px;
  margin: 1.5px 0;
  background-color: ${COLORS.WHITE};
  transition: 0.5s;
`
