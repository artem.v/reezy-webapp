import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { injectIntl, intlShape } from 'react-intl'
import { compose } from 'redux'
import { createStructuredSelector } from 'reselect'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import messages from './messages'
import PriceStatus from '../../components/PriceStatus'
import { makeSelectProperties } from '../DashboardPage/selectors'
import {
  Paper,
  PlankTextBold,
  PropertiesHeader,
  PropertyContent,
  PropertyControlWrapper,
  PropertyCostsWrapper,
  PropertyPlank,
  PropertyWrapper,
  Text,
  Title,
  ToggleButton,
  AddPropertyButton,
  ShowMoreButton,
  Dot,
} from './styled'

class Properties extends PureComponent {
  static propTypes = {
    properties: PropTypes.arrayOf(PropTypes.object),
    openPropertyDetails: PropTypes.func.isRequired,
    intl: intlShape.isRequired,
  }

  state = {
    propertiesStatus: 'active',
  }

  toggleProperties = () => {
    this.setState(state => ({
      propertiesStatus: state.propertiesStatus === 'active' ? '' : 'active',
    }))
  }

  renderPropertiesToggleButton = () => (
    <ToggleButton
      onClick={this.toggleProperties}
      className={this.state.propertiesStatus}
    >
      <Dot />
      <Dot />
      <Dot />
    </ToggleButton>
  )

  renderProperty = (property, idx) => (
    <PropertyWrapper key={property.id}>
      {this.renderPropertyPlank(property, idx)}
      {this.renderPropertyCosts(property)}
    </PropertyWrapper>
  )

  renderPropertyCosts = property => (
    <PropertyCostsWrapper>
      <div>
        <Text>{this.props.intl.formatMessage(messages.ytdIncome)}</Text>
        <PriceStatus value={property.income} />
      </div>
      <div>
        <Text>{this.props.intl.formatMessage(messages.ytdCashFlow)}</Text>
        <PriceStatus value={property.cashFlow} />
      </div>
    </PropertyCostsWrapper>
  )

  renderPropertyPlank = (property, idx) => (
    <PropertyPlank
      idx={idx}
      onClick={this.props.openPropertyDetails(property.id)}
    >
      <PlankTextBold>{property.address}</PlankTextBold>
      {/* <SmallPlankText>{props.text}</SmallPlankText>
      <SmallPlankText>{props.date}</SmallPlankText> */}
    </PropertyPlank>
  )

  render() {
    return (
      <Paper>
        <PropertiesHeader className={this.state.propertiesStatus}>
          <Title>{this.props.intl.formatMessage(messages.title)}</Title>
          {this.renderPropertiesToggleButton()}
        </PropertiesHeader>
        <PropertyContent className={this.state.propertiesStatus}>
          {this.props.properties.map(this.renderProperty)}
        </PropertyContent>
        <PropertyControlWrapper>
          <ShowMoreButton to="/properties">
            {this.props.intl.formatMessage(messages.showMore)}
          </ShowMoreButton>
          <AddPropertyButton to="/properties">
            {this.props.intl.formatMessage(messages.addProperty)}
          </AddPropertyButton>
        </PropertyControlWrapper>
      </Paper>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  properties: makeSelectProperties(),
})

const mapDispatchToProps = dispatch => ({
  openPropertyDetails: id => () => dispatch(push(`/properties/${id}`)),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

export default compose(
  withConnect,
  injectIntl
)(Properties)
