import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import moment from 'moment'
import { compose } from 'redux'
import {
  makeSelectProperty,
  makeSelectPaymentHistory,
} from '../PropertyDetailsPage/selectors'
import {
  HistoryEmptyMonth,
  HistoryMonth,
  MonthLabel,
  DateColumn,
  YearLabel,
} from '../PropertyDetailsPage/styled'
import CheckedBlock from '../../components/CheckedBlock'

export class PropertyPaymentHistory extends PureComponent {
  static propTypes = {
    paymentHistory: PropTypes.object,
  }

  render() {
    return (
      <React.Fragment>
        <HistoryMonth>
          {[...Array(12)].map((value, month) => (
            /* eslint-disable-next-line */
            <DateColumn key={month}>
              <MonthLabel>{moment(month + 1, 'M').format('MMM')}</MonthLabel>
            </DateColumn>
          ))}
        </HistoryMonth>
        {Object.entries(this.props.paymentHistory).map(([year, data]) => (
          <HistoryMonth key={year}>
            <YearLabel>{year}</YearLabel>
            {[...Array(12)].map((value, month) => (
              <DateColumn key={`${year}_${month + 1}`}>
                {typeof data[month + 1] === 'undefined' ? (
                  <HistoryEmptyMonth />
                ) : (
                  <CheckedBlock value={data[month + 1]} />
                )}
              </DateColumn>
            ))}
          </HistoryMonth>
        ))}
      </React.Fragment>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  property: makeSelectProperty(),
  paymentHistory: makeSelectPaymentHistory(),
})

const mapDispatchToProps = {}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

export default compose(withConnect)(PropertyPaymentHistory)
