import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { injectIntl, intlShape } from 'react-intl'
import { createStructuredSelector } from 'reselect'
import { compose } from 'redux'
import { connect } from 'react-redux'
import formatter from 'utils/formatter'
import messages from './messages'
import PriceStatus from '../../components/PriceStatus'
import { makeSelectSummary } from '../DashboardPage/selectors'

import {
  Plank,
  PlankPrice,
  PlankText,
  Text,
  Title,
  OffsetLeftBlock,
  OffsetPaper,
} from './styled'

class AccountSummary extends PureComponent {
  static propTypes = {
    intl: intlShape.isRequired,
    summary: PropTypes.shape({
      cashFlow: PropTypes.number,
      income: PropTypes.object,
    }),
  }

  static defaultProps = {
    summary: {
      cashFlow: {},
      income: {},
    },
  }

  render() {
    return (
      <OffsetPaper>
        <OffsetLeftBlock>
          <Title>{this.props.intl.formatMessage(messages.title)}</Title>
        </OffsetLeftBlock>
        <Plank>
          <PlankText>
            {this.props.intl.formatMessage(messages.ytdIncome)}
          </PlankText>
          <PlankPrice>
            {formatter.validPrice(
              `$${formatter.separatePrice(this.props.summary.income.total)}`
            )}
          </PlankPrice>
        </Plank>
        <OffsetLeftBlock>
          <Text style={{ marginTop: 20 }}>
            {this.props.intl.formatMessage(messages.ytdCashFlow)}
          </Text>
          {Object.keys(this.props.summary).length && (
            <PriceStatus value={this.props.summary.cashFlow} />
          )}
        </OffsetLeftBlock>
      </OffsetPaper>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  summary: makeSelectSummary(),
})

const withConnect = connect(
  mapStateToProps,
  null
)

export default compose(
  withConnect,
  injectIntl
)(AccountSummary)
