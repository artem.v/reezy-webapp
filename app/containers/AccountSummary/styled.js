import styled from 'styled-components'

import { COLORS } from '../../constants'

export const Plank = styled.div`
  padding: 20px;
  border-top-right-radius: 2px;
  border-bottom-right-radius: 2px;
  background: linear-gradient(
    to left,
    ${props => `${props.from || COLORS.FROM}, ${props.to || COLORS.TO}`}
  );
`

export const PlankPrice = styled.h3`
  color: ${COLORS.WHITE};
  font-size: 36px;
  line-height: 47px;
  font-weight: 600;
  margin: 0;
`

export const Text = styled.h3`
  font-size: 12px;
  line-height: 14px;
  color: ${COLORS.TEXT};
  margin: 0;
`

export const PlankText = styled(Text)`
  color: ${COLORS.WHITE};
  font-size: 14px;
  line-height: 18px;
`

export const Title = styled.h2`
  color: ${COLORS.TITLE};
  font-size: 18px;
  line-height: 23px;
  font-weight: 600;
  margin-bottom: 20px;
`

export const OffsetLeftBlock = styled.div`
  margin-left: 20px;
`

export const Paper = styled.div`
  padding: 40px 30px 30px 30px;
  background-color: ${COLORS.WHITE};
  box-shadow: 2px 2px 7px 0 rgba(8, 39, 64, 0.1);
  max-width: 800px;
  margin-bottom: 30px;
  border-radius: 2px;
  @media (max-width: 767px) {
    padding: 15px 10px;
  }
`

export const OffsetPaper = styled(Paper)`
  padding-left: 0;
`
