/*
 * SignInPage Messages
 *
 * This contains all the text for the SignInPage component.
 */

import { defineMessages } from 'react-intl'
import { createIntlMessage } from 'common'

const CONTAINER_ID = 'AreaChart'

export default defineMessages({
  ...createIntlMessage(CONTAINER_ID, 'income'),
  ...createIntlMessage(CONTAINER_ID, 'expenses'),
  ...createIntlMessage(CONTAINER_ID, 'cashFlow'),
})
