export const STATUS_COLORS = {
  0: '#858E93',
  1: '#6FCF97',
  2: '#E86066',
}

export const CATEGORIES = {
  1: 'Rent',
  2: 'Mortgage',
  3: 'Repair',
  4: 'Property Taxes',
  5: 'Maintenance fees',
}

export const MONTH = {
  0: 'January',
  1: 'February',
  2: 'March',
  3: 'April',
  4: 'May',
  5: 'June',
  6: 'July',
  7: 'August',
  8: 'September',
  9: 'October',
  10: 'November',
  11: 'December',
}
