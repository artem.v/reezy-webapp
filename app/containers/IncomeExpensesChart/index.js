/**
 *
 * AreaChart
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import ReactHighcharts from 'react-highcharts'
import { injectIntl, intlShape } from 'react-intl'
import formatter from 'utils/formatter'
import moment from 'moment'
import { compose } from 'redux'
import income from '../../images/income.svg'
import expenses from '../../images/expense.svg'
import messages from './messages'
import { COLORS } from '../../constants'

const getLegendOffset = () => {
  if (window.innerWidth <= 380) {
    return 0
  }

  if (window.innerWidth <= 767) {
    return 30
  }

  return 100
}

const generateDateString = data =>
  moment(data)
    .utc()
    .format('MMMM, YYYY')

const getColor = diff => {
  if (diff < 0) {
    return COLORS.FAILED
  }

  if (diff > 0) {
    return COLORS.SUCCESS
  }

  return COLORS.TEXT
}
class AreaChart extends React.Component {
  getConfig = props => ({
    title: {
      text: null,
    },
    credits: {
      enabled: false,
    },
    chart: {
      type: 'areaspline',
      paddingLeft: 0,
    },
    legend: {
      symbolHeight: 0,
      symbolWidth: 0,
      squareSymbol: false,
      alignColumns: true,
      useHTML: true,
      itemDistance: getLegendOffset(),
      labelFormatter: this.renderAreaLegends,
    },
    tooltip: {
      backgroundColor: COLORS.WHITE,
      borderColor: 'black',
      borderRadius: 2,
      borderWidth: 0,
      padding: 10,
      formatter: this.renderTooltipContent,
    },
    plotOptions: {
      allowPointSelect: false,
      line: {
        dataLabels: {
          enabled: true,
        },
      },
      series: {
        marker: {
          enabled: false,

          symbol: 'circle',
          lineWidth: 0.5,
          lineColor: COLORS.CHART_LINE,
          fillColor: COLORS.WHITE,
          radius: 4,
          height: 0,
          width: 0,
          enabledThreshold: false,
        },
      },

      area: {
        stacking: 'normal',

        marker: {
          symbol: 'circle',
          lineWidth: 0.5,
          fillColor: COLORS.WHITE,
          radius: 4,
          height: 0,
          width: 0,
          enabledThreshold: false,
        },
      },
    },
    yAxis: {
      useHTML: true,
      title: {
        enabled: false,
      },
      gridLineWidth: 0,
      labels: {
        formatter() {
          return `<h3 style="color: #858E93; font-size: 14px; line-height: 18px; font-weight: bold;">$ ${
            this.value
          }</h3>`
        },
      },
    },
    xAxis: {
      type: 'datetime',
      dateTimeLabelFormats: {
        month: ' %b',
        year: '%b',
      },
      style: {
        fontSize: '14px',
        lineHeight: '18px',
        fontWeight: 600,
        color: '#858E93',
      },
      title: {},
      opposite: true,
    },
    series: [
      {
        name: this.props.intl.formatMessage(messages.income),
        lineWidth: 3,
        color: '#6FCF97',
        image: income,
        total: props.income.total,
        zIndex: 3,
        data: props.income.data,
        fillColor: {
          linearGradient: {
            x1: 0,
            x2: 0,
            y1: 0,
            y2: 2,
          },
          stops: [[0, 'rgba(111,207,151, 0.5)'], [0.3, 'rgba(111,207,151, 0)']],
        },
      },
      {
        name: this.props.intl.formatMessage(messages.expenses),
        lineWidth: 3,
        zIndex: 2,
        image: expenses,
        total: props.expenses.total,
        color: '#F5C243',

        data: props.expenses.data,
        fillColor: {
          linearGradient: {
            x1: 0,
            x2: 0,
            y1: 0,
            y2: 2,
          },
          stops: [[0, 'rgba(245,194,67, 0.5)'], [0.3, 'rgba(245,194,67, 0)']],
        },
      },
    ],
  })

  renderAreaLegends() {
    return (
      `${'<div style="display: flex;">' +
        '<img class="legend-image" style="width: 30px; height: 30px;" src="'}${
        this.userOptions.image
      }"/><div style="display: flex; flex-direction: column; margin-left: 10px" >` +
      `<div style="display: flex; align-items: center;">` +
      `<span style="width: 5px; height: 5px; border-radius: 50%; background-color:${
        this.color
      }"> </span>` +
      `<span style="margin-left: 5px; font-size: 12px; line-height: 12px; color: #858E93">${
        this.name
      }</span>` +
      `</div><h2 style="font-weight: bold; line-height: 23px; font-size: 18px; color: #082740" >$${formatter
        .separatePrice(this.userOptions.total)
        .replace('-', '')}</span></div></div>`
    )
  }

  renderTooltipContent() {
    const currentData = this.series.userOptions.data[this.point.index][2]
    return `${`
      <h3 style="font-size: 12px;line-height: 14px;color: #858E93;margin-bottom: 12px;">
        ${generateDateString(currentData.date)}
      </h3>
      <br/>
      <span style="visibility: hidden; height: 10px;font-size: 5px">ssss</span>
      <br/>
      <span style="visibility: hidden; height: 10px; font-size: 10px">ssss </span>
      <br />
      <span style="font-weight: bold; font-size: 14px; line-height: 18px; color:
        ${getColor(currentData.diff)}
      ">
        $${currentData.diff}
      </span>
    `}`
  }

  shouldComponentUpdate = props =>
    JSON.stringify(props.summary) !== JSON.stringify(this.props.summary)

  render() {
    return Object.values(this.props.summary).length ? (
      <ReactHighcharts config={this.getConfig(this.props.summary)} />
    ) : null
  }
}

AreaChart.propTypes = {
  intl: intlShape.isRequired,
  summary: PropTypes.object,
}
AreaChart.defaultProps = {
  summary: {
    income: {},
    expenses: {},
  },
}

export default compose(injectIntl)(AreaChart)
