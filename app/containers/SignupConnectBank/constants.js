export const PLAID = {
  PLAID_PUBLIC_KEY: process.env.PLAID_PUBLIC_KEY || '',
  PLAID_PRODUCTS: ['auth', 'transactions'],
  PLAID_ENV: process.env.PLAID_ENV || 'sandbox',
  APP_NAME: 'Reezy',
}
