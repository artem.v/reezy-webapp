/*
 * AccountForm Messages
 *
 * This contains all the text for the AccountForm component.
 */

import { defineMessages } from 'react-intl'
import { createIntlMessage } from 'common'

const SIGNUP_PAGE_ID = 'SignUpPage'

export default defineMessages({
  ...createIntlMessage(SIGNUP_PAGE_ID, 'continue'),
  ...createIntlMessage(SIGNUP_PAGE_ID, 'openPlaidLink'),
  ...createIntlMessage(SIGNUP_PAGE_ID, 'connectingBank'),
  ...createIntlMessage(SIGNUP_PAGE_ID, 'alt'),
})
