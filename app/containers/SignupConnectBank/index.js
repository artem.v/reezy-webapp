import React, { Component } from 'react'
import PlaidLink from 'react-plaid-link'
import { compose } from 'redux'
import { FormattedMessage, injectIntl, intlShape } from 'react-intl'

import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { createStructuredSelector } from 'reselect'
import styled from 'styled-components'
import messages from './messages'
import { PLAID } from './constants'
import SignupErrorContainer from '../SignupErrorContainer'

import {
  makeSelectSignupStep,
  makeSelectSignupSteps,
} from '../../containers/SignupPage/selectors'
import {
  completeTask,
  setStep,
  sendBankAccountPublicToken,
  setServerError,
} from '../../containers/SignupPage/actions'
import {
  FormFooter,
  SubmitButton,
  FormContent,
  FormHeader,
  LabelDiv,
  IconDiv,
} from '../SignupCreateAccount/styled'
import connectingBank from '../../images/signup-connecting.svg'
import { COLORS } from '../../constants'
export const PlaidLinkWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  margin: 55px auto;
  button {
    border: none !important;
    background-color: ${COLORS.PRIMARY} !important;
    border-radius: 2px !important;
    padding: 8px 25px !important;
    color: ${COLORS.WHITE};
    font-size: 16px;
    font-weight: 600;
    line-height: 20px;
    min-height: 40px;
  }
`

class BankAccountConnect extends Component {
  static propTypes = {
    completeTask: PropTypes.func.isRequired,
    intl: intlShape.isRequired,
    sendBankAccountPublicToken: PropTypes.func,
    step: PropTypes.number,
    setStep: PropTypes.func.isRequired,
    setServerError: PropTypes.func,
    steps: PropTypes.arrayOf(
      PropTypes.shape({
        completed: PropTypes.bool,
      })
    ),
  }

  onSuccess = publicToken => {
    this.props.completeTask(1)
    this.props.sendBankAccountPublicToken(publicToken)
    this.props.setServerError('')
  }

  onExit = (error = {}) => {
    if ('display_message' in error) {
      this.props.setServerError(error.display_message)
    }
  }

  backTask = () => {
    if (this.props.step - 1 >= 0) {
      this.props.setStep(this.props.step - 1)
    }
  }
  nextStep = () => {
    this.props.setStep(2)
  }

  render() {
    return (
      <React.Fragment>
        <FormHeader>
          <IconDiv>
            <img
              src={connectingBank}
              alt={this.props.intl.formatMessage(messages.alt)}
            />
          </IconDiv>
          <LabelDiv>
            {this.props.intl.formatMessage(messages.connectingBank)}
          </LabelDiv>
        </FormHeader>
        <SignupErrorContainer />
        <FormContent>
          <PlaidLinkWrapper>
            <PlaidLink
              ref={this.plaidLink}
              clientName={PLAID.APP_NAME}
              env={PLAID.PLAID_ENV}
              product={PLAID.PLAID_PRODUCTS}
              onExit={this.onExit}
              publicKey={PLAID.PLAID_PUBLIC_KEY}
              onSuccess={this.onSuccess}
            >
              {this.props.intl.formatMessage(messages.openPlaidLink)}
            </PlaidLink>
          </PlaidLinkWrapper>
        </FormContent>

        <FormFooter>
          <SubmitButton
            disabled={!this.props.steps[this.props.step].completed}
            onClick={this.nextStep}
          >
            <FormattedMessage {...messages.continue} />
          </SubmitButton>
        </FormFooter>
      </React.Fragment>
    )
  }
}

const mapDispatchToProps = {
  completeTask,
  sendBankAccountPublicToken,
  setStep,
  setServerError,
}

const mapStateToProps = createStructuredSelector({
  steps: makeSelectSignupSteps(),
  step: makeSelectSignupStep(),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

export default compose(
  withConnect,
  injectIntl
)(BankAccountConnect)
