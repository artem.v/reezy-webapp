/**
 *
 * TransactionListContainer
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { compose } from 'redux'
import moment from 'moment'
import injectReducer from 'utils/injectReducer'
import injectSaga from 'utils/injectSaga'
import { injectIntl, intlShape } from 'react-intl'

import messages from './messages'
import {
  CATEGORIES,
  TRANSACTIONS_FILTERS,
  TRANSACTIONS_SORTBY,
} from '../../constants'
import {
  makeSelectTransactionsList,
  makeSelectDateStart,
  makeSelectDateEnd,
  makeSelectShowCategory,
  makeSelectSortBy,
  makeSelectPendingList,
} from './selectors'
import reducer from './reducer'
import {
  selectShowCategory,
  selectSortBy,
  changeStartDate,
  changeEndDate,
  updateTransactionProperty,
  fetchTransactionsList,
} from './actions'
import saga from './saga'
import PriceAmount from '../../components/PriceAmount'
// import SelectControl from '../../components/SelectControl'
import TransactionCategory from '../../components/TransactionCategory'
import DateTimePicker from '../../components/TimePicker'
import {
  TableWrapper,
  TableHeader,
  TabsLabel,
  TabsWrapper,
  TabView,
  TableHeaderLabel,
  TableHeaderLight,
  TableRowLabel,
  TableRow,
  TableContent,
  HeaderSorting,
  LoadMoreButton,
  SmallTableHeaderLabel,
  SmallTableRowLabel,
  Row,
  LoadingRow,
  SmallLoadingBlock,
  LoadingBlock,
  FilterModal,
  FilterModalHeader,
  FilterModalTitle,
  PrimaryButton,
  FilterContent,
  FilterButton,
  MobileTabsLabel,
  EmptyRow,
} from './styled'

class TransactionsList extends React.Component {
  static propTypes = {
    propertyId: PropTypes.number.isRequired,
    selectShowCategory: PropTypes.func,
    selectSortBy: PropTypes.func,
    showCategory: PropTypes.string,
    list: PropTypes.array,
    changeStartDate: PropTypes.func,
    changeEndDate: PropTypes.func,
    fetchTransactionsList: PropTypes.func,
    updateTransactionProperty: PropTypes.func,
    dateStart: PropTypes.string,
    dateEnd: PropTypes.string,
    // sortBy: PropTypes.string,
    pending: PropTypes.bool,
    intl: intlShape.isRequired,
  }

  constructor(props) {
    super(props)
    this.state = {
      filterModalOpen: false,
    }

    this.tabs = Object.keys(TRANSACTIONS_FILTERS).map(filter => ({
      id: filter.toLowerCase(),
      label: props.intl.formatMessage(messages[filter.toLowerCase()]),
    }))

    this.sortByGroup = Object.keys(TRANSACTIONS_SORTBY).map(column => ({
      id: column.toLowerCase(),
      label: props.intl.formatMessage(messages[column.toLowerCase()]),
    }))
  }

  componentDidMount() {
    this.props.fetchTransactionsList(this.props.propertyId)
  }

  componentDidUpdate(prevProps) {
    if (prevProps.propertyId !== this.props.propertyId) {
      this.props.fetchTransactionsList(this.props.propertyId)
    }
  }

  fetchMoreTransactionsList = () => {
    this.props.fetchTransactionsList(this.props.propertyId, true)
  }

  selectShowCategory = id => {
    this.props.selectShowCategory(id, this.props.propertyId)
  }

  selectSortBy = id => {
    this.props.selectSortBy(id, this.props.propertyId)
  }

  renderShowCategory = category => (
    <TabView
      key={category.id}
      onClick={() => this.selectShowCategory(category.id)}
      className={category.id === this.props.showCategory ? 'active' : ''}
    >
      {category.label}
    </TabView>
  )

  changeStartDate = value =>
    this.props.changeStartDate(value, this.props.propertyId)

  changeEndDate = value =>
    this.props.changeEndDate(value, this.props.propertyId)

  formatDate = date => moment(date).format('MMM DD, YYYY')

  updateCategory = transactionId => categoryId => {
    this.props.updateTransactionProperty(categoryId, transactionId)
  }

  renderTransaction = transaction => (
    <TableRow key={transaction.id}>
      <SmallTableRowLabel mobileOrder={2}>
        {this.formatDate(transaction.date)}
      </SmallTableRowLabel>
      <TableRowLabel mobileOrder={1} grow={2}>
        {transaction.name}
      </TableRowLabel>
      <TableRowLabel mobileOrder={3} grow={2}>
        <TransactionCategory
          selected={transaction.categoryId}
          placeholder={this.props.intl.formatMessage(messages.selectCategory)}
          suggestedSelected={transaction.suggestedCategoryId}
          options={Object.entries(CATEGORIES).map(([id, label]) => ({
            id: +id,
            label: this.props.intl.formatMessage(messages[label]),
          }))}
          onSelectCategory={this.updateCategory(transaction.id)}
        />
      </TableRowLabel>
      <SmallTableRowLabel mobileOrder={4}>
        <PriceAmount value={transaction.amount} />
      </SmallTableRowLabel>
    </TableRow>
  )

  renderMobileTransaction = transaction => (
    <TableRow key={transaction.id}>
      <Row>
        <TableRowLabel mobileOrder={1} grow={2}>
          {transaction.name}
        </TableRowLabel>
        <SmallTableRowLabel mobileOrder={2}>
          {this.formatDate(transaction.date)}
        </SmallTableRowLabel>
      </Row>
      <Row>
        <TableRowLabel mobileOrder={3} grow={2}>
          <TransactionCategory
            selected={transaction.categoryId}
            suggestedSelected={transaction.suggestedCategoryId}
            placeholder={this.props.intl.formatMessage(messages.selectCategory)}
            onSelectCategory={this.updateCategory(transaction.id)}
            options={Object.entries(CATEGORIES).map(([id, label]) => ({
              id: +id,
              label: this.props.intl.formatMessage(messages[label]),
            }))}
          />
        </TableRowLabel>
        <SmallTableRowLabel mobileOrder={4}>
          <PriceAmount value={transaction.amount} />
        </SmallTableRowLabel>
      </Row>
    </TableRow>
  )

  renderLoadingBlock = () =>
    window.innerWidth < 767 ? (
      <LoadingRow>
        <Row>
          <LoadingBlock height="14px" />
          <SmallLoadingBlock height="14px" />
        </Row>
        <Row>
          <LoadingBlock height="30px" />
          <SmallLoadingBlock height="30px" />
        </Row>
      </LoadingRow>
    ) : (
      <LoadingRow>
        <SmallLoadingBlock height="14px" />
        <LoadingBlock height="14px" />

        <LoadingBlock height="30px" />
        <SmallLoadingBlock height="30px" />
      </LoadingRow>
    )

  renderTabletFilters = () => (
    <FilterModal>
      <FilterModalHeader>
        <span />
        <FilterModalTitle>
          {this.props.intl.formatMessage(messages.filter)}
        </FilterModalTitle>
        <PrimaryButton onClick={this.closeFilterModal}>
          {this.props.intl.formatMessage(messages.done)}
        </PrimaryButton>
      </FilterModalHeader>
      <FilterContent>
        {/* <Row style={{ marginBottom: 10 }}>
          <MobileTabsLabel>
            {this.props.intl.formatMessage(messages.sortBy)}
          </MobileTabsLabel>
          <SelectControl
            width={115}
            selected={this.props.sortBy}
            confirm={this.selectSortBy}
            options={this.sortByGroup}
          />
        </Row> */}
        <Row>
          <MobileTabsLabel>
            {this.props.intl.formatMessage(messages.dateRange)}
          </MobileTabsLabel>
          <DateTimePicker
            dateStart={this.props.dateStart}
            dateEnd={this.props.dateEnd}
            onStartDateChange={this.changeStartDate}
            onEndDateChange={this.changeEndDate}
          />
        </Row>
      </FilterContent>
    </FilterModal>
  )

  selectSortBy = selected => {
    this.props.selectSortBy(selected.id)
  }

  openFilterModal = () => {
    this.setState({ filterModalOpen: true })
  }

  closeFilterModal = () => {
    this.setState({ filterModalOpen: false })
  }

  render() {
    return (
      <TableWrapper>
        <TableHeader>
          <TabsWrapper>
            <TabsLabel>
              {this.props.intl.formatMessage(messages.show)}
            </TabsLabel>
            {this.tabs.map(this.renderShowCategory)}
          </TabsWrapper>
          <HeaderSorting>
            {/* <TabsLabel>
              {this.props.intl.formatMessage(messages.sortBy)}
            </TabsLabel>
            <SelectControl
              width={115}
              selected={this.props.sortBy}
              confirm={this.selectSortBy}
              options={this.sortByGroup}
            /> */}

            <TabsLabel>
              {this.props.intl.formatMessage(messages.dateRange)}
            </TabsLabel>
            <DateTimePicker
              dateStart={this.props.dateStart}
              dateEnd={this.props.dateEnd}
              onStartDateChange={this.changeStartDate}
              onEndDateChange={this.changeEndDate}
            />
          </HeaderSorting>
          <FilterButton onClick={this.openFilterModal}>
            {this.props.intl.formatMessage(messages.filter)}
          </FilterButton>
          {this.state.filterModalOpen ? this.renderTabletFilters() : null}
        </TableHeader>
        <TableHeaderLight>
          <SmallTableHeaderLabel>
            {this.props.intl.formatMessage(messages.date)}
          </SmallTableHeaderLabel>
          <TableHeaderLabel grow={2}>
            {this.props.intl.formatMessage(messages.description)}
          </TableHeaderLabel>
          <TableHeaderLabel grow={2}>
            {this.props.intl.formatMessage(messages.category)}
          </TableHeaderLabel>
          <SmallTableHeaderLabel>
            {this.props.intl.formatMessage(messages.amount)}
          </SmallTableHeaderLabel>
        </TableHeaderLight>
        <TableContent>
          {!this.props.pending && !this.props.list.length ? (
            <EmptyRow>
              {this.props.intl.formatMessage(messages.noTransactions)}
            </EmptyRow>
          ) : (
            this.props.list.map(
              window.innerWidth < 768
                ? this.renderMobileTransaction
                : this.renderTransaction
            )
          )}
        </TableContent>
        {this.props.pending ? this.renderLoadingBlock() : null}
        {!this.props.pending && this.props.list.length ? (
          <LoadMoreButton onClick={this.fetchMoreTransactionsList}>
            {this.props.intl.formatMessage(messages.loadMore)}
          </LoadMoreButton>
        ) : null}
      </TableWrapper>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  list: makeSelectTransactionsList(),
  sortBy: makeSelectSortBy(),
  showCategory: makeSelectShowCategory(),
  dateStart: makeSelectDateStart(),
  dateEnd: makeSelectDateEnd(),
  pending: makeSelectPendingList(),
})

const mapDispatchToProps = {
  selectShowCategory,
  selectSortBy,
  changeStartDate,
  changeEndDate,
  updateTransactionProperty,
  fetchTransactionsList,
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

const withReducer = injectReducer({ key: 'transactions', reducer })
const withSaga = injectSaga({ key: 'transactions', saga })

export default compose(
  withReducer,
  withSaga,
  injectIntl,
  withConnect
)(TransactionsList)
