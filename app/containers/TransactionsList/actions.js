/*
 *
 * TransactionsList actions
 *
 */

export const FETCH_TRANSACTIONS_LIST =
  'app/TransactionList/FETCH_TRANSACTIONS_LIST'
export const fetchTransactionsList = (propertyId, loadMore = false) => ({
  type: FETCH_TRANSACTIONS_LIST,
  propertyId,
  loadMore,
})

export const FETCH_TRANSACTIONS_LIST_SUCCESS =
  'app/TransactionsList/FETCH_TRANSACTIONS_LIST_SUCCESS'
export const fetchTransactionsListSuccess = list => ({
  type: FETCH_TRANSACTIONS_LIST_SUCCESS,
  list,
})

export const FETCH_TRANSACTIONS_LIST_FAILED =
  'app/TransactionsList/FETCH_TRANSACTIONS_LIST_FAILED'
export const fetchTransactionsListFailed = () => ({
  type: FETCH_TRANSACTIONS_LIST_FAILED,
})

// Load more
export const FETCH_MORE_TRANSACTIONS_LIST_SUCCESS =
  'app/TransactionsList/FETCH_MORE_TRANSACTIONS_LIST_SUCCESS'
export const fetchMoreTransactionsListSuccess = list => ({
  type: FETCH_MORE_TRANSACTIONS_LIST_SUCCESS,
  list,
})

export const FETCH_MORE_TRANSACTIONS_LIST_FAILED =
  'app/TransactionsList/FETCH_MORE_TRANSACTIONS_LIST_FAILED'
export const fetchMoreTransactionsListFailed = () => ({
  type: FETCH_MORE_TRANSACTIONS_LIST_FAILED,
})
// Load more end

export const SELECT_SORT_BY = 'app/TransactionsList/SELECT_SORT_BY'
export const selectSortBy = (value, propertyId) => ({
  type: SELECT_SORT_BY,
  value,
  propertyId,
})

export const CHANGE_START_DATE = 'app/TransactionsList/CHANGE_START_DATE'
export const changeStartDate = (date, propertyId) => ({
  type: CHANGE_START_DATE,
  date,
  propertyId,
})

export const CHANGE_END_DATE = 'app/TransactionsList/CHANGE_END_DATE'
export const changeEndDate = (date, propertyId) => ({
  type: CHANGE_END_DATE,
  date,
  propertyId,
})

export const SELECT_SHOW_CATEGORY =
  'app/TransactionsListPage/SELECT_SHOW_CATEGORY'
export const selectShowCategory = (id, propertyId) => ({
  type: SELECT_SHOW_CATEGORY,
  id,
  propertyId,
})

// update transactions
export const UPDATE_TRANSACTION_PROPERTY =
  'app/TransactionsListPage/UPDATE_TRANSACTION_PROPERTY'
export const updateTransactionProperty = (categoryId, transactionId) => ({
  type: UPDATE_TRANSACTION_PROPERTY,
  categoryId,
  transactionId,
})

export const UPDATE_TRANSACTION_PROPERTY_SUCCESS =
  'app/TransactionsListPage/UPDATE_TRANSACTION_PROPERTY_SUCCESS'
export const updateTransactionPropertySuccess = (
  transactionId,
  categoryId
) => ({
  type: UPDATE_TRANSACTION_PROPERTY_SUCCESS,
  transactionId,
  categoryId,
})

export const UPDATE_TRANSACTION_PROPERTY_FAILED =
  'app/TransactionsListPage/UPDATE_TRANSACTION_PROPERTY_FAILED'
export const updateTransactionPropertyFailed = id => ({
  type: UPDATE_TRANSACTION_PROPERTY_FAILED,
  id,
})
