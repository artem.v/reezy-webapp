import { createSelector } from 'reselect'
import { initialState } from './reducer'

/**
 * Direct selector to the transactionsPage state domain
 */

const selectTransactions = state => state.transactions || initialState

const makeSelectTransactionsList = () =>
  createSelector(
    selectTransactions,
    transactionsState => transactionsState.list
  )

const makeSelectSortBy = () =>
  createSelector(
    selectTransactions,
    transactionsState => transactionsState.sortBy
  )
const makeSelectShowCategory = () =>
  createSelector(
    selectTransactions,
    transactionsState => transactionsState.showCategory
  )

const makeSelectDateStart = () =>
  createSelector(
    selectTransactions,
    transactionsState => transactionsState.startDate
  )

const makeSelectDateEnd = () =>
  createSelector(
    selectTransactions,
    transactionsState => transactionsState.endDate
  )

const makeSelectPendingList = () =>
  createSelector(
    selectTransactions,
    transactionsState => transactionsState.pending
  )

export {
  makeSelectTransactionsList,
  makeSelectDateStart,
  makeSelectDateEnd,
  makeSelectShowCategory,
  makeSelectSortBy,
  makeSelectPendingList,
}
