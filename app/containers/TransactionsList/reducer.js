/*
 *
 * TransactionPage reducer
 *
 */

import Immutable from 'seamless-immutable'
import moment from 'moment'

import {
  FETCH_TRANSACTIONS_LIST,
  FETCH_TRANSACTIONS_LIST_SUCCESS,
  FETCH_TRANSACTIONS_LIST_FAILED,
  FETCH_MORE_TRANSACTIONS_LIST_SUCCESS,
  FETCH_MORE_TRANSACTIONS_LIST_FAILED,
  SELECT_SHOW_CATEGORY,
  CHANGE_END_DATE,
  CHANGE_START_DATE,
  SELECT_SORT_BY,
} from './actions'

import { DATE_FORMAT, TRANSACTIONS_FILTERS } from '../../constants'

export const initialState = Immutable({
  propertyId: null,
  list: [],
  pending: false,
  page: 1,
  sortBy: '',
  startDate: moment()
    .subtract(12, 'months')
    .format(DATE_FORMAT),
  endDate: moment().format(DATE_FORMAT),
  showCategory: TRANSACTIONS_FILTERS.RECOGNIZED.toLowerCase(),
})

function transactionsReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_TRANSACTIONS_LIST:
      return state
        .set('list', action.loadMore ? state.list : [])
        .set('pending', true)
    case FETCH_TRANSACTIONS_LIST_SUCCESS:
      return state
        .set('list', action.list)
        .set('page', 1)
        .set('pending', false)
    case FETCH_TRANSACTIONS_LIST_FAILED:
      return state.set('pending', false)

    case FETCH_MORE_TRANSACTIONS_LIST_SUCCESS:
      return state
        .set('list', [...state.list, ...action.list])
        .set('page', state.page + 1)
        .set('pending', false)
    case FETCH_MORE_TRANSACTIONS_LIST_FAILED:
      return state.set('pending', false)

    case SELECT_SHOW_CATEGORY:
      return state
        .set('list', [])
        .set('showCategory', action.id)
        .set('pending', true)
        .set('page', 1)
    case CHANGE_START_DATE:
      return state
        .set('startDate', action.date)
        .set('page', 1)
        .set('pending', true)
    case CHANGE_END_DATE:
      return state
        .set('endDate', action.date)
        .set('page', 1)
        .set('pending', true)
    case SELECT_SORT_BY:
      return state
        .set('sortBy', action.value)
        .set('page', 1)
        .set('pending', true)
    default:
      return state
  }
}

export default transactionsReducer
