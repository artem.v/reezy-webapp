import {
  takeLatest,
  takeEvery,
  all,
  call,
  put,
  select,
} from 'redux-saga/effects'
import moment from 'moment'
import { delay } from 'redux-saga'
import {
  fetchTransactionsListSuccess,
  fetchTransactionsListFailed,
  FETCH_TRANSACTIONS_LIST,
  updateTransactionPropertyFailed,
  UPDATE_TRANSACTION_PROPERTY,
  SELECT_SHOW_CATEGORY,
  SELECT_SORT_BY,
  CHANGE_END_DATE,
  CHANGE_START_DATE,
  fetchMoreTransactionsListSuccess,
} from './actions'

import api from '../../api'

import { PER_PAGE, DATE_FORMAT, API_DATE_FORMAT } from '../../constants'

export function* fetchTransactionList({ propertyId, loadMore }) {
  try {
    const state = yield select()
    const {
      showCategory,
      sortBy,
      startDate,
      endDate,
      page,
    } = state.transactions

    const queryKeys = {
      filter: showCategory,
      sort_by: sortBy,
      start_date: moment(startDate, DATE_FORMAT).format(API_DATE_FORMAT),
      end_date: moment(endDate, DATE_FORMAT).format(API_DATE_FORMAT),
      page: loadMore ? page + 1 : 1,
      size: PER_PAGE,
    }
    const query = {}

    Object.entries(queryKeys)
      .filter(([, value]) => value)
      .forEach(([key, value]) => {
        query[key] = value
      })

    const response = yield call(api.getPropertyTransactions, propertyId, query)

    yield delay(2000)

    if (response.ok) {
      if (loadMore) {
        yield put(fetchMoreTransactionsListSuccess(response.data.transactions))
      } else {
        yield put(fetchTransactionsListSuccess(response.data.transactions))
      }
    } else {
      yield put(fetchTransactionsListFailed(response.problem))
    }
  } catch (error) {
    yield put(fetchTransactionsListFailed('Unexpected error'))
  }
}

export function* updateTransactionProperty({ transactionId, categoryId }) {
  const state = yield select()
  const { list, propertyId } = state.transactions

  try {
    const response = yield call(api.updateTransactionsProperty, {
      transactionId,
      propertyId,
      categoryId,
    })

    const transactions = list.map(transaction => {
      if (transaction.id === transactionId) {
        return {
          ...transaction,
          categoryId,
        }
      }

      return transaction
    })

    if (response.ok) {
      yield put(fetchTransactionsListSuccess(transactions))
    } else {
      yield put(updateTransactionPropertyFailed(response.problem))
    }
  } catch (error) {
    yield put(updateTransactionPropertyFailed('Unexpected error'))
  }
}

export default function* defaultSaga() {
  yield all([
    yield takeLatest(
      [
        FETCH_TRANSACTIONS_LIST,
        SELECT_SHOW_CATEGORY,
        CHANGE_START_DATE,
        CHANGE_END_DATE,
        SELECT_SORT_BY,
      ],
      fetchTransactionList
    ),
    yield takeEvery(UPDATE_TRANSACTION_PROPERTY, updateTransactionProperty),
  ])
}
