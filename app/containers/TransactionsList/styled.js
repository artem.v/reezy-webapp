import styled from 'styled-components'
import { COLORS } from '../../constants'

export const TableWrapper = styled.div`
  display: flex;
  flex-direction: column;
  border-radius: 2px;
  flex-grow: 1;
  box-shadow: 2px 2px 9px 0 rgba(95, 95, 95, 0.209239);
  @media (max-width: 767px) {
    box-shadow: none;
  }
`
export const TableHeader = styled.header`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 20px;
  width: 100%;
  position: relative;
  background-color: ${COLORS.WHITE};
  @media (max-width: 767px) {
    padding: 0;
    margin-bottom: 8px;
    box-shadow: 2px 2px 9px 0 rgba(95, 95, 95, 0.209239);
  }
`
export const TableContent = styled.div`
  display: flex;
  flex-direction: column;
  background-color: ${COLORS.WHITE};
  @media (max-width: 767px) {
    box-shadow: 2px 2px 9px 0 rgba(95, 95, 95, 0.209239);
  }
`

export const TableHeaderLight = styled(TableHeader)`
  background-color: ${COLORS.LIGHT};
  @media (max-width: 767px) {
    display: none;
  }
`

export const HeaderSorting = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  @media (max-width: 991px) {
    display: none;
  }
`
export const TableRow = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 20px;
  width: 100%;
  background-color: ${COLORS.WHITE};
  border: 1px solid ${COLORS.WHITE};
  border-bottom: 1px solid ${COLORS.LIGHT};
  flex-wrap: nowrap;
  &:hover {
    border: 1px solid ${COLORS.PRIMARY};
  }
  @media (max-width: 767px) {
    padding: 15px 15px 0;
    flex-wrap: wrap;
  }
`
export const TableHeaderLabel = styled.h3`
  font-size: 16px;
  line-height: 18px;
  color: ${COLORS.TITLE};
  padding: 0 20px;
  margin: 0;
  display: flex;
  width: 100%;
  flex-grow: ${props => props.grow || 1};
`

export const SmallTableHeaderLabel = styled.h3`
  font-size: 16px;
  line-height: 18px;
  color: ${COLORS.TITLE};
  padding: 0 5px;
  max-width: 95px;
  min-width: 95px;
  text-align: left;
  margin: 0;
  display: flex;
  flex-grow: ${props => props.grow || 1};
`
export const TableRowLabel = styled.h3`
  font-size: 14px;
  line-height: 17px;
  color: ${COLORS.TEXT};
  padding: 0 20px;
  margin: 0;
  width: 100%;
  display: flex;
  flex-grow: ${props => props.grow || 1};
  @media (max-width: 767px) {
    order: ${props => props.mobileOrder || 'initial'};
    margin-bottom: 15px;
    padding: 0 0 0 0;
    color: ${COLORS.TITLE};
  }
`

export const SmallTableRowLabel = styled(TableRowLabel)`
  max-width: 95px;
  min-width: 95px;
  padding: 0 5px;
  width: initial;
  @media (max-width: 767px) {
    color: ${COLORS.TEXT};
  }
`

export const LoadingRow = styled(TableRow)`
  position: relative;

  &:hover {
    border: 1px solid #ffffff;
    border-bottom: 1px solid #f3f4f5;
  }

  /* Shine */
  &:after {
    content: '';
    top: 0;
    transform: translateX(100%);
    width: 100%;
    height: 72px;
    position: absolute;
    z-index: 1;
    animation: slide 1s infinite 0.5s;

    background: linear-gradient(
      to right,
      rgba(255, 255, 255, 0) 0%,
      rgba(255, 255, 255, 0.8) 50%,
      rgba(128, 186, 232, 0) 99%,
      rgba(125, 185, 232, 0) 100%
    );
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00ffffff', endColorstr='#007db9e8',GradientType=1 ); /* IE6-9 */
  }

  /* animation */

  @keyframes slide {
    0% {
      transform: translateX(-100%);
    }
    100% {
      transform: translateX(100%);
    }
  }

  & > :first-child {
    margin-left: 30px;
  }
  @media (max-width: 767px) {
    & > :first-child {
      margin-left: 0px;
    }
  }
`

export const LoadingBlock = styled(TableRowLabel)`
  background: linear-gradient(90deg, #e0e0e0 0%, #f3f4f5 100%);
  height: ${props => props.height || 'initial'};
  margin-left: 30px;
  @media (max-width: 767px) {
    margin-left: 0;
  }
`

export const SmallLoadingBlock = styled(SmallTableRowLabel)`
  background: linear-gradient(90deg, #e0e0e0 0%, #f3f4f5 100%);
  height: ${props => props.height || 'initial'};
  margin-left: 30px;
  @media (max-width: 767px) {
    margin-left: 40px;
  }
`

export const TabsWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
`
export const TabsLabel = styled.h3`
  font-size: 14px;
  line-height: 17px;
  color: ${COLORS.TITLE};
  padding: 0 20px;
  margin: 0;
  @media (max-width: 767px) {
    display: none;
  }
`
export const MobileTabsLabel = styled(TabsLabel)`
  @media (max-width: 767px) {
    display: inline-flex;
  }
`
export const TabView = styled.button`
  border-radius: 2px;
  display: flex;
  margin: 0 5px;
  color: ${COLORS.TEXT};
  padding: 8px 13px;
  background-color: ${COLORS.WHITE};
  font-size: 12px;
  border: 1px solid ${COLORS.TEXT};
  cursor: pointer;
  &:hover {
    border-color: ${COLORS.PRIMARY};
  }
  &.active {
    background-color: ${COLORS.PRIMARY};
    color: ${COLORS.WHITE};
    border-color: ${COLORS.PRIMARY};
  }
  @media (max-width: 767px) {
    border: none;
    &.active {
      color: ${COLORS.PRIMARY};
      background-color: ${COLORS.WHITE};
      border-radius: 0;
      border-bottom: 3px solid ${COLORS.PRIMARY};
      margin: 0;
    }
  }
`

export const LoadMoreButton = styled.button`
  font-weight: 600;
  color: ${COLORS.WHITE};
  background-color: ${COLORS.PRIMARY};
  font-size: 18px;
  line-height: 21px;
  border-radius: 2px;
  padding: 10px 40px;
  margin: 40px auto 20px;
  @media (max-width: 767px) {
    padding: 8px 24px;
    font-size: 16px;
    line-height: 18px;
    margin: 17px auto 17px;
  }
`

export const Row = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  justify-content: space-between;
`

export const EmptyRow = styled(Row)`
  width: 100%;
  justify-content: center;
  padding: 20px;
`

export const PrimaryButton = styled.button`
  border: 1px solid ${COLORS.PRIMARY};
  background-color: ${COLORS.PRIMARY};
  color: ${COLORS.WHITE};
  font-size: 12px;
  text-align: center;
  padding: 8px 14px;
  border-radius: 2px;
  display: inline-flex;
`
export const FilterButton = styled(PrimaryButton)`
  display: none;
  @media (max-width: 991px) {
    display: inline-flex;
  }
`

export const FilterModal = styled.div`
  position: absolute;
  right: 15px;
  top: 15px;
  border: 1px solid ${COLORS.BACKGROUND};
  width: 100%;
  min-height: 395px;
  background-color: ${COLORS.WHITE};
  z-index: 1;
  @media (max-width: 767px) {
    right: 0;
    top: 0;
    left: 0;

    max-width: initial;
  }
`

export const FilterModalHeader = styled.header`
  display: flex;
  align-items: center;
  justify-content: space-between;
  background-color: ${COLORS.PRIMARY};
`

export const FilterModalTitle = styled.h2`
  margin: 0;
  font-size: 14px;
  line-height: 17px;
  color: ${COLORS.WHITE};
  font-weight: 600;
`

export const FilterContent = styled.div`
  padding: 13px 20px;
`
