/*
 * TransactionPage Messages
 *
 * This contains all the text for the TransactionPage component.
 */

import { defineMessages } from 'react-intl'
import { createIntlMessage } from 'common'

const PAGE_ID = 'transactionsList'

export default defineMessages({
  ...createIntlMessage(PAGE_ID, 'all'),
  ...createIntlMessage(PAGE_ID, 'recognized'),
  ...createIntlMessage(PAGE_ID, 'income'),
  ...createIntlMessage(PAGE_ID, 'expenses'),

  ...createIntlMessage(PAGE_ID, 'show'),
  ...createIntlMessage(PAGE_ID, 'sortBy'),
  ...createIntlMessage(PAGE_ID, 'dateRange'),
  ...createIntlMessage(PAGE_ID, 'date'),
  ...createIntlMessage(PAGE_ID, 'description'),
  ...createIntlMessage(PAGE_ID, 'category'),
  ...createIntlMessage(PAGE_ID, 'amount'),
  ...createIntlMessage(PAGE_ID, 'loadMore'),
  ...createIntlMessage(PAGE_ID, 'rent'),
  ...createIntlMessage(PAGE_ID, 'mortgage'),
  ...createIntlMessage(PAGE_ID, 'repair'),
  ...createIntlMessage(PAGE_ID, 'propertyTaxes'),
  ...createIntlMessage(PAGE_ID, 'maintenanceFees'),
  ...createIntlMessage(PAGE_ID, 'seemsLike'),
  ...createIntlMessage(PAGE_ID, 'selectCategory'),
  ...createIntlMessage(PAGE_ID, 'filter'),
  ...createIntlMessage(PAGE_ID, 'done'),
  ...createIntlMessage(PAGE_ID, 'noTransactions'),
})
