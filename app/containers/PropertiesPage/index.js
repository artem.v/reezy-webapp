import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { injectIntl, intlShape } from 'react-intl'
import { compose } from 'redux'
import injectSaga from 'utils/injectSaga'
import injectReducer from 'utils/injectReducer'
import reducer from './reducer'
import saga from './saga'
import injectSession from '../CheckSessionHOC'

import Header from '../Header'
import PriceAmount from '../../components/PriceAmount'
import Checkbox from '../../components/Forms/Checkbox'

import {
  makeSelectPropertiesList,
  makeSelectSelectedPropertiesList,
  makeSelectPropertyAddress,
} from './selectors'

import {
  fetchPropertiesList,
  deletePropertiesList,
  selectAllProperties,
  toggleProperty,
  setPropertyAddress,
  createProperty,
  unCheckSelected,
} from './actions'

import createIcon from '../../images/createIcon.svg'
import TrashIcon from '../../images/trash.svg'
import AlertIcon from '../../images/alert.svg'
import ArrowsIcon from '../../images/arrows.svg'
import MinusIcon from '../../images/minus.png'

import {
  Wrapper,
  Content,
  PageHeader,
  PageTitle,
} from '../../components/styled'

import {
  PrimaryButton,
  Button,
  TableHeader,
  TableWrapper,
  TableHeaderLight,
  SmallTableHeaderLabel,
  TableHeaderLabel,
  SelectControlWrapper,
  TableContent,
  AddPropertyButton,
  TableRow,
  SmallTableRowLabel,
  TableRowLabel,
  Modal,
  FormHeader,
  IconDiv,
  LabelDiv,
  FormContent,
  ModalLabel,
  ModalBackdrop,
  PropertyLinksWrapper,
  ModalWrapper,
  ModalInput,
  CreatePropertyButton,
  Icon,
  ButtonLink,
  AddressLink,
  CloseModal,
} from './styled'

import messages from './messages'

export class PropertiesPage extends Component {
  static propTypes = {
    fetchPropertiesList: PropTypes.func,
    intl: intlShape.isRequired,
    deletePropertiesList: PropTypes.func,
    properties: PropTypes.array,
    selected: PropTypes.arrayOf(PropTypes.number),
    selectAllProperties: PropTypes.func,
    toggleProperty: PropTypes.func,
    setPropertyAddress: PropTypes.func,
    address: PropTypes.string,
    createProperty: PropTypes.func,
    unCheckSelected: PropTypes.func,
  }

  state = {
    openCreateModal: false,
  }

  componentDidMount = () => {
    this.fetchProperties()
  }

  fetchProperties = () => {
    this.props.fetchPropertiesList()
  }

  selectAll = () => {
    this.props.selectAllProperties()
  }

  unCheckSelected = () => {
    this.props.unCheckSelected()
  }

  removeSelected = () => {
    this.props.deletePropertiesList()
  }

  setPropertyAddress = event => {
    this.props.setPropertyAddress(event.target.value)
  }

  toggleProperty = id => this.props.toggleProperty(id)

  openCreateModal = () => this.setState({ openCreateModal: true })

  closeCreateModal = () => this.setState({ openCreateModal: false })

  createProperty = () => {
    this.props.createProperty()
    this.closeCreateModal()
  }

  renderCreateModal = () => (
    <ModalWrapper>
      <ModalBackdrop onClick={this.closeCreateModal} />

      <Modal>
        <CloseModal onClick={this.closeCreateModal} />
        <FormHeader>
          <IconDiv>
            <img
              src={createIcon}
              alt={this.props.intl.formatMessage(messages.alt)}
            />
          </IconDiv>
          <LabelDiv>
            {this.props.intl.formatMessage(messages.addProperty)}
          </LabelDiv>
        </FormHeader>
        <FormContent>
          <ModalLabel>
            {this.props.intl.formatMessage(messages.propertyAddress)}
          </ModalLabel>
          <ModalInput
            value={this.props.address}
            onChange={this.setPropertyAddress}
          />
          <CreatePropertyButton onClick={this.createProperty}>
            {this.props.intl.formatMessage(messages.create)}
          </CreatePropertyButton>
        </FormContent>
      </Modal>
    </ModalWrapper>
  )

  render() {
    const { selected } = this.props

    return (
      <Wrapper>
        <Header />
        <Content>
          <PageHeader>
            <PageTitle>
              {this.props.intl.formatMessage(messages.title)}
            </PageTitle>
          </PageHeader>
          <TableWrapper>
            <TableHeader>
              <SelectControlWrapper>
                <PrimaryButton
                  style={{ marginRight: 10 }}
                  onClick={this.selectAll}
                >
                  {this.props.intl.formatMessage(messages.selectAll)}
                </PrimaryButton>
                <Button onClick={this.removeSelected}>
                  <Icon src={TrashIcon} />
                  {this.props.intl.formatMessage(messages.removeSelected)}
                </Button>
              </SelectControlWrapper>
              <AddPropertyButton onClick={this.openCreateModal}>
                {this.props.intl.formatMessage(messages.addProperty)}
              </AddPropertyButton>
            </TableHeader>
            <TableHeaderLight>
              <SmallTableHeaderLabel>
                <Checkbox
                  icon={MinusIcon}
                  checked={Boolean(this.props.selected.length)}
                  onChange={this.unCheckSelected}
                />
              </SmallTableHeaderLabel>
              <TableHeaderLabel grow={2}>
                {this.props.intl.formatMessage(messages.address)}
              </TableHeaderLabel>
              <TableHeaderLabel grow={2}>
                {this.props.intl.formatMessage(messages.rentAmount)}
              </TableHeaderLabel>

              <TableHeaderLabel grow={2} />
            </TableHeaderLight>
            <TableContent>
              {this.props.properties.map(property => (
                <TableRow key={property.id}>
                  <SmallTableRowLabel>
                    <Checkbox
                      onChange={() => this.toggleProperty(property.id)}
                      checked={selected.includes(property.id)}
                    />
                  </SmallTableRowLabel>
                  <TableRowLabel>
                    <AddressLink to={`/properties/${property.id}`}>
                      {property.address}
                    </AddressLink>
                  </TableRowLabel>
                  <TableRowLabel>
                    {property.lease ? (
                      <PriceAmount value={property.lease.rent_amount} />
                    ) : (
                      'No tenant'
                    )}
                  </TableRowLabel>
                  <PropertyLinksWrapper>
                    <ButtonLink to={`/properties/${property.id}`}>
                      <Icon src={AlertIcon} />
                      {this.props.intl.formatMessage(messages.details)}
                    </ButtonLink>

                    <ButtonLink
                      style={{ marginLeft: 10 }}
                      to={`/transactions/${property.id}`}
                    >
                      <Icon src={ArrowsIcon} />
                      {this.props.intl.formatMessage(messages.transactions)}
                    </ButtonLink>
                  </PropertyLinksWrapper>
                </TableRow>
              ))}
            </TableContent>
          </TableWrapper>
          {this.state.openCreateModal ? this.renderCreateModal() : null}
        </Content>
      </Wrapper>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  properties: makeSelectPropertiesList(),
  selected: makeSelectSelectedPropertiesList(),
  address: makeSelectPropertyAddress(),
})

const mapDispatchToProps = {
  fetchPropertiesList,
  deletePropertiesList,
  selectAllProperties,
  toggleProperty,
  createProperty,
  setPropertyAddress,
  unCheckSelected,
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

const withReducer = injectReducer({ key: 'propertiesPage', reducer })
const withSaga = injectSaga({ key: 'propertiesPage', saga })
const withSession = injectSession()

export default compose(
  withConnect,
  withSaga,
  withReducer,
  withSession,
  injectIntl
)(PropertiesPage)
