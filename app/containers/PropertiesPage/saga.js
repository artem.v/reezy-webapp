import { takeLatest, all, call, put, select } from 'redux-saga/effects'

import {
  FETCH_PROPERTIES_LIST,
  DELETE_PROPERTIES_LIST,
  fetchPropertiesListSuccess,
  fetchPropertiesListFailed,
  deletePropertiesListSuccess,
  deletePropertiesListFailed,
  setSelectedPropertiesList,
  SELECT_ALL_PROPERTIES,
  TOGGLE_PROPERTY,
  CREATE_PROPERTY,
  createPropertySuccess,
  createPropertyFailed,
} from './actions'
import { initialState } from './reducer'
import api from '../../api'

const selectPropertiesPageDomain = state => state.propertiesPage || initialState

export function* fetchPropertiesList() {
  try {
    const response = yield call(api.getPropertiesList)

    if (response.ok) {
      const properties = yield Promise.all(
        response.data.response.map(async property => {
          const responsePropertyLeases = await api.getPropertyLeases(
            property.id
          )

          if (
            responsePropertyLeases.ok &&
            Array.isArray(responsePropertyLeases.data.response)
          ) {
            const propertyLeases =
              responsePropertyLeases.data.response[
                responsePropertyLeases.data.response.length - 1
              ]

            if (propertyLeases) {
              return {
                ...property,
                lease: propertyLeases,
              }
            }
          }

          return property
        })
      )

      yield put(fetchPropertiesListSuccess(properties))
    } else {
      yield put(fetchPropertiesListFailed(response.problem))
    }
  } catch (error) {
    yield put(fetchPropertiesListFailed())
  }
}

export function* deletePropertiesList() {
  try {
    const state = yield select(selectPropertiesPageDomain)
    const deleted = []

    yield Promise.all(
      state.selected.map(async propertyId => {
        const response = await api.deleteProperties(propertyId)

        if (response.ok) {
          deleted.push(propertyId)
        }
      })
    )
    const list = state.properties.filter(
      property => !deleted.includes(property.id)
    )

    yield put(deletePropertiesListSuccess())
    yield put(fetchPropertiesListSuccess(list))
  } catch (error) {
    yield put(deletePropertiesListFailed())
  }
}

export function* toggleProperty(action) {
  try {
    const { id } = action
    const state = yield select(selectPropertiesPageDomain)

    let selected = [...state.selected]

    if (selected.includes(id)) {
      selected = selected.filter(propertyId => propertyId !== id)
    } else {
      selected.push(id)
    }

    yield put(setSelectedPropertiesList(selected))
  } catch (error) {
    yield put(deletePropertiesListFailed())
  }
}

export function* selectAllProperties() {
  try {
    const state = yield select(selectPropertiesPageDomain)

    yield put(
      setSelectedPropertiesList(state.properties.map(property => property.id))
    )
  } catch (error) {
    yield put(deletePropertiesListFailed())
  }
}

export function* createProperty() {
  try {
    const state = yield select(selectPropertiesPageDomain)

    const response = yield call(api.createProperty, { address: state.address })

    if (response.ok) {
      const properties = [
        ...state.properties,
        { id: response.data.response.id, address: state.address },
      ]

      yield put(createPropertySuccess())
      yield put(fetchPropertiesListSuccess(properties))
    } else {
      yield put(createPropertyFailed(response.problem))
    }
  } catch (error) {
    yield put(createPropertyFailed())
  }
}

// Individual exports for testing
export default function* defaultSaga() {
  yield all([
    yield takeLatest(FETCH_PROPERTIES_LIST, fetchPropertiesList),
    yield takeLatest(DELETE_PROPERTIES_LIST, deletePropertiesList),
    yield takeLatest(SELECT_ALL_PROPERTIES, selectAllProperties),
    yield takeLatest(TOGGLE_PROPERTY, toggleProperty),
    yield takeLatest(CREATE_PROPERTY, createProperty),
  ])
}
