/*
 * PropertiesPage Messages
 *
 * This contains all the text for the Properties component.
 */

import { defineMessages } from 'react-intl'
import { createIntlMessage } from 'common'

const PAGE_ID = 'PropertiesPage'

export default defineMessages({
  ...createIntlMessage(PAGE_ID, 'title'),
  ...createIntlMessage(PAGE_ID, 'addProperty'),
  ...createIntlMessage(PAGE_ID, 'removeSelected'),
  ...createIntlMessage(PAGE_ID, 'selectAll'),
  ...createIntlMessage(PAGE_ID, 'address'),
  ...createIntlMessage(PAGE_ID, 'rentAmount'),
  ...createIntlMessage(PAGE_ID, 'details'),
  ...createIntlMessage(PAGE_ID, 'transactions'),
  ...createIntlMessage(PAGE_ID, 'addNew'),
  ...createIntlMessage(PAGE_ID, 'alt'),
  ...createIntlMessage(PAGE_ID, 'propertyAddress'),
  ...createIntlMessage(PAGE_ID, 'create'),
})
