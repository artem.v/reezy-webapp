/*
 *
 * PropertiesPage actions
 *
 */

export const FETCH_PROPERTIES_LIST = 'app/PropertiesPage/FETCH_PROPERTIES_LIST'
export const fetchPropertiesList = () => ({
  type: FETCH_PROPERTIES_LIST,
})

export const FETCH_PROPERTIES_LIST_SUCCESS =
  'app/PropertiesPage/FETCH_PROPERTIES_LIST_SUCCESS'
export const fetchPropertiesListSuccess = properties => ({
  type: FETCH_PROPERTIES_LIST_SUCCESS,
  properties,
})

export const FETCH_PROPERTIES_LIST_FAILED =
  'app/PropertiesPage/FETCH_PROPERTIES_LIST_FAILED'
export const fetchPropertiesListFailed = user => ({
  type: FETCH_PROPERTIES_LIST_FAILED,
  user,
})

// delete properties

export const DELETE_PROPERTIES_LIST =
  'app/PropertiesPage/DELETE_PROPERTIES_LIST'
export const deletePropertiesList = () => ({
  type: DELETE_PROPERTIES_LIST,
})

export const DELETE_PROPERTIES_LIST_SUCCESS =
  'app/PropertiesPage/DELETE_PROPERTIES_LIST_SUCCESS'
export const deletePropertiesListSuccess = () => ({
  type: DELETE_PROPERTIES_LIST_SUCCESS,
})

export const DELETE_PROPERTIES_LIST_FAILED =
  'app/PropertiesPage/DELETE_PROPERTIES_LIST_FAILED'
export const deletePropertiesListFailed = () => ({
  type: DELETE_PROPERTIES_LIST_FAILED,
})

// set selected
export const SET_SELECTED_PROPERTIES_LIST =
  'app/PropertiesPage/SET_SELECTED_PROPERTIES_LIST'
export const setSelectedPropertiesList = selected => ({
  type: SET_SELECTED_PROPERTIES_LIST,
  selected,
})

export const SELECT_ALL_PROPERTIES = 'app/PropertiesPage/SELECT_ALL_PROPERTIES'
export const selectAllProperties = () => ({
  type: SELECT_ALL_PROPERTIES,
})

export const TOGGLE_PROPERTY = 'app/PropertiesPage/TOGGLE_PROPERTY'
export const toggleProperty = id => ({
  type: TOGGLE_PROPERTY,
  id,
})

export const SET_PROPERTY_ADDRESS = 'app/PropertiesPage/SET_PROPERTY_ADDRESS'
export const setPropertyAddress = value => ({
  type: SET_PROPERTY_ADDRESS,
  value,
})

// create property

export const CREATE_PROPERTY = 'app/PropertiesPage/CREATE_PROPERTY'
export const createProperty = () => ({
  type: CREATE_PROPERTY,
})

export const CREATE_PROPERTY_SUCCESS =
  'app/PropertiesPage/CREATE_PROPERTY_SUCCESS'
export const createPropertySuccess = () => ({
  type: CREATE_PROPERTY_SUCCESS,
})

export const CREATE_PROPERTY_FAILED =
  'app/PropertiesPage/CREATE_PROPERTY_FAILED'
export const createPropertyFailed = () => ({
  type: CREATE_PROPERTY_FAILED,
})

export const UN_CHECK_SELECTED = 'app/PropertiesPage/UN_CHECK_SELECTED'
export const unCheckSelected = () => ({
  type: UN_CHECK_SELECTED,
})
