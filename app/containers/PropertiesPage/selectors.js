import { createSelector } from 'reselect'
import { initialState } from './reducer'

/**
 * Direct selector to the propertiesPage state domain
 */

const selectPropertiesPageDomain = state => state.propertiesPage || initialState

const makeSelectPropertiesList = () =>
  createSelector(selectPropertiesPageDomain, state => state.properties)

const makeSelectSelectedPropertiesList = () =>
  createSelector(selectPropertiesPageDomain, state => state.selected)
const makeSelectPropertyAddress = () =>
  createSelector(selectPropertiesPageDomain, state => state.address)

export {
  makeSelectPropertiesList,
  makeSelectSelectedPropertiesList,
  makeSelectPropertyAddress,
}
