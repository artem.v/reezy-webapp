/*
 *
 * PropertiesPage reducer
 *
 */

import Immutable from 'seamless-immutable'
import {
  FETCH_PROPERTIES_LIST,
  FETCH_PROPERTIES_LIST_SUCCESS,
  SET_SELECTED_PROPERTIES_LIST,
  DELETE_PROPERTIES_LIST_SUCCESS,
  SET_PROPERTY_ADDRESS,
  CREATE_PROPERTY_SUCCESS,
  UN_CHECK_SELECTED,
} from './actions'

export const initialState = Immutable({
  properties: [],
  selected: [],
  address: '',
})

function propertiesPageReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_PROPERTIES_LIST:
      return state.set('properties', [])

    case FETCH_PROPERTIES_LIST_SUCCESS:
      return state.set('properties', action.properties)

    case SET_SELECTED_PROPERTIES_LIST:
      return state.set('selected', action.selected)

    case DELETE_PROPERTIES_LIST_SUCCESS:
      return state.set('selected', [])
    case SET_PROPERTY_ADDRESS:
      return state.set('address', action.value)
    case UN_CHECK_SELECTED:
      return state.set('selected', [])

    case CREATE_PROPERTY_SUCCESS:
      return state.set('address', '')
    default:
      return state
  }
}

export default propertiesPageReducer
