import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { COLORS } from '../../constants'

export const TableWrapper = styled.div`
  display: flex;
  flex-direction: column;
  border-radius: 2px;
  flex-grow: 1;
  box-shadow: 2px 2px 9px 0 rgba(95, 95, 95, 0.209239);
`
export const TableHeader = styled.header`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 20px;
  width: 100%;
  position: relative;
  background-color: ${COLORS.WHITE};
  @media (max-width: 767px) {
    padding: 15px;
  }
`
export const TableContent = styled.div`
  display: flex;
  flex-direction: column;
  background-color: ${COLORS.WHITE};
  @media (max-width: 767px) {
    box-shadow: 2px 2px 9px 0 rgba(95, 95, 95, 0.209239);
  }
`

export const TableHeaderLight = styled(TableHeader)`
  background-color: ${COLORS.LIGHT};

  @media (max-width: 767px) {
    & > :last-child {
      display: none;
    }
    & > :nth-child(3) {
      justify-content: flex-end;
      padding-right: 0;
    }
  }
`

export const HeaderSorting = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  @media (max-width: 991px) {
    display: none;
  }
`
export const TableRow = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 20px;
  width: 100%;
  background-color: ${COLORS.WHITE};
  border: 1px solid ${COLORS.WHITE};
  border-bottom: 1px solid ${COLORS.LIGHT};
  flex-wrap: nowrap;
  &:hover {
    border: 1px solid ${COLORS.PRIMARY};
  }
  & > :last-child {
    justify-content: flex-end;
  }
  @media (max-width: 767px) {
    padding: 15px;
    & > :nth-child(3) {
      justify-content: flex-end;
      padding-right: 0;
    }
  }
`
export const TableHeaderLabel = styled.h3`
  font-size: 16px;
  line-height: 18px;
  color: ${COLORS.TITLE};
  padding: 0 20px;
  margin: 0;
  display: flex;
  width: 100%;
  flex-grow: ${props => props.grow || 1};
`

export const SmallTableHeaderLabel = styled.h3`
  font-size: 16px;
  line-height: 18px;
  color: ${COLORS.TITLE};
  padding: 0 5px;
  max-width: 35px;
  min-width: 35px;
  text-align: left;
  margin: 0;
  display: flex;
  flex-grow: ${props => props.grow || 1};
`
export const TableRowLabel = styled.h3`
  font-size: 14px;
  line-height: 17px;
  color: ${COLORS.TEXT};
  padding: 0 20px;
  margin: 0;
  width: 100%;
  display: flex;
  flex-grow: ${props => props.grow || 1};
`

export const SmallTableRowLabel = styled(TableRowLabel)`
  max-width: 30px;
  min-width: 30px;
  padding: 0 5px;
  width: initial;
  @media (max-width: 767px) {
    color: ${COLORS.TEXT};
  }
`

export const LoadingRow = styled(TableRow)`
  & > :first-child {
    margin-left: 30px;
  }
  @media (max-width: 767px) {
    & > :first-child {
      margin-left: 0px;
    }
  }
`

export const LoadingBlock = styled(TableRowLabel)`
  background: linear-gradient(90deg, #e0e0e0 0%, #f3f4f5 100%);
  height: ${props => props.height || 'initial'};
  margin-left: 30px;
  @media (max-width: 767px) {
    margin-left: 0;
  }
`

export const SmallLoadingBlock = styled(SmallTableRowLabel)`
  background: linear-gradient(90deg, #e0e0e0 0%, #f3f4f5 100%);
  height: ${props => props.height || 'initial'};
  margin-left: 30px;
  @media (max-width: 767px) {
    margin-left: 40px;
  }
`

export const TabsWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
`
export const TabsLabel = styled.h3`
  font-size: 14px;
  line-height: 17px;
  color: ${COLORS.TITLE};
  padding: 0 20px;
  margin: 0;
  @media (max-width: 767px) {
    display: none;
  }
`
export const MobileTabsLabel = styled(TabsLabel)`
  @media (max-width: 767px) {
    display: inline-flex;
  }
`
export const TabView = styled.button`
  border-radius: 2px;
  display: flex;
  margin: 0 5px;
  color: ${COLORS.TEXT};
  padding: 8px 13px;
  background-color: ${COLORS.WHITE};
  font-size: 12px;
  border: 1px solid ${COLORS.TEXT};
  cursor: pointer;
  &:hover {
    border-color: ${COLORS.PRIMARY};
  }
  &.active {
    background-color: ${COLORS.PRIMARY};
    color: ${COLORS.WHITE};
    border-color: ${COLORS.PRIMARY};
  }
  @media (max-width: 767px) {
    border: none;
    &.active {
      color: ${COLORS.PRIMARY};
      background-color: ${COLORS.WHITE};
      border-radius: 0;
      border-bottom: 3px solid ${COLORS.PRIMARY};
      margin: 0;
    }
  }
`

export const LoadMoreButton = styled.button`
  font-weight: 600;
  color: ${COLORS.WHITE};
  background-color: ${COLORS.PRIMARY};
  font-size: 18px;
  line-height: 21px;
  border-radius: 2px;
  padding: 10px 40px;
  margin: 40px auto 20px;
  @media (max-width: 767px) {
    padding: 8px 24px;
    font-size: 16px;
    line-height: 18px;
    margin: 17px auto 17px;
  }
`

export const Row = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  justify-content: space-between;
`

export const AddressLink = styled(Link)`
  color: ${COLORS.TEXT};
  &:hover {
    text-decoration: none;
  }
`

export const Button = styled.button`
  border-radius: 2px;
  border: 1px solid ${COLORS.TEXT};
  color: ${COLORS.TEXT};
  font-size: 12px;
  align-items: center;
  text-align: center;
  padding: 8px 14px;
  cursor: pointer;
  display: inline-flex;
`

export const ButtonLink = styled(Link)`
  border: 1px solid ${COLORS.PRIMARY};
  padding: 7px 10px;
  color: ${COLORS.PRIMARY};
  border-radius: 2px;
  font-size: 14px;
  display: flex;
  align-items: center;

  &:hover {
    background-color: ${COLORS.BUTTON_BACKGROUND};
    color: ${COLORS.WHITE};
    text-decoration: none;
  }
`
export const PropertyLinksWrapper = styled(TableRowLabel)`
  padding: 0;
  @media (max-width: 767px) {
    display: none;
  }
`
export const PrimaryButton = styled(Button)`
  border: 1px solid ${COLORS.PRIMARY};
  background-color: ${COLORS.PRIMARY};
  color: ${COLORS.WHITE};
`

export const AddPropertyButton = styled(PrimaryButton)`
  font-weight: 600;
  font-size: 16px;
  line-height: 21px;
  padding: 10px 20px;
  @media (max-width: 767px) {
    font-size: 14px;
    line-height: 17px;
  }
`

export const Icon = styled.img`
  width: 14px;
  height: 14px;
  min-width: 14px;
  margin: 0 4px;
`
export const CreatePropertyButton = styled(AddPropertyButton)`
  padding: 10px 55px;
  margin: 0 auto;
  display: inline-flex;
`
export const SelectControlWrapper = styled.div`
  display: flex;
  align-items: center;
`
export const FilterButton = styled(PrimaryButton)`
  display: none;
  @media (max-width: 991px) {
    display: inline-flex;
  }
`

export const FilterModal = styled.div`
  position: absolute;
  right: 15px;
  top: 15px;
  border: 1px solid ${COLORS.BACKGROUND};
  width: 100%;
  min-height: 395px;
  background-color: ${COLORS.WHITE};
  z-index: 1;
  @media (max-width: 767px) {
    right: 0;
    top: 0;
    left: 0;

    max-width: initial;
  }
`

export const FilterModalHeader = styled.header`
  display: flex;
  align-items: center;
  justify-content: space-between;
  background-color: ${COLORS.PRIMARY};
`

export const FilterModalTitle = styled.h2`
  margin: 0;
  font-size: 14px;
  line-height: 17px;
  color: ${COLORS.WHITE};
  font-weight: 600;
`

export const FilterContent = styled.div`
  padding: 13px 20px;
`

export const Modal = styled.div`
  display: flex;
  padding: 25px;
  flex-direction: column;
  background-color: ${COLORS.WHITE};
  border-radius: 2px;
  width: 100%;
  max-width: 570px;
  align-items: flex-start;
  z-index: 2;
  position: relative;
`

export const FormHeader = styled.header`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-bottom: 35px;
`

export const IconDiv = styled.div`
  img {
    width: 45px;
    height: 45px;
  }
`
export const LabelDiv = styled.div`
  margin-left: 20px;
  font-size: 18px;
  font-weight: 600;
  color: ${COLORS.TITLE};
  @media (max-width: 767px) {
    font-size: 16px;
  }
`

export const ModalLabel = styled.h3`
  margin: 0;
  color: ${COLORS.TITLE};
  font-weight: 600;
  font-size: 14px;
  line-height: 17px;
`

export const FormContent = styled.div`
  padding: 0;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  width: 100%;
`

export const ModalBackdrop = styled.div`
  position: absolute;
  left: 0;
  right: 0;
  bottom: 0;
  top: 0;
  background-color: rgba(107, 107, 107, 0.8);
`

export const ModalWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
  position: fixed;
  padding: 0 18px;
  left: 0;
  right: 0;
  bottom: 0;
  top: 0;
`

export const ModalInput = styled.input`
  border-radius: 2px;
  border: none;
  border-bottom: 2px solid ${COLORS.PRIMARY};
  background-color: ${COLORS.INPUT_BACKGROUND};
  color: ${COLORS.TITLE};
  font-size: 12px;
  width: 100%;
  min-height: 45px;
  margin: 20px 0 40px;
  padding: 15px 10px;
`

export const CloseModal = styled(FontAwesomeIcon).attrs({
  icon: ['far', 'times-circle'],
})`
  color: ${COLORS.PRIMARY};
  background: transparent;
  width: 20px;
  height: 20px;
  min-width: 20px;
  position: absolute;
  right: 10px;
  top: 10px;
  cursor: pointer;
`
