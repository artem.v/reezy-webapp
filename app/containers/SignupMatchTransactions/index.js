/**
 *
 * Sign-up: Setting up Properties and Leases
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { FormattedMessage, injectIntl, intlShape } from 'react-intl'
import { createStructuredSelector } from 'reselect'
import { compose } from 'redux'
import TransactionsList from '../TransactionsList'

import {
  makeSelectSignupPropertyForm,
  makeSelectSignupStep,
  makeSelectSignupSteps,
  makeSelectProperty,
} from '../../containers/SignupPage/selectors'

import {
  matchingTransactionsSubmit,
  setStep,
} from '../../containers/SignupPage/actions'

import SignupErrorContainer from '../SignupErrorContainer'

import messages from './messages'
import signupMatchTransactionsIcon from '../../images/signup-match-transactions-active.svg'

import {
  FormFooter,
  SubmitButton,
  FormContent,
  FormHeader,
  LabelDiv,
  IconDiv,
} from '../SignupPage/styled'

export class SignupMatchTransactions extends React.Component {
  onSubmit = () => {
    this.props.matchingTransactionsSubmit()
  }

  backTask = () => {
    if (this.props.step - 1 >= 0) {
      this.props.setStep(this.props.step - 1)
    }
  }

  render() {
    const { intl, form, property } = this.props

    return (
      <React.Fragment>
        <FormHeader>
          <IconDiv>
            <img
              src={signupMatchTransactionsIcon}
              alt={intl.formatMessage(messages.alt)}
            />
          </IconDiv>
          <LabelDiv>
            {intl.formatMessage(messages.matchingTransactions)}
          </LabelDiv>
        </FormHeader>
        <SignupErrorContainer />
        <FormContent>
          <TransactionsList propertyId={property.id} />
          <FormFooter>
            <SubmitButton
              disabled={!form.$form.valid || form.$form.pending}
              onClick={this.onSubmit}
            >
              <FormattedMessage {...messages.continue} />
            </SubmitButton>
          </FormFooter>
        </FormContent>
      </React.Fragment>
    )
  }
}

SignupMatchTransactions.propTypes = {
  form: PropTypes.object,
  intl: intlShape.isRequired,
  matchingTransactionsSubmit: PropTypes.func.isRequired,
  step: PropTypes.number,
  setStep: PropTypes.func.isRequired,
  steps: PropTypes.arrayOf(
    PropTypes.shape({
      completed: PropTypes.bool,
    })
  ),
  property: PropTypes.shape({
    id: PropTypes.number.isRequired,
  }),
}

const mapStateToProps = createStructuredSelector({
  form: makeSelectSignupPropertyForm(),
  step: makeSelectSignupStep(),
  steps: makeSelectSignupSteps(),
  property: makeSelectProperty(),
})

const mapDispatchToProps = {
  matchingTransactionsSubmit,
  setStep,
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

export default compose(
  withConnect,
  injectIntl
)(SignupMatchTransactions)
