import React from 'react'
import { Switch, Route } from 'react-router-dom'

import injectSaga from 'utils/injectSaga'
import { withRouter } from 'react-router'
import { compose } from 'redux'
import LandingPage from 'containers/LandingPage/Loadable'
import SigninPage from 'containers/SigninPage/Loadable'
import SignupPage from 'containers/SignupPage/Loadable'
import DashboardPage from 'containers/DashboardPage/Loadable'
import TransactionsPage from 'containers/TransactionsPage/Loadable'
import PropertiesPage from 'containers/PropertiesPage'
import PropertyDetailsPage from 'containers/PropertyDetailsPage'

import saga from './saga'

function App() {
  return (
    <Switch>
      <Route exact path="/" component={LandingPage} />
      <Route exact path="/dashboard" component={DashboardPage} />
      <Route exact path="/properties" component={PropertiesPage} />
      <Route exact path="/properties/:id" component={PropertyDetailsPage} />
      <Route path="/transactions/:id?" component={TransactionsPage} />
      <Route exact path="/signin" component={SigninPage} />
      <Route exact path="/signup" component={SignupPage} />
    </Switch>
  )
}

const withSaga = injectSaga({ key: 'app', saga })

export default compose(
  withRouter,
  withSaga
)(App)
