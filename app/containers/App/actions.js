export const FETCH_USER = 'app/FETCH_USER'
export const fetchUser = () => ({
  type: FETCH_USER,
})

export const SESSION_RESTORE = 'app/SESSION_RESTORE'
export const sessionRestore = user => ({
  type: SESSION_RESTORE,
  user,
})

export const SESSION_DESTROY = 'app/SESSION_DESTROY'
export const sessionDestroy = () => ({
  type: SESSION_DESTROY,
})

export const SESSION_DESTROY_SUCCESS = 'app/SESSION_DESTROY_SUCCESS'
export const sessionDestroySuccess = () => ({
  type: SESSION_DESTROY_SUCCESS,
})

export const CHECK_SESSION_FAILED = 'app/CHECK_SESSION_FAILED'
export const checkSessionFailed = () => ({
  type: CHECK_SESSION_FAILED,
})

export const USER_SIGNIN_SUCCESS = 'app/USER_SIGNIN_SUCCESS'
export const userSignInSuccess = user => ({
  type: USER_SIGNIN_SUCCESS,
  user,
})
