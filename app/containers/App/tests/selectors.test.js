import Immutable from 'seamless-immutable'

import { makeSelectLocation } from 'containers/App/selectors'

describe('makeSelectLocation', () => {
  it('should select the location', () => {
    const route = Immutable({
      location: { pathname: '/foo' },
    })
    const mockedState = Immutable({
      route,
    })

    expect(makeSelectLocation()(mockedState)).toEqual(route.location)
  })
})
