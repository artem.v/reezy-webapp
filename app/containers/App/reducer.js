/*
 *
 * App reducer
 *
 */
import Immutable from 'seamless-immutable'

import {
  SESSION_RESTORE,
  CHECK_SESSION_FAILED,
  SESSION_DESTROY_SUCCESS,
  USER_SIGNIN_SUCCESS,
} from './actions'

export const initialState = Immutable({
  user: null,
  sessionChecked: false,
  dashboard: {
    area: {},
    bar: {},
    summary: {},
    properties: [],
  },
})

function appReducer(state = initialState, action) {
  switch (action.type) {
    case USER_SIGNIN_SUCCESS:
    case SESSION_RESTORE:
      return state.set('user', action.user).set('sessionChecked', true)
    case SESSION_DESTROY_SUCCESS:
      return state.set('user', null)
    case CHECK_SESSION_FAILED:
      return state.set('sessionChecked', true)
    default:
      return state
  }
}

export default appReducer
