import { takeLatest, all, call, put } from 'redux-saga/effects'

import {
  FETCH_USER,
  sessionRestore,
  checkSessionFailed,
  SESSION_DESTROY,
  sessionDestroySuccess,
  USER_SIGNIN_SUCCESS,
} from './actions'
import api from '../../api'

export function* fetchUserSaga() {
  try {
    const res = yield call(api.checkSession)

    if (res.ok && res.data.user) {
      yield put(sessionRestore(res.data.user))
    } else {
      yield put(checkSessionFailed())
    }
  } catch (e) {
    yield put(checkSessionFailed())
  }
}

export function* destroySessionSaga() {
  try {
    window.sessionStorage.removeItem('authToken')
    api.instance.setHeader('Authorization', null)
    yield put(sessionDestroySuccess())
  } catch (e) {
    // handle error
  }
}

export function* saveAuthToken({ user }) {
  window.sessionStorage.setItem('authToken', user.token)
  api.instance.setHeader('Authorization', `Bearer ${user.token}`)
}

// Individual exports for testing
export default function* defaultSaga() {
  yield all([
    yield takeLatest(FETCH_USER, fetchUserSaga),
    yield takeLatest(SESSION_DESTROY, destroySessionSaga),
    yield takeLatest(USER_SIGNIN_SUCCESS, saveAuthToken),
  ])
}
