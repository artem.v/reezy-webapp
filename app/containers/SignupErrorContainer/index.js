import React from 'react'
import styled from 'styled-components'
import { createStructuredSelector } from 'reselect'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { makeSelectSignupError } from '../../containers/SignupPage/selectors'
import { COLORS } from '../../constants'

export const ErrorBlock = styled.div`
  display: flex;
  padding: 15px;
  border-radius: 5px;
  background-color: ${COLORS.FAILED};
  color: ${COLORS.WHITE};
  font-size: 14px;
  font-weight: 600;
  line-height: normal;
  margin-bottom: 35px;
`

export class SignupErrorContainer extends React.PureComponent {
  static propTypes = {
    error: PropTypes.string,
  }

  render() {
    if (!this.props.error) {
      return null
    }

    return <ErrorBlock>{this.props.error}</ErrorBlock>
  }
}

const mapStateToProps = createStructuredSelector({
  error: makeSelectSignupError(),
})

export default connect(
  mapStateToProps,
  null
)(SignupErrorContainer)
