import React from 'react'
import PropTypes from 'prop-types'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'

import { fetchUser } from '../App/actions'

class CheckSession extends React.Component {
  static propTypes = {
    fetchUser: PropTypes.func.isRequired,
    sessionChecked: PropTypes.bool.isRequired,
    user: PropTypes.object,
    redirectSignedIn: PropTypes.string,
    redirectNoSession: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    children: PropTypes.object,
  }

  static defaultProps = {
    redirectNoSession: true,
  }

  componentDidMount() {
    if (!this.props.sessionChecked) {
      this.props.fetchUser()
    }
  }

  render() {
    const {
      sessionChecked,
      user,
      redirectSignedIn,
      redirectNoSession,
    } = this.props

    if (!sessionChecked) {
      return null
    }

    if (user && redirectSignedIn) {
      return <Redirect to={redirectSignedIn} />
    }

    if (!user && redirectNoSession && !redirectSignedIn) {
      const redirectTo =
        redirectNoSession === true ? '/signin' : redirectNoSession
      return <Redirect to={redirectTo} />
    }

    return this.props.children
  }
}

const mapStateToProps = state => ({
  sessionChecked: state.app.sessionChecked,
  user: state.app.user,
})

const mapDispatchToProps = dispatch => ({
  fetchUser: () => dispatch(fetchUser()),
})

const ConnectedSession = connect(
  mapStateToProps,
  mapDispatchToProps
)(CheckSession)

export default (options = {}) => Component => props => (
  <ConnectedSession {...options}>
    <Component {...props} />
  </ConnectedSession>
)
