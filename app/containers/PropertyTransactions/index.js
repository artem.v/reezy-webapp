import React, { Component } from 'react'
import { compose } from 'redux'
import { createStructuredSelector } from 'reselect'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import moment from 'moment'
import { injectIntl, intlShape } from 'react-intl'
import messages from '../PropertyDetailsPage/messages'
import PriceAmount from '../../components/PriceAmount'
import {
  selectShowCategory,
  changeTransactionsStartDate,
  changeTransactionsEndDate,
} from '../PropertyDetailsPage/actions'
import {
  makeSelectSelectedCategory,
  makeSelectTransactions,
  makeSelectTransactionsPending,
  makeSelectTransactionsDateStart,
  makeSelectTransactionsDateEnd,
} from '../PropertyDetailsPage/selectors'
import { tabs } from './constants'
import {
  TableHeader,
  TabsWrapper,
  TabView,
  TableHeaderLight,
  SmallTableHeaderLabel,
  TableHeaderLabel,
  TableContent,
  EmptyRow,
  TableRow,
  SmallTableRowLabel,
  TableRowLabel,
  PriceAmountWrapper,
} from './styled'
import DateTimePicker from '../../components/TimePicker'

export class PropertyTransaction extends Component {
  static propTypes = {
    selectedCategory: PropTypes.string,
    selectShowCategory: PropTypes.func,
    intl: intlShape.isRequired,
    pending: PropTypes.bool,
    transactions: PropTypes.array,
    dateStart: PropTypes.string,
    dateEnd: PropTypes.string,
    changeTransactionsStartDate: PropTypes.func,
    changeTransactionsEndDate: PropTypes.func,
  }

  selectShowCategory = id => {
    this.props.selectShowCategory(id)
  }

  renderCategoryTab = category => (
    <TabView
      key={category.id}
      onClick={() => this.selectShowCategory(category.id)}
      className={category.id === this.props.selectedCategory ? 'active' : ''}
    >
      {this.props.intl.formatMessage(messages[category.label])}
    </TabView>
  )

  formatDate = date => moment(date).format('MMM DD, YYYY')

  renderTransaction = transaction => (
    <TableRow key={transaction.id}>
      <SmallTableRowLabel mobileOrder={2}>
        {this.formatDate(transaction.date)}
      </SmallTableRowLabel>
      <TableRowLabel mobileOrder={1} grow={2}>
        {transaction.name}
      </TableRowLabel>

      <PriceAmountWrapper mobileOrder={4}>
        <PriceAmount value={transaction.amount} />
      </PriceAmountWrapper>
    </TableRow>
  )

  changeStartDate = value => {
    this.props.changeTransactionsStartDate(value)
  }

  changeEndDate = value => {
    this.props.changeTransactionsEndDate(value)
  }

  render() {
    return (
      <React.Fragment>
        <TableHeader>
          <TabsWrapper>{tabs.map(this.renderCategoryTab)}</TabsWrapper>
          <DateTimePicker
            dateStart={this.props.dateStart}
            dateEnd={this.props.dateEnd}
            onStartDateChange={this.changeStartDate}
            onEndDateChange={this.changeEndDate}
          />
        </TableHeader>
        <TableHeaderLight>
          <SmallTableHeaderLabel>
            {this.props.intl.formatMessage(messages.date)}
          </SmallTableHeaderLabel>
          <TableHeaderLabel grow={2}>
            {this.props.intl.formatMessage(messages.description)}
          </TableHeaderLabel>
          <SmallTableHeaderLabel>
            {this.props.intl.formatMessage(messages.amount)}
          </SmallTableHeaderLabel>
        </TableHeaderLight>
        <TableContent>
          {!this.props.pending && !this.props.transactions.length ? (
            <EmptyRow>
              {this.props.intl.formatMessage(messages.noTransactions)}
            </EmptyRow>
          ) : (
            this.props.transactions.map(this.renderTransaction)
          )}
        </TableContent>
      </React.Fragment>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  selectedCategory: makeSelectSelectedCategory(),
  transactions: makeSelectTransactions(),
  pending: makeSelectTransactionsPending(),
  dateStart: makeSelectTransactionsDateStart(),
  dateEnd: makeSelectTransactionsDateEnd(),
})

const mapDispatchToProps = {
  selectShowCategory,
  changeTransactionsStartDate,
  changeTransactionsEndDate,
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

export default compose(
  withConnect,
  injectIntl
)(PropertyTransaction)
