import styled from 'styled-components'
import { COLORS } from '../../constants'

export const TableHeader = styled.header`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  padding: 20px 0;
  position: relative;
  background-color: ${COLORS.WHITE};
  @media (max-width: 767px) {
    display: none;
  }
`
export const TableContent = styled.div`
  display: flex;
  flex-direction: column;
  background-color: ${COLORS.WHITE};
  & > div {
    border-top: 1px solid ${COLORS.BACKGROUND};
    border-bottom: 1px solid ${COLORS.BACKGROUND};
  }
`

export const TableHeaderLight = styled(TableHeader)`
  background-color: ${COLORS.LIGHT};
  padding: 15px 40px;
  position: relative;
  width: calc(100% + 40px);
  left: -20px;

  @media (max-width: 991px) {
    width: calc(100% + 20px);
    left: -10px;
  }
`

export const HeaderSorting = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  @media (max-width: 991px) {
    display: none;
  }
`
export const TableRow = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 20px 40px;
  background-color: ${COLORS.WHITE};
  border: 1px solid ${COLORS.WHITE};
  border-bottom: 1px solid ${COLORS.LIGHT};
  flex-wrap: nowrap;
  position: relative;
  width: calc(100% + 40px);
  left: -20px;
  &:hover {
    border: 1px solid ${COLORS.PRIMARY};
  }
  @media (max-width: 991px) {
    width: calc(100% + 20px);
    left: -10px;
  }
  @media (max-width: 767px) {
    padding: 15px;
    flex-wrap: wrap;
    width: calc(100% + 30px);
    left: -15px;
  }
`
export const TableHeaderLabel = styled.h3`
  font-size: 16px;
  line-height: 18px;
  color: ${COLORS.TITLE};
  padding: 0 20px;
  margin: 0;
  display: flex;
  width: 100%;
  flex-grow: ${props => props.grow || 1};
`

export const SmallTableHeaderLabel = styled.h3`
  font-size: 16px;
  line-height: 18px;
  color: ${COLORS.TITLE};
  padding: 0 5px;
  max-width: 95px;
  min-width: 95px;
  text-align: left;
  margin: 0;
  display: flex;
  flex-grow: ${props => props.grow || 1};
`
export const TableRowLabel = styled.h3`
  font-size: 14px;
  line-height: 17px;
  color: ${COLORS.TEXT};
  padding: 0 20px;
  margin: 0;
  width: 100%;
  display: flex;
  flex-grow: ${props => props.grow || 1};
  @media (max-width: 767px) {
    order: ${props => props.mobileOrder || 'initial'};
    padding: 0 0 0 0;
    color: ${COLORS.TITLE};
    padding-right: 100px;
  }
`

export const SmallTableRowLabel = styled(TableRowLabel)`
  max-width: 95px;
  min-width: 95px;
  padding: 0 5px;
  width: initial;
  @media (max-width: 767px) {
    color: ${COLORS.TEXT};
    padding: 0;
  }
`

export const PriceAmountWrapper = styled(SmallTableRowLabel)`
  @media (max-width: 767px) {
    position: absolute;
    top: calc(50% - 14px);
    right: 0;
  }
`

export const LoadingRow = styled(TableRow)`
  & > :first-child {
    margin-left: 30px;
  }
  @media (max-width: 767px) {
    & > :first-child {
      margin-left: 0px;
    }
  }
`

export const TabsWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
`
export const TabsLabel = styled.h3`
  font-size: 14px;
  line-height: 17px;
  color: ${COLORS.TITLE};
  padding: 0 20px;
  margin: 0;
  @media (max-width: 767px) {
    display: none;
  }
`
export const MobileTabsLabel = styled(TabsLabel)`
  @media (max-width: 767px) {
    display: inline-flex;
  }
`
export const TabView = styled.button`
  border-radius: 2px;
  display: flex;
  margin: 0 5px;
  color: ${COLORS.TEXT};
  padding: 8px 13px;
  background-color: ${COLORS.WHITE};
  font-size: 12px;
  border: 1px solid ${COLORS.TEXT};
  cursor: pointer;
  &:hover {
    border-color: ${COLORS.PRIMARY};
  }
  &.active {
    background-color: ${COLORS.PRIMARY};
    color: ${COLORS.WHITE};
    border-color: ${COLORS.PRIMARY};
  }
  @media (max-width: 767px) {
    border: none;
    &.active {
      color: ${COLORS.PRIMARY};
      background-color: ${COLORS.WHITE};
      border-radius: 0;
      border-bottom: 3px solid ${COLORS.PRIMARY};
      margin: 0;
    }
  }
`

export const Row = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  justify-content: space-between;
`

export const EmptyRow = styled(Row)`
  width: 100%;
  justify-content: center;
  padding: 20px;
`
