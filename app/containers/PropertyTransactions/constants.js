import { CATEGORIES } from '../../constants'

export const tabs = [
  {
    id: 'mortgage',
    label: CATEGORIES[2],
  },
  {
    id: 'propertyTax',
    label: CATEGORIES[4],
  },
  {
    id: 'repairs',
    label: CATEGORIES[3],
  },
  {
    id: 'maintenance',
    label: CATEGORIES[5],
  },
]
