/*
 * AccountForm Messages
 *
 * This contains all the text for the AccountForm component.
 */

import { defineMessages } from 'react-intl'
import { createIntlMessage } from 'common'

const SIGNUP_PAGE_ID = 'SignUpPage'

export default defineMessages({
  ...createIntlMessage(SIGNUP_PAGE_ID, 'completeSignup'),
  ...createIntlMessage(SIGNUP_PAGE_ID, 'redirectToDashboard'),
  ...createIntlMessage(SIGNUP_PAGE_ID, 'goToDashboard'),
})
