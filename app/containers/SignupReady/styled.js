import styled from 'styled-components'
import { Link } from 'react-router-dom'

import { COLORS } from '../../constants'
import arrowDown from '../../images/arrowDown.png'

export const FormContainer = styled.div`
  width: 100%;
`

export const FormContent = styled.div`
  padding: 0;
  display: flex;
  flex-direction: column;
  align-items: center;
`
export const FormFooter = styled.div`
  width: 100%;
  text-align: right;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 35px;
`

export const FinishButton = styled(Link)`
  min-width: 160px;
  height: 40px;
  background-color: ${COLORS.PRIMARY};
  border-radius: 2px;
  font-weight: 600;
  font-size: 18px;
  padding: 8px 20px;
  text-align: center;
  color: ${COLORS.WHITE};
  @media (max-width: 767px) {
    width: 100%;
  }
  &:hover {
    cursor: pointer;
  }
  &:disabled {
    opacity: 0.65;
    color: black;
  }
`

export const BackArrow = styled.button`
  width: 40px;
  display: none;
  background-image: url(${arrowDown});
  height: 40px;
  background-size: 15px;
  background-position: center;
  background-repeat: no-repeat;
  border-radius: 2px;
  transform: rotate(90deg);
  border: 1px solid ${COLORS.PRIMARY};
  margin: 0 10px 0 0;
  cursor: pointer;
  @media (max-width: 767px) {
    display: inline-flex;
  }
  &:disabled {
    display: none;
  }
`

export const Done = styled.img`
  width: 73px;
  height: 73px;
  margin-bottom: 23px;
`
export const Label = styled.h3`
  color: ${COLORS.TITLE};
  font-size: 18px;
  line-height: 27px;
  text-align: center;
`
