/**
 *
 * AccountForm
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { injectIntl, intlShape } from 'react-intl'
import { createStructuredSelector } from 'reselect'
import { compose } from 'redux'
import { withRouter } from 'react-router'

import { makeSelectSignupStep } from '../../containers/SignupPage/selectors'
import { setStep } from '../../containers/SignupPage/actions'
import messages from './messages'

import DoneIcon from '../../images/done.svg'

import {
  FormContainer,
  FormContent,
  FormFooter,
  FinishButton,
  BackArrow,
  Label,
  Done,
} from './styled'

export class FinishedSignupForm extends React.Component {
  componentDidMount = () => {
    this.timer = setTimeout(() => this.props.history.push('/dashboard'), 5000)
  }

  backTask = () => {
    clearTimeout(this.timer)
    if (this.props.step - 1 >= 0) {
      this.props.setStep(2)
    }
  }

  render() {
    const { intl } = this.props
    return (
      <FormContainer>
        <FormContent>
          <Done src={DoneIcon} />
          <Label>{intl.formatMessage(messages.completeSignup)}</Label>
          <Label>{intl.formatMessage(messages.redirectToDashboard)}</Label>

          <FormFooter>
            <BackArrow disabled={!this.props.step} onClick={this.backTask} />
            <FinishButton to="/dashboard">
              {intl.formatMessage(messages.goToDashboard)}
            </FinishButton>
          </FormFooter>
        </FormContent>
      </FormContainer>
    )
  }
}

FinishedSignupForm.propTypes = {
  intl: intlShape.isRequired,
  step: PropTypes.number,
  setStep: PropTypes.func.isRequired,
  history: PropTypes.object,
}

const mapStateToProps = createStructuredSelector({
  step: makeSelectSignupStep(),
})

const mapDispatchToProps = {
  setStep,
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

export default compose(
  withConnect,
  withRouter,
  injectIntl
)(FinishedSignupForm)
