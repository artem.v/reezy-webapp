/*
 * SignupPage Messages
 *
 * This contains all the text for the SignupPage component.
 */

import { defineMessages } from 'react-intl'
import { createIntlMessage } from 'common'

const SIGNIN_PAGE_ID = 'SignInPage'
const SIGNUP_PAGE_ID = 'SignUpPage'

export default defineMessages({
  ...createIntlMessage(SIGNIN_PAGE_ID, 'emailPlaceholder'),
  ...createIntlMessage(SIGNIN_PAGE_ID, 'passwordPlaceholder'),
  ...createIntlMessage(SIGNIN_PAGE_ID, 'fullName'),
  ...createIntlMessage(SIGNIN_PAGE_ID, 'reezy'),
  ...createIntlMessage(SIGNUP_PAGE_ID, 'creatingAccount'),
  ...createIntlMessage(SIGNUP_PAGE_ID, 'connectingBank'),
  ...createIntlMessage(SIGNUP_PAGE_ID, 'settingProperty'),
  ...createIntlMessage(SIGNUP_PAGE_ID, 'matchingTransactions'),
  ...createIntlMessage(SIGNUP_PAGE_ID, 'readyUse'),
  ...createIntlMessage(SIGNUP_PAGE_ID, 'alt'),
  ...createIntlMessage(SIGNUP_PAGE_ID, 'openPlaidLink'),
})
