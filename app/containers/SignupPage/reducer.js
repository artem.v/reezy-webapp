/*
 *
 * SignupPage reducer
 *
 */

import Immutable from 'seamless-immutable'
import {
  SET_STEP,
  SET_PROPERTY,
  SEND_BANK_ACCOUNT_PUBLIC_TOKEN_FAILED,
  COMPLETE_TASK,
  SET_SERVER_ERROR,
  LOAD_SIGNUP_STATE_COMPLETE,
} from './actions'
import { SESSION_DESTROY_SUCCESS } from '../App/actions'

export const initialState = Immutable({
  sessionChecked: false,
  step: 0,
  steps: [
    { completed: false },
    { completed: false },
    { completed: false },
    { completed: false },
    { completed: false },
  ],
  error: '',
  property: null,
})

function signupPageReducer(state = initialState, action) {
  switch (action.type) {
    case SET_STEP:
      return state.set('step', action.step)
    case SET_PROPERTY:
      return state.set('property', action.property)
    case SEND_BANK_ACCOUNT_PUBLIC_TOKEN_FAILED:
      return state.set('error', action.message)
    case COMPLETE_TASK:
      return state.setIn(['steps', action.step], { completed: true })
    case SET_SERVER_ERROR:
      return state.set('error', action.error)
    case LOAD_SIGNUP_STATE_COMPLETE:
      return state.set('sessionChecked', true)
    case SESSION_DESTROY_SUCCESS:
      return state
    default:
      return state
  }
}

export default signupPageReducer
