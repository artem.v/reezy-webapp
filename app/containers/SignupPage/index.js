/**
 *
 * SignupPage
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { compose } from 'redux'

import injectSaga from 'utils/injectSaga'
import injectReducer from 'utils/injectReducer'
import {
  makeSelectSignupStep,
  makeSelectSignupSteps,
  makeSelectSessionChecked,
} from './selectors'
import reducer from './reducer'
import saga from './saga'
import { setStep, loadSignupState } from './actions'
import Header from '../Header'

import SignupNavigation from '../../components/SignupNavigation'
import SignupNavigationMobile from '../../components/SignupNavigation/mobile'
import SignupCreateAccount from '../SignupCreateAccount'
import SignupConnectBank from '../SignupConnectBank'
import SignupSetupProperty from '../SignupSetupProperty'
import SignupMatchTransactions from '../SignupMatchTransactions'
import SignupReady from '../SignupReady'

import {
  Wrapper,
  StepBarContainer,
  Content,
  FormContainer,
  MobileStepBarContainer,
} from './styled'

class SignupPage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isMobile: this.isMobileDevice(),
    }
    this.stepbar = React.createRef()
    this.handleStep = this.handleStep.bind(this)
  }

  componentDidMount() {
    this.props.loadSignupState()
  }

  handleStep = step => {
    if (step < this.props.step || this.props.steps[step - 1].completed) {
      this.props.setStep(step)
    }
  }

  isMobileDevice = () =>
    navigator.userAgent.indexOf('IEMobile') !== -1 || window.innerWidth <= 1200

  renderCurrentStep = () => {
    switch (this.props.step) {
      case 0:
        return <SignupCreateAccount />
      case 1:
        return <SignupConnectBank />
      case 2:
        return <SignupSetupProperty />
      case 3:
        return <SignupMatchTransactions />
      case 4:
        return <SignupReady />
      default:
        return <SignupCreateAccount />
    }
  }

  render() {
    const { step, sessionChecked } = this.props

    if (!sessionChecked) {
      return (
        <Wrapper>
          <Header />
        </Wrapper>
      )
    }

    return (
      <Wrapper>
        <Header hideMainMenu />
        {!this.state.isMobile && (
          <StepBarContainer ref={this.stepbar}>
            <SignupNavigation step={step} chooseStep={this.handleStep} />
          </StepBarContainer>
        )}
        <Content>
          <FormContainer fullWidth={step === 3}>
            {this.state.isMobile && (
              <MobileStepBarContainer>
                <SignupNavigationMobile
                  step={step}
                  chooseStep={this.handleStep}
                />
              </MobileStepBarContainer>
            )}

            {this.renderCurrentStep()}
          </FormContainer>
        </Content>
      </Wrapper>
    )
  }
}

SignupPage.propTypes = {
  step: PropTypes.number,
  setStep: PropTypes.func,
  steps: PropTypes.arrayOf(
    PropTypes.shape({
      completed: PropTypes.bool,
    })
  ),
  sessionChecked: PropTypes.bool.isRequired,
  loadSignupState: PropTypes.func.isRequired,
}

const mapStateToProps = createStructuredSelector({
  step: makeSelectSignupStep(),
  steps: makeSelectSignupSteps(),
  sessionChecked: makeSelectSessionChecked(),
})

const mapDispatchToProps = {
  setStep,
  loadSignupState,
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

const withReducer = injectReducer({ key: 'signuppage', reducer })
const withSaga = injectSaga({ key: 'signuppage', saga })

export default compose(
  withReducer,
  withSaga,
  withConnect
)(SignupPage)
