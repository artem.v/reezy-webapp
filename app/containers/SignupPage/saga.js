import { takeLatest, all, call, put, select } from 'redux-saga/effects'
import { actions as formActions } from 'react-redux-form'
import moment from 'moment'
import get from 'lodash/get'
import { ERRORS, BILLING_CYCLES } from '../../constants'

import {
  SEND_BANK_ACCOUNT_PUBLIC_TOKEN,
  CREATING_ACCOUNT_SUBMIT,
  setStep,
  sendBankAccountPublicTokenFailed,
  CREATING_PROPERTY_SUBMIT,
  completeTask,
  LOAD_SIGNUP_STATE,
  loadSignupStateComplete,
  MATCHING_TRANSACTIONS_SUBMIT,
  setProperty,
} from './actions'
import { sessionRestore, userSignInSuccess } from '../App/actions'
import api from '../../api'

export function* creatingAccountSubmit() {
  const FORM_NAME = 'forms.signupCreateAccount'
  const { name, email, password } = yield select(
    state => state.forms.signupCreateAccount
  )

  yield put(formActions.setPending(FORM_NAME, true))
  yield put(completeTask(0))

  const [firstName, lastName] = name.split(' ')

  try {
    const res = yield call(api.createAccount, {
      firstName,
      lastName,
      email,
      password,
    })

    yield put(formActions.setPending(FORM_NAME, false))

    if (!res.ok) {
      if (res.status === 409) {
        yield put(formActions.setErrors(FORM_NAME, ERRORS.USER_EXISTS))
      } else {
        yield put(formActions.setErrors(FORM_NAME, res.problem))
      }
    } else {
      const signInResult = yield call(api.signIn, {
        username: email,
        password,
      })

      if (!signInResult.ok) {
        yield put(formActions.setErrors(FORM_NAME, res.problem))
      } else {
        yield put(userSignInSuccess(signInResult.data.user))
        yield put(completeTask(0))
        yield put(setStep(1))
      }
    }
  } catch (e) {
    yield put(formActions.setErrors(FORM_NAME, e.message))
    yield put(formActions.setPending(FORM_NAME, false))
  }
}

export function* sendBankAccountPublicToken(action) {
  try {
    const { publicToken } = action
    const response = yield call(api.authBankAccount, { publicToken })

    if (response.ok) {
      yield put(setStep(2))
    } else {
      yield put(sendBankAccountPublicTokenFailed(response.problem))
    }
  } catch (error) {
    yield put(sendBankAccountPublicTokenFailed('Unexpected error'))
  }
}

export function* creatingPropertySubmit() {
  const FORM_NAME = 'forms.signupProperty'

  const {
    address,
    rentAmount,
    billingCycle,
    rentStartDate,
    tenantName,
  } = yield select(state => state.forms.signupProperty)

  yield put(formActions.setPending(FORM_NAME, true))

  try {
    const res = yield call(api.createProperty, { address })

    if (!res.ok) {
      yield put(formActions.setErrors(FORM_NAME, res.problem))
    } else {
      const { id: propertyId } = res.data.response

      yield put(setProperty(res.data.response))

      const endDate = moment(rentStartDate)
        .add(3, 'years')
        .format('YYYY-MM-DD')
      const billingCycleType = Object.keys(BILLING_CYCLES)[
        parseInt(billingCycle, 10) - 1
      ]

      const res2 = yield call(api.createLeases, {
        propertyId,
        tenantName,
        startDate: rentStartDate,
        endDate,
        rentAmount: parseInt(rentAmount, 10),
        billingCycle: billingCycleType,
      })

      if (!res2.ok) {
        yield put(formActions.setErrors(FORM_NAME, res.problem))
      } else {
        yield put(completeTask(2))
        yield put(setStep(3))
      }
    }

    yield put(formActions.setPending(FORM_NAME, false))
  } catch (e) {
    yield put(formActions.setErrors(FORM_NAME, e.message))
    yield put(formActions.setPending(FORM_NAME, false))
  }
}

export function* matchingTransactionsSubmit() {
  yield put(completeTask(3))
  yield put(setStep(4))
}

export function* loadSignupState() {
  const resSession = yield call(api.checkSession)

  // If the user has a session then move to the next step
  if (resSession.ok) {
    yield put(completeTask(0))
    yield put(setStep(1))
    yield put(sessionRestore(resSession.data.user))

    const resBankAccounts = yield call(api.getBankAccounts)

    // If the user has bank accounts then move to the next step
    if (
      resBankAccounts.ok &&
      get(resBankAccounts, 'data.bankAccounts', []).length
    ) {
      yield put(completeTask(1))
      yield put(setStep(2))

      const resProperties = yield call(api.getPropertiesList)

      if (resProperties.ok && resProperties.data.response.length > 0) {
        yield put(setProperty(resProperties.data.response[0]))
        yield put(completeTask(2))
        yield put(setStep(3))
      }
    }
  }

  yield put(loadSignupStateComplete())
}

export default function* defaultSaga() {
  yield all([
    yield takeLatest(
      SEND_BANK_ACCOUNT_PUBLIC_TOKEN,
      sendBankAccountPublicToken
    ),
    yield takeLatest(CREATING_ACCOUNT_SUBMIT, creatingAccountSubmit),
    yield takeLatest(CREATING_PROPERTY_SUBMIT, creatingPropertySubmit),
    yield takeLatest(MATCHING_TRANSACTIONS_SUBMIT, matchingTransactionsSubmit),
    yield takeLatest(LOAD_SIGNUP_STATE, loadSignupState),
  ])
}
