import { createSelector } from 'reselect'
import { initialState } from './reducer'

/**
 * Direct selector to the signupPage state domain
 */

const selectSignupPage = state => state.signuppage || initialState
const selectFormsDomain = state => state.forms

/**
 * Other specific selectors
 */

const makeSelectSignupCreateAccountForm = () =>
  createSelector(selectFormsDomain, forms => forms.forms.signupCreateAccount)

const makeSelectSignupPropertyForm = () =>
  createSelector(selectFormsDomain, forms => forms.forms.signupProperty)

/**
 * Default selector used by SignupPage
 */

const makeSelectSignupStep = () =>
  createSelector(selectSignupPage, signuppageState => signuppageState.step)
const makeSelectSignupSteps = () =>
  createSelector(selectSignupPage, signuppageState => signuppageState.steps)
const makeSelectSignupError = () =>
  createSelector(selectSignupPage, signuppageState => signuppageState.error)
const makeSelectSessionChecked = () =>
  createSelector(selectSignupPage, state => state.sessionChecked)
const makeSelectProperty = () =>
  createSelector(selectSignupPage, state => state.property)

export {
  selectSignupPage,
  makeSelectSignupStep,
  makeSelectSignupCreateAccountForm,
  makeSelectSignupPropertyForm,
  makeSelectSignupSteps,
  makeSelectSignupError,
  makeSelectSessionChecked,
  makeSelectProperty,
}
