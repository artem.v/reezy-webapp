/*
 *
 * SignupPage actions
 *
 */

export const LOAD_SIGNUP_STATE = 'app/SignupPage/LOAD_SIGNUP_STATE'
export const loadSignupState = () => ({
  type: LOAD_SIGNUP_STATE,
})

export const LOAD_SIGNUP_STATE_COMPLETE =
  'app/SignupPage/LOAD_SIGNUP_STATE_COMPLETE'
export const loadSignupStateComplete = () => ({
  type: LOAD_SIGNUP_STATE_COMPLETE,
})

export const SET_STEP = 'app/SignupPage/SET_STEP'
export const setStep = step => ({
  type: SET_STEP,
  step,
})

export const CREATING_ACCOUNT_SUBMIT = 'app/SignupPage/CREATING_ACCOUNT_SUBMIT'
export const createAccountSubmit = () => ({
  type: CREATING_ACCOUNT_SUBMIT,
})

export const CREATING_PROPERTY_SUBMIT =
  'app/SignupPage/CREATING_PROPERTY_SUBMIT'
export const createPropertySubmit = () => ({
  type: CREATING_PROPERTY_SUBMIT,
})

export const SET_PROPERTY = 'app/SignupPage/SET_PROPERTY'
export const setProperty = property => ({
  type: SET_PROPERTY,
  property,
})

export const SEND_BANK_ACCOUNT_PUBLIC_TOKEN =
  'app/SignupPage/SEND_BANK_ACCOUNT_PUBLIC_TOKEN'
export const sendBankAccountPublicToken = publicToken => ({
  type: SEND_BANK_ACCOUNT_PUBLIC_TOKEN,
  publicToken,
})

export const SEND_BANK_ACCOUNT_PUBLIC_TOKEN_FAILED =
  'app/SignupPage/SEND_BANK_ACCOUNT_PUBLIC_TOKEN_FAILED'
export const sendBankAccountPublicTokenFailed = message => ({
  type: SEND_BANK_ACCOUNT_PUBLIC_TOKEN_FAILED,
  message,
})

export const MATCHING_TRANSACTIONS_SUBMIT =
  'app/SignupPage/MATCHING_TRANSACTIONS_SUBMIT'
export const matchingTransactionsSubmit = () => ({
  type: MATCHING_TRANSACTIONS_SUBMIT,
})

export const COMPLETE_TASK = 'app/SignupPage/COMPLETE_TASK'
export const completeTask = step => ({
  type: COMPLETE_TASK,
  step,
})

export const SET_SERVER_ERROR = 'app/SignupPage/SET_SERVER_ERROR'
export const setServerError = error => ({
  type: SET_SERVER_ERROR,
  error,
})
