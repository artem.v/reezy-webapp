import styled from 'styled-components'
import { COLORS, SIZES } from '../../constants'

import signupBackground from '../../images/signup-bg.png'
import arrowDown from '../../images/arrowDown.png'

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  min-height: 100vh;
  background-color: ${COLORS.BACKGROUND};
  font-family: PT Sans, sans-serif;
`
export const TempHeader = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  background-color: ${COLORS.WHITE};
  height: 80px;
  @media only screen and (max-width: 768px) {
    height: 60px;
  }
`
export const StepBarContainer = styled.div`
  width: 100%;
  overflow-x: overlay;
  text-align: center;
  @media only screen and (max-width: 767px) {
    display: none;
  }
`
export const Content = styled.div`
  width: 100%;
  height: calc(100vh - 210px);
  display: flex;
  justify-content: center;
  align-items: baseline;
  margin-top: 30px;
  background-image: url(${signupBackground});
  background-size: cover;

  @media only screen and (max-width: 768px) {
    height: calc(100vh - 80px);
  }
`
export const FormContainer = styled.div`
  width: ${props => (props.fullWidth ? '80%' : '540px')};
  padding: 27px 20px;
  background: ${COLORS.WHITE};
  box-shadow: 2px 2px 7px rgba(8, 39, 64, 0.1);
  border-radius: 2px;
  @media (max-width: 767px) {
    padding: 20px;
  }
  @media only screen and (max-width: 600px) {
    width: 100%;
    margin-left: 17px;
    margin-right: 17px;
  }
`
export const MobileStepBarContainer = styled.div`
  width: 100%;
  text-align: center;
  margin-bottom: 35px;
  @media only screen and (min-width: 1200px) {
    display: none;
  }
`

export const PlaidLinkWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 100px;
`

// export const FormContainer = styled.div`
//   width: 100%;
// `

export const FormHeader = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-bottom: 35px;
`
export const FormContent = styled.div`
  padding: 0;
  display: flex;
  flex-direction: column;
  align-items: center;
`
export const FormFooter = styled.div`
  width: 100%;
  text-align: right;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  margin-top: 20px;
`
export const IconDiv = styled.div`
  img {
    width: 45px;
    height: 45px;
  }
`
export const LabelDiv = styled.div`
  margin-left: 20px;
  font-size: 18px;
  font-weight: 600;
  color: ${COLORS.TITLE};
  @media (max-width: 767px) {
    font-size: 16px;
  }
`
export const SubmitButton = styled.button`
  width: 160px;
  height: 40px;
  background-color: ${COLORS.PRIMARY};
  border-radius: 2px;
  font-weight: 600;
  font-size: ${SIZES.H2};
  text-align: center;
  color: ${COLORS.WHITE};
  @media (max-width: 767px) {
    width: 100%;
  }
  &:hover {
    cursor: pointer;
  }
  &:disabled {
    background-color: ${COLORS.TEXT};
  }
`

export const BackArrow = styled.button`
  width: 40px;
  display: none;
  background-image: url(${arrowDown});
  height: 40px;
  background-size: 15px;
  background-position: center;
  background-repeat: no-repeat;
  border-radius: 2px;
  transform: rotate(90deg);
  border: 1px solid ${COLORS.PRIMARY};
  margin: 0 10px 0 0;
  cursor: pointer;
  @media (max-width: 767px) {
    display: inline-flex;
  }
  &:disabled {
    display: none;
  }
`
