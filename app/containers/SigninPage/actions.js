/*
 *
 * SignInPage actions
 *
 */

export const USER_SIGNIN_SUBMIT = 'app/SignInPage/USER_SIGNIN_SUBMIT'
export const userSignInSubmit = () => ({
  type: USER_SIGNIN_SUBMIT,
})
