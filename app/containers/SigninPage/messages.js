/*
 * SignInPage Messages
 *
 * This contains all the text for the SignInPage component.
 */

import { defineMessages } from 'react-intl'
import { createIntlMessage } from 'common'

const PAGE_ID = 'SignInPage'

export default defineMessages({
  ...createIntlMessage(PAGE_ID, 'reezy'),
  ...createIntlMessage(PAGE_ID, 'description'),
  ...createIntlMessage(PAGE_ID, 'dontHaveAccount'),
  ...createIntlMessage(PAGE_ID, 'signUpLink'),
  ...createIntlMessage(PAGE_ID, 'header'),
  ...createIntlMessage(PAGE_ID, 'submitButton'),
  ...createIntlMessage(PAGE_ID, 'buttonLoading'),
  ...createIntlMessage(PAGE_ID, 'forgotPasswordLink'),
  ...createIntlMessage(PAGE_ID, 'privacyLink'),
  ...createIntlMessage(PAGE_ID, 'legalLink'),
  ...createIntlMessage(PAGE_ID, 'contactLink'),
  ...createIntlMessage(PAGE_ID, 'emailPlaceholder'),
  ...createIntlMessage(PAGE_ID, 'passwordPlaceholder'),
})
