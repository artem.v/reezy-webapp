/*
 *
 * SignInPage reducer
 *
 */

import Immutable from 'seamless-immutable'

export const initialState = Immutable({})

function signInPageReducer(state = initialState, action) {
  switch (action.type) {
    default:
      return state
  }
}

export default signInPageReducer
