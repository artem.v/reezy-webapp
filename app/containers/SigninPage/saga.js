import { takeLatest, all, call, put, select } from 'redux-saga/effects'
import { actions as formActions } from 'react-redux-form'
import { push } from 'react-router-redux'

import { USER_SIGNIN_SUBMIT } from './actions'
import { userSignInSuccess } from '../App/actions'
import { ERRORS } from '../../constants'
import api from '../../api'

const FORM_NAME = 'forms.signin'

export function* submitForm() {
  const { username, password } = yield select(state => state.forms.signin)

  yield put(formActions.setPending(FORM_NAME, true))

  try {
    const res = yield call(api.signIn, { username, password })

    yield put(formActions.setPending(FORM_NAME, false))

    if (!res.ok) {
      if (res.status === 401) {
        yield put(
          formActions.setErrors(FORM_NAME, ERRORS.INCORRECT_CREDENTIALS)
        )
      } else {
        yield put(formActions.setErrors(FORM_NAME, res.problem))
      }
    } else {
      yield put(userSignInSuccess(res.data.user))
      yield put(push('/dashboard'))
    }
  } catch (e) {
    yield put(formActions.setErrors(FORM_NAME, e.message))
    yield put(formActions.setPending(FORM_NAME, false))
  }
}

// Individual exports for testing
export default function* defaultSaga() {
  yield all([yield takeLatest(USER_SIGNIN_SUBMIT, submitForm)])
}
