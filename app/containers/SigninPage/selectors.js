import { createSelector } from 'reselect'
import { initialState } from './reducer'

/**
 * Direct selector to the signInPage state domain
 */

const selectSignInPageDomain = state => state.signInPage || initialState
const selectFormsDomain = state => state.forms

/**
 * Other specific selectors
 */
export const signInFormSelector = createSelector(
  selectFormsDomain,
  forms => forms.forms.signin
)

/**
 * Default selector used by SignInPage
 */

const makeSelectSignInPage = () =>
  createSelector(selectSignInPageDomain, substate => substate)

export default makeSelectSignInPage
