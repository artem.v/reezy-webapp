import Immutable from 'seamless-immutable'
import signInPageReducer from '../reducer'

describe('signInPageReducer', () => {
  it('returns the initial state', () => {
    expect(signInPageReducer(undefined, {})).toEqual(Immutable({}))
  })
})
