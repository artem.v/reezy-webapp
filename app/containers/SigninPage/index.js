/**
 *
 * SigninPage
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { FormattedMessage, injectIntl, intlShape } from 'react-intl'
import { compose } from 'redux'
import { Container, Row, Col } from 'reactstrap'
import { Link } from 'react-router-dom'

import injectSaga from 'utils/injectSaga'
import injectReducer from 'utils/injectReducer'
import reducer from './reducer'
import { userSignInSubmit } from './actions'
import saga from './saga'
import messages from './messages'

import { SubmitButton } from '../../components/styled'
import {
  DescriptionBlock,
  DescHeader,
  Footer,
  FooterLinks,
  MainBlock,
  SignUpWrapper,
  FormHeader,
  FormRow,
  ActionButton,
  ForgotPasswordLink,
} from './styled'

import DynamicForm from '../../components/Forms/DynamicForm'
import { FORM_INPUT_TYPES } from '../../components/Forms/constants'

import logoWhite from '../../images/logo_white.png'

const CURRENT_YEAR = new Date().getFullYear()

function SigninPage({ form, intl, onClickSubmit }) {
  return (
    <Container fluid>
      <Row>
        <DescriptionBlock xs="12" md="5">
          <Link to="/">
            <img src={logoWhite} height="60" alt="Reezy" />
          </Link>
          <DescHeader className="d-none d-md-flex">
            <FormattedMessage {...messages.description} />
          </DescHeader>
          <Footer className="d-none d-md-flex">
            &copy; {CURRENT_YEAR} {intl.formatMessage(messages.reezy)}
            <FooterLinks>
              <Link to="/privacy">
                {intl.formatMessage(messages.privacyLink)}
              </Link>
              <Link to="/legal">{intl.formatMessage(messages.legalLink)}</Link>
              <Link to="/contact">
                {intl.formatMessage(messages.contactLink)}
              </Link>
            </FooterLinks>
          </Footer>
        </DescriptionBlock>
        <MainBlock xs="12" md="7">
          <SignUpWrapper>
            <FormattedMessage {...messages.dontHaveAccount} />
            <Link to="/signup">{intl.formatMessage(messages.signUpLink)}</Link>
          </SignUpWrapper>
          <FormHeader>
            <FormattedMessage {...messages.header} />
          </FormHeader>
          <FormRow>
            <Col
              md={{
                size: 10,
                offset: 1,
              }}
              lg={{
                size: 8,
                offset: 2,
              }}
            >
              <DynamicForm
                formNS="forms"
                name="signin"
                theme="signin"
                models={['username', 'password']}
                modelsOptions={{
                  username: {
                    type: FORM_INPUT_TYPES.EMAIL,
                    labelHidden: true,
                    placeholder: intl.formatMessage(messages.emailPlaceholder),
                    noPadding: true,
                    fieldClassName: 'col-12',
                    validation: ['required'],
                  },
                  password: {
                    type: FORM_INPUT_TYPES.PASSWORD,
                    labelHidden: true,
                    placeholder: intl.formatMessage(
                      messages.passwordPlaceholder
                    ),
                    noPadding: true,
                    fieldClassName: 'col-12',
                    validation: ['required'],
                  },
                }}
                noSubmitButton
                onSubmit={onClickSubmit}
              >
                <ActionButton>
                  <ForgotPasswordLink to="/signin">
                    {intl.formatMessage(messages.forgotPasswordLink)}
                  </ForgotPasswordLink>
                  <SubmitButton
                    type="submit"
                    disabled={!form.$form.valid || form.$form.pending}
                  >
                    {form.$form.pending
                      ? intl.formatMessage(messages.buttonLoading)
                      : intl.formatMessage(messages.submitButton)}
                  </SubmitButton>
                </ActionButton>
              </DynamicForm>
            </Col>
          </FormRow>
        </MainBlock>
      </Row>
    </Container>
  )
}

SigninPage.propTypes = {
  form: PropTypes.object,
  intl: intlShape.isRequired,
  onClickSubmit: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
  form: state.forms.forms.signin,
})

const mapDispatchToProps = dispatch => ({
  onClickSubmit: () => dispatch(userSignInSubmit()),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

const withReducer = injectReducer({ key: 'signin', reducer })
const withSaga = injectSaga({ key: 'SigninPage', saga })

export default compose(
  withReducer,
  withSaga,
  withConnect,
  injectIntl
)(SigninPage)
