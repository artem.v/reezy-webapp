import styled from 'styled-components'
import { Col, Row } from 'reactstrap'
import { Link } from 'react-router-dom'

import signinBackground from '../../images/signin-bg.jpg'

export const DescriptionBlock = styled(Col)`
  @media only screen and (min-width: 770px) {
    height: 100vh;
  }
  background-image: url(${signinBackground});
  background-size: cover;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  padding: 60px 60px;
`

export const DescHeader = styled.h2`
  color: #fff;
  line-height: 2;
`

export const Footer = styled.div`
  margin-bottom: 20px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-self: stretch;
`

export const FooterLinks = styled.div`
  a {
    padding: 0 15px;
    color: #fff;
  }
`

export const MainBlock = styled(Col)`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 40px;
`

export const FormRow = styled(Row)`
  align-self: stretch;
`

export const SignUpWrapper = styled.div`
  margin: 0px 0 80px 0;
  align-self: flex-end;
  font-size: 1.1rem;
  font-weight: 500;

  a {
    padding-left: 10px;
    color: #f4516c;
  }

  @media only screen and (max-width: 768px) {
    margin-bottom: 40px;
  }
`

export const FormHeader = styled.h3`
  margin-bottom: 40px;

  @media only screen and (max-width: 768px) {
    margin-bottom: 30px;
  }
`

export const ActionButton = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin: 20px 0;
`

export const ForgotPasswordLink = styled(Link)`
  @media only screen and (min-width: 360px) {
    margin-left: 40px;
  }
  color: #aba9b4;
`
