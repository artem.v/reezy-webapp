/*
 * AccountForm Messages
 *
 * This contains all the text for the AccountForm component.
 */

import { defineMessages } from 'react-intl'
import { createIntlMessage } from 'common'

const SIGNUP_PAGE_ID = 'SignUpPage'

export default defineMessages({
  ...createIntlMessage(SIGNUP_PAGE_ID, 'settingProperty'),
  ...createIntlMessage(SIGNUP_PAGE_ID, 'alt'),
  ...createIntlMessage(SIGNUP_PAGE_ID, 'continue'),
  ...createIntlMessage(SIGNUP_PAGE_ID, 'address'),
  ...createIntlMessage(SIGNUP_PAGE_ID, 'rentAmount'),
  ...createIntlMessage(SIGNUP_PAGE_ID, 'billingCycle'),
  ...createIntlMessage(SIGNUP_PAGE_ID, 'billingCycle_DAILY'),
  ...createIntlMessage(SIGNUP_PAGE_ID, 'billingCycle_WEEKLY'),
  ...createIntlMessage(SIGNUP_PAGE_ID, 'billingCycle_BI_WEEKLY'),
  ...createIntlMessage(SIGNUP_PAGE_ID, 'billingCycle_MONTHLY'),
  ...createIntlMessage(SIGNUP_PAGE_ID, 'rentStartDate'),
  ...createIntlMessage(SIGNUP_PAGE_ID, 'tenantName'),
})
