/**
 *
 * Sign-up: Setting up Properties and Leases
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { FormattedMessage, injectIntl, intlShape } from 'react-intl'
import { createStructuredSelector } from 'reselect'
import { compose } from 'redux'
import camelCase from 'lodash/camelCase'

import {
  makeSelectSignupPropertyForm,
  makeSelectSignupStep,
  makeSelectSignupSteps,
} from '../../containers/SignupPage/selectors'

import {
  createPropertySubmit,
  setStep,
} from '../../containers/SignupPage/actions'

import SignupErrorContainer from '../SignupErrorContainer'
import DynamicForm from '../../components/Forms/DynamicForm'

import { FORM_INPUT_TYPES } from '../../components/Forms/constants'
import { BILLING_CYCLES } from '../../constants'

import messages from './messages'
import signupSetupPropertyIcon from '../../images/signup-setup-property-active.svg'

import {
  FormFooter,
  SubmitButton,
  FormContent,
  FormHeader,
  LabelDiv,
  IconDiv,
} from '../SignupPage/styled'

export class SignupSetupProperty extends React.Component {
  onSubmit = () => {
    this.props.createPropertySubmit()
  }

  backTask = () => {
    if (this.props.step - 1 >= 0) {
      this.props.setStep(this.props.step - 1)
    }
  }

  render() {
    const { intl, form } = this.props

    return (
      <React.Fragment>
        <FormHeader>
          <IconDiv>
            <img
              src={signupSetupPropertyIcon}
              alt={intl.formatMessage(messages.alt)}
            />
          </IconDiv>
          <LabelDiv>{intl.formatMessage(messages.settingProperty)}</LabelDiv>
        </FormHeader>
        <SignupErrorContainer />
        <FormContent>
          <DynamicForm
            formNS="forms"
            name="signupProperty"
            models={[
              'address',
              'rentAmount',
              'billingCycle',
              'rentStartDate',
              'tenantName',
            ]}
            modelsOptions={{
              address: {
                label: intl.formatMessage(messages.address),
                placeholder: intl.formatMessage(messages.address),
                noPadding: true,
                fieldClassName: 'col-12',
                validation: ['required'],
              },
              rentAmount: {
                label: intl.formatMessage(messages.rentAmount),
                placeholder: intl.formatMessage(messages.rentAmount),
                noPadding: true,
                fieldClassName: 'col-12 col-sm-7',
                validation: ['required'],
              },
              billingCycle: {
                type: FORM_INPUT_TYPES.SELECT,
                label: intl.formatMessage(messages.billingCycle),
                placeholder: intl.formatMessage(messages.billingCycle),
                noPadding: true,
                fieldClassName: 'col-12 col-sm-5',
                options: Object.keys(BILLING_CYCLES).map(billingCycle =>
                  intl.formatMessage(
                    messages[camelCase(`billingCycle_${billingCycle}`)]
                  )
                ),
                validation: ['required'],
              },
              rentStartDate: {
                type: FORM_INPUT_TYPES.DATE,
                label: intl.formatMessage(messages.rentStartDate),
                noPadding: true,
                fieldClassName: 'col-12',
                validation: ['required', 'date'],
              },
              tenantName: {
                label: intl.formatMessage(messages.tenantName),
                placeholder: intl.formatMessage(messages.tenantName),
                noPadding: true,
                fieldClassName: 'col-12',
                validation: ['required'],
              },
            }}
            noSubmitButton
            onSubmit={this.onSubmit}
          >
            <FormFooter>
              <SubmitButton
                type="submit"
                disabled={!form.$form.valid || form.$form.pending}
              >
                <FormattedMessage {...messages.continue} />
              </SubmitButton>
            </FormFooter>
          </DynamicForm>
        </FormContent>
      </React.Fragment>
    )
  }
}

SignupSetupProperty.propTypes = {
  form: PropTypes.object,
  intl: intlShape.isRequired,
  createPropertySubmit: PropTypes.func.isRequired,
  step: PropTypes.number,
  setStep: PropTypes.func.isRequired,
  steps: PropTypes.arrayOf(
    PropTypes.shape({
      completed: PropTypes.bool,
    })
  ),
}

const mapStateToProps = createStructuredSelector({
  form: makeSelectSignupPropertyForm(),
  step: makeSelectSignupStep(),
  steps: makeSelectSignupSteps(),
})

const mapDispatchToProps = {
  createPropertySubmit,
  setStep,
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

export default compose(
  withConnect,
  injectIntl
)(SignupSetupProperty)
