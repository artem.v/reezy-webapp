/**
 *
 * TransactionPage
 *
 */

import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { compose } from 'redux'
import { injectIntl, intlShape } from 'react-intl'
import { createStructuredSelector } from 'reselect'
import get from 'lodash/get'
import injectSaga from 'utils/injectSaga'
import injectReducer from 'utils/injectReducer'

import injectSession from '../CheckSessionHOC'

import TransactionsList from '../TransactionsList'
import SelectProperty from '../../components/SelectProperty'
import Header from '../Header'

import saga from './saga'
import reducer from './reducer'

import { fetchPropertiesList, selectProperty } from './actions'
import {
  Wrapper,
  Content,
  PageHeader,
  PageTitle,
} from '../../components/styled'

import messages from './messages'

import {
  makeSelectPropertiesList,
  makeSelectSelectedProperty,
} from './selectors'

class TransactionsPage extends React.Component {
  static propTypes = {
    properties: PropTypes.array,
    selectedProperty: PropTypes.number,
    fetchPropertiesList: PropTypes.func,
    selectProperty: PropTypes.func,
    match: PropTypes.shape({
      params: PropTypes.shape({
        id: PropTypes.string,
      }),
    }),
    intl: intlShape.isRequired,
  }

  componentDidMount() {
    const propertyId = +get(this.props.match, 'params.id')

    if (propertyId) {
      this.props.selectProperty(propertyId)
    }

    this.props.fetchPropertiesList()
  }

  render() {
    const { intl, properties, selectedProperty } = this.props

    return (
      <Wrapper>
        <Header />
        <Content>
          <PageHeader>
            <PageTitle>
              {intl.formatMessage(messages.selectTransactions)}
            </PageTitle>
            {!(properties && selectedProperty) ? null : (
              <SelectProperty
                properties={properties}
                onSelect={this.props.selectProperty}
                currentProperty={selectedProperty}
              />
            )}
          </PageHeader>
          {!selectedProperty ? null : (
            <TransactionsList propertyId={selectedProperty} />
          )}
        </Content>
      </Wrapper>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  properties: makeSelectPropertiesList(),
  property: makeSelectSelectedProperty(),
  selectedProperty: makeSelectSelectedProperty(),
})

const mapDispatchToProps = {
  fetchPropertiesList,
  selectProperty,
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

const withSaga = injectSaga({ key: 'transactionspage', saga })
const withReducer = injectReducer({ key: 'transactionspage', reducer })
const withSession = injectSession()

export default compose(
  withSaga,
  withReducer,
  withConnect,
  withSession,
  injectIntl
)(TransactionsPage)
