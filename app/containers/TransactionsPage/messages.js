/*
 * TransactionPage Messages
 *
 * This contains all the text for the TransactionPage component.
 */

import { defineMessages } from 'react-intl'
import { createIntlMessage } from 'common'

const PAGE_ID = 'transactionsPage'

export default defineMessages({
  ...createIntlMessage(PAGE_ID, 'selectTransactions'),
})
