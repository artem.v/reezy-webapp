/*
 *
 * TransactionsPage actions
 *
 */

export const SET_PROPERTY_ID = 'app/TransactionsPagePage/SET_PROPERTY_ID'
export const setPropertyId = propertyId => ({
  type: SET_PROPERTY_ID,
  propertyId,
})

export const FETCH_PROPERTIES_LIST =
  'app/TransactionsPagePage/FETCH_PROPERTIES_LIST'
export const fetchPropertiesList = () => ({
  type: FETCH_PROPERTIES_LIST,
})

export const FETCH_PROPERTIES_LIST_SUCCESS =
  'app/TransactionsPagePage/FETCH_PROPERTIES_LIST_SUCCESS'
export const fetchPropertiesListSuccess = list => ({
  type: FETCH_PROPERTIES_LIST_SUCCESS,
  list,
})

export const FETCH_PROPERTIES_LIST_FAILED =
  'app/TransactionsPagePage/FETCH_PROPERTIES_LIST_FAILED'
export const fetchPropertiesListFailed = id => ({
  type: FETCH_PROPERTIES_LIST_FAILED,
  id,
})

export const SELECT_PROPERTY = 'app/TransactionsPagePage/SELECT_PROPERTY'
export const selectProperty = id => ({
  type: SELECT_PROPERTY,
  id: parseInt(id, 10),
})
