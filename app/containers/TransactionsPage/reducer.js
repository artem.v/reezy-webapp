/*
 *
 * TransactionPage reducer
 *
 */

import Immutable from 'seamless-immutable'

import { FETCH_PROPERTIES_LIST_SUCCESS, SELECT_PROPERTY } from './actions'

export const initialState = Immutable({
  properties: [],
  selectedProperty: 0,
})

function transactionsPageReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_PROPERTIES_LIST_SUCCESS:
      return state.set('properties', action.list)
    case SELECT_PROPERTY:
      return state.set('selectedProperty', action.id).set('pending', true)
    default:
      return state
  }
}

export default transactionsPageReducer
