import { createSelector } from 'reselect'
import { initialState } from './reducer'

/**
 * Direct selector to the transactionsPage state domain
 */

const selectTransactionsPage = state => state.transactionspage || initialState

const makeSelectSelectedProperty = () =>
  createSelector(
    selectTransactionsPage,
    transactionsPageState => transactionsPageState.selectedProperty
  )

const makeSelectPropertiesList = () =>
  createSelector(
    selectTransactionsPage,
    transactionsPageState => transactionsPageState.properties
  )

export { makeSelectSelectedProperty, makeSelectPropertiesList }
