import { takeLatest, all, call, put, select } from 'redux-saga/effects'
import { push } from 'react-router-redux'
import get from 'lodash/get'
import {
  setPropertyId,
  SELECT_PROPERTY,
  fetchPropertiesListSuccess,
  fetchPropertiesListFailed,
  FETCH_PROPERTIES_LIST,
  selectProperty,
} from './actions'
import api from '../../api'

export function* fetchProperties() {
  const selectedProperty = yield select(
    state => state.transactionspage.selectedProperty
  )
  try {
    const response = yield call(api.getPropertiesList)

    if (response.ok) {
      if (!response.data.response.length) {
        yield put(push('/dashboard'))
      }

      const resBankAccounts = yield call(api.getBankAccounts)

      // If the user has bank accounts then move to the next step
      if (
        resBankAccounts.ok &&
        !get(resBankAccounts, 'data.bankAccounts', []).length
      ) {
        yield put(push('/signup'))
      }

      yield put(fetchPropertiesListSuccess(response.data.response))
      const firstProperty = response.data.response[0]

      if (!selectedProperty) {
        yield put(selectProperty(firstProperty.id))
      }
    } else {
      yield put(fetchPropertiesListFailed(response.problem))
    }
  } catch (error) {
    yield put(fetchPropertiesListFailed('Unexpected error'))
  }
}

export function* switchProperty({ id }) {
  yield put(setPropertyId(id))
}

export default function* defaultSaga() {
  yield all([
    yield takeLatest(SELECT_PROPERTY, switchProperty),
    yield takeLatest(FETCH_PROPERTIES_LIST, fetchProperties),
  ])
}
