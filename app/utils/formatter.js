const separatePrice = price => {
  if (!price || typeof price !== 'number') {
    return '0'
  }

  return price.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ',')
}

const validPrice = price => {
  if (!price || typeof price !== 'string') {
    return '0'
  }

  if (price.includes('-')) {
    return `-${price.replace('-', '')}`
  }

  return price
}
export default {
  separatePrice,
  validPrice,
}
