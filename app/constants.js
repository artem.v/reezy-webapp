import keyMirror from 'keymirror'

export const ERRORS = keyMirror({
  INCORRECT_CREDENTIALS: true,
  USER_EXISTS: true,
})

export const BILLING_CYCLES = keyMirror({
  // DAILY: true,
  // WEEKLY: true,
  // BI_WEEKLY: true,
  MONTHLY: true,
})

export const SIZES = {
  H1: '24px',
  H2: '18px',
  H3: '14px',
  REGULAR: '12px',
}

export const COLORS = {
  PRIMARY: '#5983f7',
  TEXT: '#858E93',
  SUCCESS: '#6FCF97',
  FAILED: '#E86066',
  BACKGROUND: '#f5f7fc',
  FROM: '#3EBB72',
  TO: '#3AE581',
  TITLE: '#082740',
  CHART_LINE: '#5C8AF7',
  WHITE: '#FFFFFF',
  GRAY: '#C4C4C4',
  INPUT_BACKGROUND: '#FBFBFB',
  LIGHT: '#f3f4f5',
  BUTTON_BACKGROUND: '#5983F7',
}

export const CATEGORIES = {
  1: 'rent',
  2: 'mortgage',
  3: 'repair',
  4: 'propertyTaxes',
  5: 'maintenanceFees',
}

export const PER_PAGE = 20

export const DATE_FORMAT = 'D/MM/YYYY'
export const API_DATE_FORMAT = 'YYYY-MM-DD'

export const TRANSACTIONS_FILTERS = keyMirror({
  RECOGNIZED: true,
  INCOME: true,
  EXPENSES: true,
  ALL: true,
})

export const TRANSACTIONS_SORTBY = keyMirror({
  DATE: true,
  DESCRIPTION: true,
  CATEGORY: true,
  AMOUNT: true,
})
