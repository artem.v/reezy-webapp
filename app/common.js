import { camelCase } from 'lodash'
import moment from 'moment'
import { BILLING_CYCLES } from './constants'

export const createIntlMessage = (component, id) => ({
  [camelCase(id)]: {
    id: `app.${component}.${id}`,
    defaultMessage: id,
  },
})

export const calculateNextPaymentDue = (rentStartDate, billingCycle) => {
  const rentedOnDay = new Date(rentStartDate).getDate()

  let payDate

  if (billingCycle === BILLING_CYCLES.MONTHLY) {
    payDate = moment().set('date', rentedOnDay)
    if (rentedOnDay < new Date().getDate()) {
      payDate = payDate.add(1, 'month')
    }
  }

  return payDate
}
