import { isEmpty } from 'validator'
import moment from 'moment'

const isEmail = val =>
  !(val && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(val))

export const generateValidations = ({ intl, validation, label }) => {
  const validators = {}
  const messages = {}

  if (validation.includes('required')) {
    validators.required = val => val && !isEmpty(val.toString())
    messages.required = intl.formatMessage(
      {
        id: 'app.validation.required',
        defaultMessage: 'This field is required',
      },
      { field: label && label.toLowerCase() }
    )
  }

  if (validation.includes('isEmail')) {
    validators.isEmail = val => isEmail(val)
    messages.isEmail = intl.formatMessage({
      id: 'app.validation.invalidEmail',
      defaultMessage: 'This has to be a valid email address',
    })
  }

  if (validation.includes('passwordLength')) {
    validators.minLength8 = val =>
      (val && val.length > 7 && val.length < 26) || !val
    messages.minLength8 = intl.formatMessage({
      id: 'app.validation.passwordLength',
      defaultMessage: 'Password has to be between 8 and 25 characters',
    })
  }

  if (validation.includes('number')) {
    validators.number = val => parseInt(val, 10) === val || !val
    messages.number = intl.formatMessage({
      id: 'app.validation.number',
      defaultMessage: 'This has to be a number',
    })
  }

  if (validation.includes('date')) {
    validators.date = val =>
      !val ||
      !/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/.test(val.toString()) ||
      moment(val).isValid()
    messages.date = intl.formatMessage(
      {
        id: 'validation.invalid',
        defaultMessage: `A valid {field} is required`,
      },
      {
        field: label.toLowerCase(),
      }
    )
  }

  return {
    validators,
    messages,
  }
}
