/* eslint-disable */
/* REMOVE WHEN THIS COMPONENT WILL BE USED */
import React from 'react'
import { shallow } from 'enzyme'

import UnitsControl from '../CustomControls/UnitsControl'

describe('<UnitsControl />', () => {
  it('should render when it mounts', () => {
    const renderedComponent = shallow(<UnitsControl />)

    expect(renderedComponent.length).toBe(1)
  })

  it('should trigger function when adding text to input', () => {
    const mockFn = jest.fn()
    const renderedComponent = shallow(<UnitsControl onChange={mockFn} />)
    const input = renderedComponent.find('input')
    input.simulate('change', { target: { value: '124' } })

    expect(mockFn).toHaveBeenCalledTimes(1)
  })

  it('should render value with units when value prop is defined', () => {
    const renderedComponent = shallow(<UnitsControl units="cm" />)

    expect(renderedComponent.find('.units-text').length).toBe(0)

    renderedComponent.setProps({ value: '123' })

    expect(renderedComponent.find('.units-text').length).toBe(1)
  })

  it('should render correct value and units', () => {
    const renderedComponent = shallow(<UnitsControl units="cm" value="123" />)
    const expectedUnit = renderedComponent.find('.units-text').props()
      .children[1]
    const expectedValue = renderedComponent.find('.units-text').props()
      .children[0].props.children

    expect(expectedUnit).toBe('cm')
    expect(expectedValue).toBe('123')
  })
})
