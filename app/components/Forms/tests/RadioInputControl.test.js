import React from 'react'
import { shallow } from 'enzyme'

import RadioInputControl from '../CustomControls/RadioInputControl'

describe('<RadioInputControl />', () => {
  it.skip('should render when it mounts', () => {
    const renderedComponent = shallow(<RadioInputControl />)

    expect(renderedComponent.length).toBe(1)
  })

  it('should trigger function when selecting a radio input', () => {
    const mockFn = jest.fn()
    const renderedComponent = shallow(
      <RadioInputControl onChange={mockFn} model="insuranceHistory" />
    )
    const input = renderedComponent.find('#input-insuranceHistory-yes')

    input.simulate('change', { target: { value: 'yes' } })

    expect(mockFn).toHaveBeenCalledTimes(1)
  })

  it.skip('should trigger function when selecting a radio input', () => {
    const mockFn = jest.fn()
    const renderedComponent = shallow(
      <RadioInputControl onChange={mockFn} model="insuranceHistory" />
    )
    const input = renderedComponent.find('#input-undefined-no')

    input.simulate('change', { target: { value: 'no' } })

    expect(mockFn).toHaveBeenCalledTimes(1)
  })
})
