import styled from 'styled-components'
import { Control, Errors } from 'react-redux-form'
import { COLORS, SIZES } from '../../constants'

import ErrorIcon from '../../images/icon_error.png'
import CompletedIcon from '../../images/icon_complete.png'
import downArrowIcon from '../../images/icon-dropdown-arrow.png'

export const themes = {
  default: {
    fontFamily: 'Roboto, sans-serif',
    inputBorderRadius: '0px',
    inputBorderBottom: `2px solid ${COLORS.TEXT}`,
    inputBorderBottomActive: `2px solid ${COLORS.PRIMARY}`,
  },
  signin: {
    fontFamily: 'Poppins, sans-serif',
    inputBorderRadius: '40px',
    inputBorderBottom: 'none',
  },
}

const Icons = [
  { key: 'icon_error', icon: ErrorIcon },
  { key: 'icon_complete', icon: CompletedIcon },
]

export const ErrorsStyled = styled(Errors)`
  margin: 0 auto;
  margin-bottom: 10px;
  color: ${COLORS.WHITE};
  font-size: 14px;
  background-color: ${COLORS.FAILED};
`

export const Label = styled.label`
  font-weight: 600;
  font-size: 14px;
  color: ${COLORS.TITLE};
  @media (max-width: 767px) {
    font-size: 12px;
  }
`

export const ControlStyled = styled(Control)`
  font-family: ${props => props.theme.fontFamily};
  border-radius: ${props => props.theme.inputBorderRadius || '0px'};
  border: none;
  border-bottom: ${props => props.theme.inputBorderBottom || 'none'};
  padding: 1.5rem 2.14rem;
  background-color: ${COLORS.INPUT_BACKGROUND};

  &:active {
    border-bottom: ${props => props.theme.inputBorderBottomActive || 'none'};
  }
`

export const ControlStyledIcon = ControlStyled.extend`
  font-family: ${props => props.theme.fontFamily};
  background: url(${props => Icons.find(icon => icon.key === props.icon).icon})
    no-repeat scroll 15px 15px;
  background-size: auto 20px;
  background-position: right;
  background-position-x: 95%;
`

export const DateControlWrapper = styled.div`
  display: flex;
  flex-direction: row;
  flex: 1;

  div:not(:first-child) {
    padding-left: 20px;
  }
`

export const SelectWrapper = styled.div`
  position: relative;
  width: 100%;

  &:after {
    content: '';
    width: 15px;
    height: 13px;
    top: 14px;
    right: 10px;
    position: absolute;
    pointer-events: none;
    background-image: url(${downArrowIcon});
    background-size: cover;
  }
`

export const Select = styled.select`
  appearance: none;
  &::-ms-expand {
    display: none;
  }

  font-family: ${props => props.theme.formFamily};
  border-radius: ${props => props.theme.inputBorderRadius || '0px'};
  border: none;
  border-bottom: ${props => props.theme.inputBorderBottom || 'none'};
  padding: 10px 20px;
  background-color: ${COLORS.INPUT_BACKGROUND};

  &:active {
    border-bottom: ${props => props.theme.inputBorderBottomActive || 'none'};
  }

  width: 100%;
`

export const Preamble = styled.div`
  font-size: 16px;
`

export const Description = styled.div`
  font-size: 15px;
`

export const InputComponentWrapper = styled.div`
  margin-top: ${props => (props.noPadding ? '3px' : '50px')};
`

export const ErrorsWrapper = styled.div`
  margin-top: ${props => (props.validation.length > 0 ? '9px' : '0px')};
  color: ${COLORS.FAILED};
  font-size: ${SIZES.H3};
`

export const FormGroup = styled.div`
  position: relative;
  margin-bottom: 20px;
`
