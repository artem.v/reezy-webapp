import React from 'react'
import PropTypes from 'prop-types'
import { Control, Errors } from 'react-redux-form'
import classnames from 'classnames'
import { injectIntl, intlShape } from 'react-intl'

import {
  Label,
  ControlStyled,
  ControlStyledIcon,
  ErrorsWrapper,
  FormGroup,
  Preamble,
  Description,
  InputComponentWrapper,
} from '../styled'

import DateControl from '../CustomControls/DateControl'
import SelectControl from '../CustomControls/SelectControl'
// import OptionsControl from '../CustomControls/OptionsControl'
// import PictureChoiceControl from '../CustomControls/PictureChoiceControl'
// import UsernameControl from '../CustomControls/UsernameControl'
// import LocationControl from '../CustomControls/LocationControl'

import { generateValidations } from '../common'
import { FORM_INPUT_TYPES } from '../constants'

import messages from './messages'

let ErrorWrapper = ({ children, className, intl }) => (
  <span className={className}>
    <span className="sr-only">{intl.formatMessage(messages.fieldError)}</span>
    {children}
  </span>
)

ErrorWrapper.propTypes = {
  children: PropTypes.array,
  className: PropTypes.string,
  intl: intlShape.isRequired,
}
ErrorWrapper = injectIntl(ErrorWrapper)

// const textInputErrorStyle = {
//   borderBottom: `2px solid ${COLORS.FAILED}`,
//   borderRadius: '2px',
//   fontFamily: 'Roboto',
// }
// const textInputCompletedStyle = {
//   borderBottom: `2px solid ${COLORS.SUCCESS}`,
//   borderRadius: '2px',
//   fontFamily: 'Roboto',
// }
// const selectStyle = {
//   height: '40px',
// }

const LabeledInput = props => {
  const {
    label,
    model,
    validation,
    type,
    disabled,
    placeholder,
    maxLength,
    minLength,
    parser,
    options,
    icon,
    preamble,
    description,
    noPadding,
  } = props
  const extraProps = {}
  const validators = generateValidations(props)

  if (validators.validators.required) {
    extraProps['aria-required'] = 'true'
  }

  let InputComponent

  if (type === FORM_INPUT_TYPES.TEXTAREA) {
    InputComponent = Control.textarea
  } else if (type === FORM_INPUT_TYPES.PASSWORD) {
    InputComponent = icon ? ControlStyledIcon : ControlStyled
    extraProps.type = 'password'
  } else if (type === FORM_INPUT_TYPES.EMAIL) {
    InputComponent = icon ? ControlStyledIcon : ControlStyled
    extraProps.type = 'email'
  } else if (type === FORM_INPUT_TYPES.DATE) {
    InputComponent = Control
    extraProps.component = DateControl
  } else if (type === FORM_INPUT_TYPES.SELECT) {
    InputComponent = Control
    extraProps.component = SelectControl
    // } else if (type === FORM_INPUT_TYPES.OPTIONS) {
    //   InputComponent = Control
    //   extraProps.onSelect = onSelect
    //   extraProps.component = OptionsControl
    // } else if (type === FORM_INPUT_TYPES.PICTURE_CHOICE) {
    //   InputComponent = Control
    //   extraProps.component = PictureChoiceControl
    //   extraProps.onSelect = onSelect
    // } else if (type === FORM_INPUT_TYPES.USERNAME) {
    //   InputComponent = Control
    //   extraProps.component = UsernameControl
    // } else if (type === FORM_INPUT_TYPES.LOCATION) {
    //   InputComponent = Control
    //   extraProps.component = LocationControl
    // } else if (type === FORM_INPUT_TYPES.SELECT) {
    //   InputComponent = Control.select
  } else {
    InputComponent = icon ? ControlStyledIcon : ControlStyled
    extraProps.type = 'text'
  }

  return (
    <FormGroup>
      {!preamble ? null : <Preamble>{preamble}</Preamble>}
      {!label ? null : (
        <Label
          htmlFor={`input-${model}`}
          className={classnames(`${model.toLowerCase()}-label`)}
        >
          {label}
        </Label>
      )}
      {!description ? null : <Description>{description}</Description>}
      <InputComponentWrapper noPadding={noPadding}>
        <InputComponent
          model={`.${model}`}
          className={classnames('form-control', `${model.toLowerCase()}-field`)}
          validators={validators.validators}
          disabled={disabled}
          placeholder={placeholder}
          id={`input-${model}`}
          maxLength={maxLength}
          minLength={minLength}
          parser={parser}
          options={options}
          icon={icon}
          {...extraProps}
        />
      </InputComponentWrapper>

      <ErrorsWrapper
        id="errorRegion"
        aria-atomic="true"
        aria-live="assertive"
        validation={validation}
      >
        <Errors
          className={classnames('errorText', 'error', model.toLowerCase())}
          model={`.${model}`}
          wrapper={ErrorWrapper}
          show="touched"
          messages={validators.messages}
        />
      </ErrorsWrapper>
    </FormGroup>
  )
}

LabeledInput.propTypes = {
  label: PropTypes.string,
  model: PropTypes.string.isRequired,
  validation: PropTypes.array,
  disabled: PropTypes.bool,
  placeholder: PropTypes.string,
  tooltip: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  type: PropTypes.oneOf(Object.keys(FORM_INPUT_TYPES)),
  className: PropTypes.string,
  intl: intlShape.isRequired,
  maxLength: PropTypes.number,
  minLength: PropTypes.number,
  parser: PropTypes.func,
  units: PropTypes.string,
  options: PropTypes.array,
  icon: PropTypes.string,
  preamble: PropTypes.bool,
  description: PropTypes.bool,
  onSelect: PropTypes.func,
  noPadding: PropTypes.bool,
}

LabeledInput.defaultProps = {
  disabled: false,
  style: {},
  type: FORM_INPUT_TYPES.TEXT,
}

export default injectIntl(LabeledInput)
