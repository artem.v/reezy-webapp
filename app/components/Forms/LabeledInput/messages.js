import { defineMessages } from 'react-intl'

export default defineMessages({
  fieldError: {
    id: 'form.field.error',
    defaultMessage: 'Error: ',
  },
})
