export const FORM_LAYOUTS = {
  ONE_COLUMN: 'ONE_COLUMN',
}

export const FORM_ELEMNTS_LAYOUT = {
  COLUMNS: 'COLUMNS',
  ROW: 'ROWS',
}

export const FORM_INPUT_TYPES = {
  TEXT: 'TEXT',
  PASSWORD: 'PASSWORD',
  EMAIL: 'EMAIL',
  TEXTAREA: 'TEXTAREA',
  RADIO: 'RADIO',
  CHECKBOX: 'CHECKBOX',
  DATE: 'DATE',
  LOCATION: 'LOCATION',
  SELECT: 'SELECT',
  OPTIONS: 'OPTIONS',
  PICTURE_CHOICE: 'PICTURE_CHOICE',
  USERNAME: 'USERNAME',
}
