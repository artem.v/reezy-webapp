import React from 'react'
import PropTypes from 'prop-types'
import { Control, Errors } from 'react-redux-form'
import classnames from 'classnames'
import { injectIntl, intlShape } from 'react-intl'
import styled from 'styled-components'

import { generateValidations } from '../common'
import { FORM_INPUT_TYPES } from '../constants'

import messages from './messages'

let ErrorWrapper = ({ children, className, intl }) => (
  <span className={className}>
    <span className="sr-only">{intl.formatMessage(messages.fieldError)}</span>
    {children}
  </span>
)

ErrorWrapper.propTypes = {
  children: PropTypes.array,
  className: PropTypes.string,
  intl: intlShape.isRequired,
}
ErrorWrapper = injectIntl(ErrorWrapper)

const Label = styled.label`
  font-size: 17px;
  padding-left: 15px;
`

const LabeledToggle = props => {
  const {
    label,
    model,
    style,
    type,
    className,
    disabled,
    placeholder,
    maxLength,
    minLength,
    parser,
    options,
    icon,
  } = props
  const extraProps = {}
  const validators = generateValidations(props)

  if (validators.validators.required) {
    extraProps['aria-required'] = 'true'
  }

  let InputComponent

  if (type === FORM_INPUT_TYPES.CHECKBOX) {
    InputComponent = Control.checkbox
  } else {
    InputComponent = Control.checkbox
  }

  return (
    <div
      className={classnames('form-group', className)}
      style={{ position: 'relative' }}
    >
      <InputComponent
        model={`.${model}`}
        className={classnames(`${model.toLowerCase()}-field`)}
        validators={validators.validators}
        disabled={disabled}
        placeholder={placeholder}
        style={style}
        id={`input-${model}`}
        maxLength={maxLength}
        minLength={minLength}
        parser={parser}
        options={options}
        icon={icon}
        {...extraProps}
      />

      {!label ? null : (
        <Label
          htmlFor={`input-${model}`}
          className={classnames(`${model.toLowerCase()}-label`)}
        >
          {label}
        </Label>
      )}

      <div id="errorRegion" aria-atomic="true" aria-live="assertive">
        <Errors
          className={classnames('errorText', 'error', model.toLowerCase())}
          model={`.${model}`}
          wrapper={ErrorWrapper}
          show="touched"
          messages={validators.messages}
        />
      </div>
    </div>
  )
}

LabeledToggle.propTypes = {
  label: PropTypes.string,
  model: PropTypes.string.isRequired,
  validation: PropTypes.array,
  disabled: PropTypes.bool,
  placeholder: PropTypes.string,
  tooltip: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  style: PropTypes.object,
  type: PropTypes.oneOf(Object.keys(FORM_INPUT_TYPES)),
  className: PropTypes.string,
  icon: PropTypes.object,
  intl: intlShape.isRequired,
  maxLength: PropTypes.number,
  minLength: PropTypes.number,
  parser: PropTypes.func,
  units: PropTypes.string,
  options: PropTypes.array,
}

LabeledToggle.defaultProps = {
  disabled: false,
  style: {},
  type: FORM_INPUT_TYPES.TEXT,
}

export default injectIntl(LabeledToggle)
