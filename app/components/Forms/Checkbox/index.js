import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { COLORS } from '../../../constants'
import CheckedIcon from '../../../images/checkedIconWhite.png'
const Wrapper = styled.label`
  position: relative;
  display: inline-flex;
  overflow: hidden;
`
const Input = styled.input`
  position: absolute;
  right: -10000px;
  visibility: hidden;
  &:checked {
    & + span {
      background-color: ${COLORS.PRIMARY};
      background-image: url(${props => props.icon || CheckedIcon});
      background-position: center;
      background-repeat: no-repeat;
    }
  }
`
const Box = styled.span`
  width: 18px;
  height: 18px;
  border-radius: 1px;
  border: 1px solid ${COLORS.PRIMARY};
`
export default class Checkbox extends Component {
  static propTypes = {
    onChange: PropTypes.func,
    checked: PropTypes.bool,
    icon: PropTypes.string,
  }

  onChange = () => this.props.onChange(!this.props.checked)

  render() {
    return (
      <Wrapper>
        <Input
          type="checkbox"
          onChange={this.onChange}
          checked={this.props.checked}
          icon={this.props.icon}
        />
        <Box />
      </Wrapper>
    )
  }
}
