/* eslint-disable react/no-array-index-key */
import React from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import { padStart } from 'lodash'
import { injectIntl, intlShape } from 'react-intl'

import { SelectWrapper, Select, DateControlWrapper } from '../styled'
import messages from '../DynamicForm/messages'

class DateControl extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      month: 0,
      day: 0,
      year: 0,
    }
  }

  onChange = (name, e) => {
    this.setState(
      {
        [name]: e.target.value,
      },
      () => {
        const formattedDate = `${padStart(this.state.year, 4, '0')}-${padStart(
          this.state.month,
          2,
          '0'
        )}-${padStart(this.state.day, 2, '0')}`

        this.props.onChange(formattedDate)
      }
    )
  }

  render() {
    const { intl, onFocus, onBlur } = this.props

    return (
      <DateControlWrapper onFocus={onFocus} onBlur={onBlur}>
        <SelectWrapper>
          <Select onChange={value => this.onChange('month', value)}>
            <option>{intl.formatMessage(messages.datePlaceholderMonth)}</option>
            {[...Array(12)].map((_, i) => {
              const month = moment().month(i)
              return (
                <option
                  key={`month${month.format('MMMM')}`}
                  value={month.format('MM')}
                >
                  {month.format('MMMM')}
                </option>
              )
            })}
          </Select>
        </SelectWrapper>
        <SelectWrapper>
          <Select onChange={value => this.onChange('day', value)}>
            <option>{intl.formatMessage(messages.datePlaceholderDay)}</option>
            {[...Array(31)].map((_, i) => (
              <option key={`day${i}`} value={i + 1}>
                {i + 1}
              </option>
            ))}
          </Select>
        </SelectWrapper>
        <SelectWrapper>
          <Select onChange={value => this.onChange('year', value)}>
            <option>{intl.formatMessage(messages.datePlaceholderYear)}</option>
            {[...Array(100)].map((_, i) => (
              <option key={`year${i}`} value={moment().year() - i}>
                {moment().year() - i}
              </option>
            ))}
          </Select>
        </SelectWrapper>
      </DateControlWrapper>
    )
  }
}

DateControl.propTypes = {
  onChange: PropTypes.func.isRequired,
  onFocus: PropTypes.func.isRequired,
  onBlur: PropTypes.func.isRequired,
  intl: intlShape.isRequired,
}

export default injectIntl(DateControl)
