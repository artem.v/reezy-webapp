import React from 'react'
import PropTypes from 'prop-types'
// import classNames from 'classNames';
import { Control } from 'react-redux-form'

const RadioInputControl = props => {
  const { style, model, options } = props

  const customStyle = {
    margin: '1em',
  }

  const radioStyle = Object.assign({}, style, customStyle)

  return (
    <div className="radio-input-component">
      {options &&
        options.length &&
        options.map(option => (
          <label htmlFor={`input-${model}-${option.value}`} style={radioStyle}>
            <Control.radio
              model=".insuranceHistory"
              name={`input-${model}-${option.value}`}
              id={`input-${model}-${option.value}`}
              value={option.value}
              style={radioStyle}
              validators={props.validators}
              // {...props}
            />
            {option.text}
          </label>
        ))}
    </div>
  )
}

RadioInputControl.propTypes = {
  // className: PropTypes.string,
  style: PropTypes.object,
  model: PropTypes.string.isRequired,
  validators: PropTypes.func.isRequired,
  options: PropTypes.array.isRequired,
}

RadioInputControl.defaultProps = {
  // disabled: false,
  style: {},
}

export default RadioInputControl
