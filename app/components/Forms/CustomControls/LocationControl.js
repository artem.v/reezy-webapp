/* eslint-disable */
/* REMOVE WHEN THIS COMPONENT WILL BE USED */
import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
// import PlacesAutocomplete, { geocodeByAddress } from 'react-places-autocomplete'
function PlacesAutocomplete() {
  return null
}
const Input = styled.input`
  border: 0.6px solid #e8e8e8;
  padding: 15px;
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.1);
  border-radius: 2.4px;
  background-color: #fff;
  width: 100%;
  max-width: 800px;
`

const AutocompleteDropdown = styled.div`
  background-color: #fff;
  width: 100%;
  max-width: 800px;
  margin: 0 auto;
  border: 0.6px solid #e8e8e8;
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.1);
  text-align: left;
`

const AutocompleteSuggestion = styled.div`
  padding: 10px;
`

class LocationControl extends React.Component {
  constructor(props) {
    super(props)
    this.onChange = this.onChange.bind(this)
    this.handleSelect = this.handleSelect.bind(this)
  }

  onChange(address) {
    this.props.onChange({
      formatted: address,
    })
  }

  async handleSelect(address) {
    // const res = await geocodeByAddress(address)
    const res = [[{}]]
    const addrComponents = res[0].address_components
    const streetNumber = addrComponents.find(comp =>
      comp.types.includes('street_number')
    )
    const street = addrComponents.find(comp => comp.types.includes('route'))
    const city = addrComponents.find(comp => comp.types.includes('locality'))
    const zipcode = addrComponents.find(comp =>
      comp.types.includes('postal_code')
    )
    const state = addrComponents.find(comp =>
      comp.types.includes('administrative_area_level_1')
    )
    const country = addrComponents.find(comp => comp.types.includes('country'))

    const addressObject = {
      street: `${streetNumber && streetNumber.long_name} ${street &&
        street.long_name}`,
      city: city && city.long_name,
      zipcode: zipcode && zipcode.long_name,
      state: state && state.long_name,
      country: country && country.long_name,
      formatted: address,
    }

    this.props.onChange(addressObject)
  }

  render() {
    const { value } = this.props
    return (
      <PlacesAutocomplete
        value={value && value.get('formatted')}
        onChange={this.onChange}
        onSelect={this.handleSelect}
      >
        {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
          <div>
            <Input
              {...getInputProps({
                placeholder: 'Search Places ...',
                className: 'location-search-input',
              })}
            />
            <AutocompleteDropdown>
              {loading && <div>Loading...</div>}
              {suggestions.map(suggestion => {
                const className = suggestion.active
                  ? 'suggestion-item--active'
                  : 'suggestion-item'
                // inline style for demonstration purpose
                const style = suggestion.active
                  ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                  : { backgroundColor: '#ffffff', cursor: 'pointer' }
                return (
                  <AutocompleteSuggestion
                    {...getSuggestionItemProps(suggestion, {
                      className,
                      style,
                    })}
                  >
                    <span>{suggestion.description}</span>
                  </AutocompleteSuggestion>
                )
              })}
            </AutocompleteDropdown>
          </div>
        )}
      </PlacesAutocomplete>
    )
  }
}

LocationControl.propTypes = {
  options: PropTypes.array.isRequired,
  validators: PropTypes.func,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
}

LocationControl.defaultProps = {}

export default LocationControl
