/* eslint-disable react/no-array-index-key */
import React from 'react'
import PropTypes from 'prop-types'
import { injectIntl, intlShape } from 'react-intl'

import { SelectWrapper, Select } from '../styled'
import messages from '../DynamicForm/messages'

class SelectControl extends React.Component {
  onChange = e => {
    this.props.onChange(e.target.value)
  }

  render() {
    const { options, intl } = this.props
    return (
      <div>
        <SelectWrapper>
          <Select onChange={this.onChange}>
            <option value="0">
              {intl.formatMessage(messages.selectPlaceholder)}
            </option>
            {options.map((option, i) => (
              <option key={`option${i}`} value={i + 1}>
                {option}
              </option>
            ))}
          </Select>
        </SelectWrapper>
      </div>
    )
  }
}

SelectControl.propTypes = {
  options: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired,
  intl: intlShape.isRequired,
}

SelectControl.defaultProps = {}

export default injectIntl(SelectControl)
