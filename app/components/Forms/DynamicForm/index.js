import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { injectIntl, intlShape } from 'react-intl'
import { Form, Errors } from 'react-redux-form'
import styled, { ThemeProvider } from 'styled-components'

import { COLORS } from '../../../constants'
import { FORM_LAYOUTS } from '../constants'
import ModelGroups from './components/ModelGroups'
import SingleGroup from './components/SingleGroup'
import { themes } from '../styled'

const ErrorsStyled = styled(Errors)`
  margin: 0 auto;
  color: ${COLORS.WHITE};
  font-size: 14px;
  background-color: ${COLORS.FAILED};
`

export class DynamicFormComponent extends React.Component {
  constructor(props) {
    super(props)
    this.moveToNextStep = this.moveToNextStep.bind(this)
  }

  getModelOption(model, option, defaultValue = false) {
    if (
      this.props.modelsOptions[model] &&
      typeof this.props.modelsOptions[model][option] !== 'undefined'
    ) {
      return this.props.modelsOptions[model][option]
    }

    return defaultValue
  }

  moveToNextStep() {
    this.setState(state => ({
      currentGroup: state.currentGroup + 1,
    }))
  }

  CustomErrorComponent = ({ children }) => (
    <span>
      {this.props.intl.formatMessage({
        id: `app.errors.${children}`,
        defaultMessage: children,
      })}
    </span>
  )

  render() {
    const {
      formNS,
      name,
      onSubmit,
      formValidators,
      errorMessages,
      modelGroups,
      theme,
      children,
    } = this.props

    const styledTheme = themes[theme]

    return (
      <ThemeProvider theme={styledTheme}>
        <Form
          model={`${formNS}.${name}`}
          onSubmit={onSubmit}
          validators={formValidators || {}}
          autoComplete="off"
        >
          <div className="row">
            <div className="col-sm-12 mb-3">
              <div aria-atomic="true" aria-live="assertive">
                <ErrorsStyled
                  className="alert alert-danger"
                  model={`${formNS}.${name}`}
                  messages={errorMessages || {}}
                  component={this.CustomErrorComponent}
                />
              </div>
            </div>
          </div>

          {modelGroups && modelGroups.length > 0 ? (
            <ModelGroups {...this.props} />
          ) : (
            <SingleGroup {...this.props}>{children}</SingleGroup>
          )}
        </Form>
      </ThemeProvider>
    )
  }
}

DynamicFormComponent.propTypes = {
  formNS: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  models: PropTypes.array.isRequired,
  modelsOptions: PropTypes.object,
  modelGroups: PropTypes.array,
  submitButtonText: PropTypes.string,
  layout: PropTypes.oneOf([FORM_LAYOUTS.ONE_COLUMN, FORM_LAYOUTS.TWO_COLUMN]),
  theme: PropTypes.string,
  form: PropTypes.object.isRequired,
  intl: intlShape.isRequired,
  formValidators: PropTypes.object,
  errorMessages: PropTypes.object,
  children: PropTypes.object,
  noSubmitButton: PropTypes.bool,
  onSubmit: PropTypes.func,
}

DynamicFormComponent.defaultProps = {
  layout: FORM_LAYOUTS.ONE_COLUMN,
  formValidators: {},
  noSubmitButton: false,
  theme: 'default',
}

const mapStateToProps = (state, props) => ({
  form: state[props.formNS].forms[props.name],
})

export default connect(mapStateToProps)(injectIntl(DynamicFormComponent))
