import React, { Component } from 'react'
import { injectIntl, intlShape } from 'react-intl'
import PropTypes from 'prop-types'
import { FORM_INPUT_TYPES } from '../../constants'

import LabeledInput from '../../LabeledInput'
import LabeledToggle from '../../LabeledToggle'

class ModelComponents extends Component {
  getModelOption(model, option, defaultValue = false) {
    if (
      this.props.modelsOptions[model] &&
      typeof this.props.modelsOptions[model][option] !== 'undefined'
    ) {
      return this.props.modelsOptions[model][option]
    }

    return defaultValue
  }

  getInputField(type) {
    switch (type) {
      case FORM_INPUT_TYPES.CHECKBOX:
        return LabeledToggle
      default:
        return LabeledInput
    }
  }

  render() {
    const { models, intl } = this.props
    return (
      <div className="row justify-content-around">
        {models.map(model => {
          const fieldType = this.getModelOption(
            model,
            'type',
            FORM_INPUT_TYPES.TEXT
          )
          const label = this.getModelOption(
            model,
            'label',
            intl.formatMessage({
              id: `form.label.${model}`,
              defaultMessage: model,
            })
          )
          const labelHidden = this.getModelOption(model, 'labelHidden')
          const preamble = this.getModelOption(model, 'preamble')
          const description = this.getModelOption(model, 'description')
          const placeholder = this.getModelOption(model, 'placeholder', '')
          const InputField = this.getInputField(fieldType)
          const parser = this.getModelOption(model, 'parser', val => val)
          const icon = this.getModelOption(model, 'icon', null)
          const options = this.getModelOption(model, 'options', [])
          const onSelect = this.getModelOption(model, 'onSelect', () => {})
          const noPadding = this.getModelOption(model, 'noPadding')
          const fieldClassName = this.getModelOption(
            model,
            'fieldClassName',
            ''
          )

          return (
            <div className={fieldClassName} key={`form-model-${model}`}>
              <InputField
                label={labelHidden ? null : label}
                preamble={preamble}
                description={description}
                model={model}
                validation={this.getModelOption(model, 'validation', [])}
                disabled={this.getModelOption(model, 'disabled')}
                placeholder={placeholder}
                tooltip={this.getModelOption(model, 'tooltip', null)}
                style={this.getModelOption(model, 'style', {})}
                type={fieldType}
                maxLength={this.getModelOption(model, 'maxlength', null)}
                minLength={this.getModelOption(model, 'minlength', null)}
                parser={parser}
                icon={icon}
                options={options}
                onSelect={onSelect}
                noPadding={noPadding}
              />
            </div>
          )
        })}
      </div>
    )
  }
}

ModelComponents.propTypes = {
  modelsOptions: PropTypes.object,
  models: PropTypes.array,
  intl: intlShape.isRequired,
}

ModelComponents.defaultProps = {
  modelsOptions: {},
}

export default injectIntl(ModelComponents)
