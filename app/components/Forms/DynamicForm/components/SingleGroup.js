import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { injectIntl, intlShape } from 'react-intl'
import classnames from 'classnames'

import ModelComponents from './ModelComponents'

import messages from '../messages'
import { SubmitButton } from '../../../styled'

class SingleGroup extends Component {
  getModelOption(model, option, defaultValue = false) {
    if (
      this.props.modelsOptions[model] &&
      typeof this.props.modelsOptions[model][option] !== 'undefined'
    ) {
      return this.props.modelsOptions[model][option]
    }

    return defaultValue
  }

  render() {
    const { intl, form, models, modelsOptions, children } = this.props
    const submitButtonText =
      this.props.submitButtonText || intl.formatMessage(messages.buttonSubmit)

    return (
      <div className="row">
        <div className="col-sm-12" style={{ overflow: 'hidden' }}>
          <ModelComponents models={models} modelsOptions={modelsOptions} />

          {!this.props.noSubmitButton && (
            <div className="row">
              <div className="col-sm-12">
                <SubmitButton
                  type="submit"
                  disabled={!form.$form.valid || form.$form.pending}
                  id="btn-submit"
                  className={classnames(
                    'btn btn-primary btn-block',
                    `${submitButtonText.toLowerCase()}-btn`
                  )}
                  style={{ maxWidth: '800px', margin: '30px auto' }}
                >
                  {form.$form.pending
                    ? intl.formatMessage(messages.buttonLoading)
                    : submitButtonText}
                </SubmitButton>
              </div>
            </div>
          )}

          {children}
        </div>
      </div>
    )
  }
}

SingleGroup.propTypes = {
  modelsOptions: PropTypes.object,
  models: PropTypes.array,
  form: PropTypes.object,
  submitButtonText: PropTypes.string,
  noSubmitButton: PropTypes.bool,
  children: PropTypes.object,
  intl: intlShape.isRequired,
}

export default injectIntl(SingleGroup)
