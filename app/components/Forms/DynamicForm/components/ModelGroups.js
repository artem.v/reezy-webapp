/* eslint-disable */
import React, { Component } from 'react'
import { injectIntl } from 'react-intl'
import styled from 'styled-components'
// import transition from 'styled-transition-group'
import classnames from 'classnames'

import ModelComponents from './ModelComponents'

import messages from '../messages'

// const AnimatedSteps = transition.div.attrs({
//   unmountOnExit: true,
//   timeout: 200,
// })`
//   &:enter {
//     transform: translate(0%, 100%);
//   }
//   &:enter-active {
//     transform: translate(0%);
//     transition: transform 200ms linear;
//   }
//   &:exit {
//     transform: translate(0%);
//   }
//   &:exit-active {
//     transform: translate(0%, -100%);
//     transition: transform 200ms linear;
//   }
// `

const SubmitButton = styled.button`
  font-size: 12px;
  background-color: #0290ff;
  border-radius: 1.8px;
  padding: 15px;
  letter-spacing: 0.46px;
  margin-top: 30px;
`

class ModelGroups extends Component {
  getModelOption(model, option, defaultValue = false) {
    if (
      this.props.modelsOptions[model] &&
      typeof this.props.modelsOptions[model][option] !== 'undefined'
    ) {
      return this.props.modelsOptions[model][option]
    }
    return defaultValue
  }

  render() {
    const { modelGroups, intl, currentStep, form, onStepChange } = this.props
    const submitButtonText =
      this.props.submitButtonText || intl.formatMessage(messages.buttonSubmit)
    const stepFilterEnabled =
      modelGroups && currentStep < modelGroups.length - 1
    let disableButton = false
    if (modelGroups && stepFilterEnabled) {
      disableButton = this.getModelOption(
        modelGroups[currentStep][0],
        'disableButton'
      )
    }
    return (
      <div className="row">
        {modelGroups.map((group, i) => (
          <div className="col-sm-12" style={{ overflow: 'hidden' }}>
            <AnimatedSteps in={i === currentStep} unmountOnExit>
              <ModelComponents
                models={group}
                modelsOptions={this.props.modelsOptions}
              />

              {stepFilterEnabled && !disableButton ? (
                <div className="row">
                  <div className="col-sm-12">
                    <SubmitButton
                      type="button"
                      className={classnames(
                        'btn btn-primary btn-block',
                        `${submitButtonText.toLowerCase()}-btn`
                      )}
                      style={{ maxWidth: '800px', margin: '30px auto' }}
                      onClick={() => onStepChange(currentStep + 1)}
                    >
                      {form.$form.pending
                        ? intl.formatMessage(messages.buttonLoading)
                        : submitButtonText}
                    </SubmitButton>
                  </div>
                </div>
              ) : null}

              {!stepFilterEnabled &&
                !this.props.noSubmitButton && (
                <div className="row">
                  <div className="col-sm-12">
                    <SubmitButton
                      type="submit"
                      disabled={!form.$form.valid || form.$form.pending}
                      id="btn-submit"
                      className={classnames(
                        'btn btn-primary btn-block',
                        `${submitButtonText.toLowerCase()}-btn`
                      )}
                      style={{ maxWidth: '800px', margin: '30px auto' }}
                    >
                      {form.$form.pending
                        ? intl.formatMessage(messages.buttonLoading)
                        : submitButtonText}
                    </SubmitButton>
                  </div>
                </div>
                )}
            </AnimatedSteps>
          </div>
        ))}
      </div>
    )
  }
}

export default injectIntl(ModelGroups)
