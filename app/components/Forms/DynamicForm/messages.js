import { defineMessages } from 'react-intl'
import { createIntlMessage } from 'common'

const PAGE_ID = 'Forms'

export default defineMessages({
  ...createIntlMessage(PAGE_ID, 'buttonSubmit'),
  ...createIntlMessage(PAGE_ID, 'buttonLoading'),
  ...createIntlMessage(PAGE_ID, 'datePlaceholderDay'),
  ...createIntlMessage(PAGE_ID, 'datePlaceholderMonth'),
  ...createIntlMessage(PAGE_ID, 'datePlaceholderYear'),
  ...createIntlMessage(PAGE_ID, 'selectPlaceholder'),
})
