import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { COLORS } from '../../constants'
import CheckIcon from '../../images/checkedIconWhite.svg'
import CancelIcon from '../../images/cancelIconWhite.svg'

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 24px;
  min-width: 24px;
  height: 24px;
  margin-right: 20px;
  border-radius: 1px;
  background-color: ${props => (props.value ? COLORS.SUCCESS : COLORS.FAILED)};
  @media (max-width: 767px) {
    margin-right: 3px;

    width: 20px;
    height: 20px;
    min-width: 20px;
  }
`

const Icon = styled.img`
  width: 15px;
  min-width: 15px;
  height: 15px;
`

export default class CheckedBlock extends PureComponent {
  static propTypes = {
    value: PropTypes.bool,
  }

  render() {
    return (
      <Wrapper value={this.props.value}>
        <Icon src={this.props.value ? CheckIcon : CancelIcon} />
      </Wrapper>
    )
  }
}
