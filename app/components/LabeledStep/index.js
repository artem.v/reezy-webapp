/**
 *
 * LabeledStep
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { injectIntl, intlShape } from 'react-intl'

import messages from './messages'
import { COLORS } from '../../constants'
import CheckIcon from '../../images/signup-check.svg'

const StepLabel = styled.div`
  width: 220px;
  min-width: 220px;
  height: 50px;
  display: inline-block;
  border-radius: 2px;
  background-color: ${COLORS.WHITE};
  color: ${COLORS.TEXT};
  &.label-type-1 {
    color: ${COLORS.TITLE};
    background-color: ${COLORS.WHITE};
    border: 1px solid ${COLORS.PRIMARY};
  }
  &.label-type-2 {
    background: linear-gradient(
      90deg,
      ${COLORS.SUCCESS} 0%,
      ${COLORS.PRIMARY} 100%
    );
    color: ${COLORS.WHITE};
  }
  &.label-type-3 {
    color: ${COLORS.WHITE};
    background-color: ${COLORS.SUCCESS};
  }
`
const LabelContainer = styled.div`
  position: relative;
  height: 100%;
  display: flex;
  justify-content: space-around;
  align-items: center;
`
const LabelDiv = styled.div`
  font-size: 12px;
  min-width: 150px;
  text-align: left;
`
const IconDiv = styled.div`
  img {
    height: 30px;

    &.icon-type-0 {
      fill: ${COLORS.TEXT};
      filter: invert(0.5);
    }
    &.icon-type-1 {
      fill: ${COLORS.PRIMARY};
    }
  }
`
const CheckDiv = styled.div`
  position: absolute;
  height: 100%;
  width: 18px;
  justify-content: center;
  align-items: center;
  display: flex;
  right: 16px;
  input {
    width: 16px;
    height: 16px;
  }
`

const LabeledStep = props => {
  const { type, label, icon, intl } = props
  const labelTypeClass = `label-type-${type}`
  const iconTypeClass = `icon-type-${type}`

  return (
    <StepLabel className={labelTypeClass}>
      <LabelContainer onClick={props.chooseStep}>
        {type < 2 && (
          <IconDiv>
            <img
              src={icon}
              className={iconTypeClass}
              alt={intl.formatMessage(messages.alt)}
            />
          </IconDiv>
        )}
        <LabelDiv>{label}</LabelDiv>
        <CheckDiv>
          {type > 1 && (
            <img src={CheckIcon} alt={intl.formatMessage(messages.alt)} />
          )}
        </CheckDiv>
      </LabelContainer>
    </StepLabel>
  )
}

LabeledStep.propTypes = {
  intl: intlShape.isRequired,
  type: PropTypes.number,
  label: PropTypes.string.isRequired,
  icon: PropTypes.string,
  chooseStep: PropTypes.func,
}

LabeledStep.defaultProps = {
  type: 0,
}

export default injectIntl(LabeledStep)
