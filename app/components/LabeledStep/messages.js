/*
 * LabeledStep Messages
 *
 * This contains all the text for the LabeledStep component.
 */

import { defineMessages } from 'react-intl'
import { createIntlMessage } from 'common'

const SIGNUP_PAGE_ID = 'SignUpPage'

export default defineMessages({
  ...createIntlMessage(SIGNUP_PAGE_ID, 'alt'),
})
