import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import moment from 'moment'
import { DATE_FORMAT } from './constants'
import { COLORS } from '../../constants'
// import CancelIcon from '../../images/cancel_icon.svg'
const Wrapper = styled.div`
  display: flex;
  position: relative;
`
const Input = styled.input`
  border-radius: 2px;

  border: 1px solid ${props => (props.error ? COLORS.FAILED : COLORS.TEXT)};
  padding: 3px 40px 3px 10px;
  position: relative;
  color: ${COLORS.TEXT};
  height: 30px;
  width: ${props => `${props.width}px` || '100%'};
  @media (max-width: 767px) {
    max-width: 170px;
  }
`

const OptionsWrapper = styled.ul`
  display: flex;
  position: absolute;
  right: 0;
  justify-content: space-between;
  top: 32px;
  padding: 20px;
  width: 100%;
  margin: 0;
  box-shadow: 2px 2px 7px rgba(8, 39, 64, 0.1);
  z-index: 3;
  background-color: ${COLORS.WHITE};
  min-width: 400px;
  @media (max-width: 767px) {
    padding: 20px 7px;
    min-width: 320;
  }
`

const TimeInput = styled(Input)`
  max-width: 170px;
`

export const Square = styled.span`
  width: 30px;
  height: 30px;
  top: 0;
  right: 0;
  background-color: ${COLORS.PRIMARY};
  position: absolute;
  border-radius: 2px;
`
export default class DateTimePicker extends React.PureComponent {
  static propTypes = {
    dateStart: PropTypes.string,
    dateEnd: PropTypes.string,
    onStartDateChange: PropTypes.func,
    onEndDateChange: PropTypes.func,
  }
  static defaultProps = {
    dateStart: '',
    dateEnd: '',
  }
  constructor(props) {
    super(props)
    this.wrapper = React.createRef()
  }

  state = {
    isOpen: false,
    startDate: null,
    endDate: null,
    startDateError: null,
    endDateError: null,
  }

  openList = () => {
    this.setState({ isOpen: true })
    window.addEventListener('click', this.closeList)
  }

  validateDate = date => moment(date, DATE_FORMAT, true).isValid()

  closeList = event => {
    const wrapper = this.wrapper.current

    if (wrapper && !wrapper.contains(event.target)) {
      this.setState({ isOpen: false })
      window.removeEventListener('click', this.closeList)
      if (this.state.endDate && this.validateDate(this.state.endDate)) {
        this.props.onEndDateChange(this.state.endDate)
      }

      if (this.state.startDate && this.validateDate(this.state.startDate)) {
        this.props.onStartDateChange(this.state.startDate)
      }
    }
  }

  changeStartDate = event => {
    this.setState({
      startDate: event.target.value,
      startDateError: !this.validateDate(event.target.value),
    })
  }
  changeEndDate = event => {
    this.setState({
      endDate: event.target.value,
      endDateError: !this.validateDate(event.target.value),
    })
  }

  render() {
    return (
      <Wrapper innerRef={this.wrapper}>
        <Input
          onFocus={this.openList}
          onChange={() => {}}
          error={this.state.startDateError || this.state.endDateError}
          onBlur={this.closeList}
          value={`${this.props.dateStart}-${this.props.dateEnd}`}
        />
        {this.state.isOpen ? (
          <OptionsWrapper>
            <TimeInput
              defaultValue={this.props.dateStart}
              value={this.state.startDate}
              onChange={this.changeStartDate}
              error={this.state.startDateError}
            />
            <TimeInput
              defaultValue={this.props.dateEnd}
              value={this.state.endDate}
              onChange={this.changeEndDate}
              error={this.state.endDateError}
            />
          </OptionsWrapper>
        ) : null}
        <Square />
      </Wrapper>
    )
  }
}
