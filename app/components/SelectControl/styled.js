import styled from 'styled-components'

import { COLORS } from '../../constants'
import ConfirmIcon from '../../images/icon_complete.png'
import CancelIcon from '../../images/cancel_icon.svg'

export const Wrapper = styled.div`
  display: flex;
  max-width: 195px;
  position: relative;
  align-items: center;
  justify-content: space-between;
`
export const Input = styled.input`
  border-radius: 2px;
  border: 1px solid ${COLORS.LIGHT};
  padding: 3px 40px 3px 10px;
  position: relative;
  color: ${COLORS.TEXT};
  text-overflow: ellipsis;
  height: 30px;
  width: ${props => (props.width ? `${props.width}px` : '100%')};
  @media (max-width: 767px) {
    max-width: 170px;
  }
`

export const SelectedValueInput = styled(Input)`
  border: 1px solid ${COLORS.PRIMARY};
  color: ${COLORS.PRIMARY};
  padding: 3px 10px;
  text-align: center;
  background-color: ${COLORS.WHITE};
  text-overflow: ellipsis;
  cursor: pointer;
  &:hover {
    background-color: ${COLORS.PRIMARY};
    color: ${COLORS.WHITE};
  }
`

export const SelectedWrapper = styled.div`
  border-radius: 2px;
  padding: 3px 40px 3px 10px;
  position: relative;
  text-overflow: ellipsis;
  display: flex;
  align-items: center;
  justify-content: center;

  background-color: ${COLORS.LIGHT};
  border-color: ${COLORS.LIGHT};
  color: ${COLORS.TEXT};
  text-align: center;
  height: 30px;
  width: ${props => (props.width ? `${props.width}px` : '200px')};
  @media (max-width: 767px) {
    max-width: 170px;
  }
`
export const OptionsWrapper = styled.ul`
  flex-direction: column;
  position: absolute;
  right: 0;
  top: 30px;
  padding: 0;
  width: 100%;
  margin: 0;
  box-shadow: 2px 2px 7px rgba(8, 39, 64, 0.1);
  z-index: 3;
`
export const Option = styled.li`
  width: 100%;
  display: flex;
  cursor: pointer;
  align-items: center;
  justify-content: flex-start;
  padding: 5px;
  background-color: ${COLORS.WHITE};
  border-bottom: 1px solid ${COLORS.LIGHT};
`

export const Confirm = styled.img.attrs({
  src: ConfirmIcon,
})`
  width: 20px;
  min-width: 20px;
  height: 15px;
  margin: 0 15px;
  cursor: pointer;
  margin-top: 7px;
`

export const Cancel = styled.img.attrs({
  src: CancelIcon,
})`
  width: 10px;
  min-width: 10px;
  height: 10px;
  position: absolute;
  right: 10px;
  top: 10px;
  z-index: 1;
  cursor: pointer;
`

export const TableRowLabel = styled.h3`
  font-size: 14px;
  line-height: 17px;
  color: ${COLORS.TEXT};
  padding: 0 5px 0 0;
  margin: 0;
  width: 100%;
  display: flex;
  flex-grow: ${props => props.grow || 1};
  @media (max-width: 767px) {
    order: ${props => props.mobileOrder || 'initial'};
    padding: 0 0 0 0;
  }
`

export const Square = styled.span`
  width: 30px;
  height: 30px;
  top: 0;
  right: 0;
  background-color: ${COLORS.PRIMARY};
  position: absolute;
  border-radius: 2px;
`
