import React from 'react'
import PropTypes from 'prop-types'
import { compose } from 'redux'
import { injectIntl, intlShape } from 'react-intl'
import messages from '../../containers/TransactionsPage/messages'

import {
  Cancel,
  Confirm,
  Input,
  Option,
  OptionsWrapper,
  SelectedValueInput,
  SelectedWrapper,
  Square,
  TableRowLabel,
  Wrapper,
} from './styled'

class SelectControl extends React.PureComponent {
  static propTypes = {
    options: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
        label: PropTypes.string,
      })
    ),
    confirm: PropTypes.func.isRequired,
    intl: intlShape.isRequired,
    selected: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    suggestedSelected: PropTypes.number,
    placeholder: PropTypes.string,
    width: PropTypes.number,
    mod: PropTypes.shape({
      withConfirmation: PropTypes.bool,
    }),
  }

  static defaultProps = {
    selected: '',
    mod: {},
    placeholder: 'Select',
  }

  constructor(props) {
    super(props)
    this.wrapper = React.createRef()
  }

  state = {
    isOpen: false,
    selected: {},
    confirmed: true,
  }

  select = option => this.setState({ selected: option })

  confirm = () => {
    this.props.confirm(this.state.selected.id)
    this.setState({ selected: {}, isOpen: false, confirmed: true })
    window.removeEventListener('click', this.closeList)
  }

  confirmSuggested = id => {
    this.props.confirm(id)
    this.setState({ selected: {}, isOpen: false, confirmed: true })
  }

  openList = () => {
    this.setState({ isOpen: true, confirmed: false })
    window.addEventListener('click', this.closeList)
  }

  closeList = event => {
    const wrapper = this.wrapper.current

    if (
      window.document.contains(event.target) &&
      wrapper &&
      !wrapper.contains(event.target)
    ) {
      this.setState({ isOpen: false })
    }
  }

  cancel = () => {
    this.setState({ selected: {}, isOpen: false, confirmed: true })
  }

  void = () => {}

  renderSelectedOptions = () => (
    <React.Fragment>
      <Confirm onClick={this.confirm} />
      <SelectedWrapper width={this.props.width}>
        {this.state.selected.label}
      </SelectedWrapper>
      <Cancel onClick={this.cancel} />
    </React.Fragment>
  )

  renderOption = option => (
    <Option
      onClick={
        this.props.mod.withConfirmation
          ? () => this.select(option)
          : () => this.props.confirm(option)
      }
      key={option.id}
    >
      {option.label}
    </Option>
  )

  render() {
    const selected = this.props.options.find(
      option => option.id === this.props.selected
    )
    let content = null

    if (this.props.mod.withConfirmation) {
      const suggestedSelected = this.props.options.find(
        option => option.id === this.props.suggestedSelected
      )

      if (this.state.confirmed && selected && !this.state.isOpen && !content) {
        content = (
          <React.Fragment>
            <SelectedWrapper width={this.props.width}>
              {selected.label}
            </SelectedWrapper>
            <Cancel onClick={this.openList} />
          </React.Fragment>
        )
      }

      if (
        this.state.confirmed &&
        !selected &&
        suggestedSelected &&
        !this.state.isOpen &&
        !content
      ) {
        content = (
          <React.Fragment>
            <TableRowLabel>
              {this.props.intl.formatMessage(messages.seemsLike)}
            </TableRowLabel>
            <SelectedValueInput
              onChange={this.void}
              value={`+ ${suggestedSelected.label}`}
              onFocus={() => this.confirmSuggested(+suggestedSelected.id)}
            />
          </React.Fragment>
        )
      }
    }

    if (
      this.props.mod.withConfirmation &&
      Object.entries(this.state.selected).length &&
      !content
    ) {
      content = this.renderSelectedOptions()
    }

    if (!content) {
      content = (
        <React.Fragment>
          <Input
            width={this.props.width}
            placeholder={this.props.placeholder}
            onFocus={this.openList}
            onBlur={this.closeList}
            onChange={this.void}
            value={(selected && selected.label) || ''}
          />
          {this.state.isOpen ? (
            <OptionsWrapper>
              {this.props.options.map(this.renderOption)}
            </OptionsWrapper>
          ) : null}
          <Square />
        </React.Fragment>
      )
    }

    return <Wrapper innerRef={this.wrapper}>{content}</Wrapper>
  }
}

export default compose(injectIntl)(SelectControl)
