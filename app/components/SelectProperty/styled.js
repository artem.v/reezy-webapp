import styled from 'styled-components'
import { COLORS } from '../../constants'
export const Wrapper = styled.div`
  position: relative;
  display: flex;
  align-items: center;
`

export const SelectWrapper = styled.div`
  position: relative;
  display: flex;
  cursor: pointer;
  align-items: center;
`
export const OptionsList = styled.ul`
  margin: 0;
  padding: 0;
  display: flex;
  background-color: ${COLORS.WHITE};
  border-radius: 2px;
  box-shadow: 2px 2px 9px rgba(95, 95, 95, 0.209239);
  flex-direction: column;
  position: absolute;
  top: 35px;
  z-index: 2;
  right: 0;
  & > :last-child {
    border-bottom: none;
  }
  @media (max-width: 767px) {
    left: 0;
    right: initial;
  }
`

export const Option = styled.li`
  display: flex;
  padding: 7px 13px;
  color: ${COLORS.TEXT};
  min-width: 250px;
  cursor: pointer;
  border-bottom: 1px solid ${COLORS.BACKGROUND};
  &.active {
    color: ${COLORS.PRIMARY};
  }
  &:hover {
    color: ${COLORS.PRIMARY};
  }
`
export const PageTitle = styled.h1`
  font-size: 24px;
  line-height: 29px;
  font-weight: 600;
  color: ${COLORS.TITLE};
  padding-right: 10px;
  margin: 0;
  @media (max-width: 767px) {
    font-size: 18px;
    line-height: 30px;
  }
`

export const PageTitleLight = styled(PageTitle)`
  color: ${COLORS.TEXT};
`
export const Arrow = styled.span`
  width: 0;
  height: 0;
  border-left: 6px solid transparent;
  border-right: 6px solid transparent;

  border-top: 6px solid;
  display: flex;
  border-top-color: ${COLORS.PRIMARY};
`
