import React from 'react'
import PropTypes from 'prop-types'

import {
  Wrapper,
  Option,
  OptionsList,
  Arrow,
  SelectWrapper,
  PageTitleLight,
} from './styled'

export default class SelectProperty extends React.Component {
  static propTypes = {
    properties: PropTypes.array,
    onSelect: PropTypes.func,
    currentProperty: PropTypes.number,
  }

  state = {
    isOpen: false,
  }

  selectProperty = id => {
    this.props.onSelect(id)
    this.setState({ isOpen: false })
  }

  toggleList = () => {
    if (this.props.properties.length === 1) {
      return false
    }

    return this.setState(state => ({ isOpen: !state.isOpen }))
  }

  renderOption = property => (
    <Option key={property.id} onClick={() => this.selectProperty(property.id)}>
      {property.address}
    </Option>
  )

  render() {
    const selected = this.props.properties.find(
      property => +property.id === +this.props.currentProperty
    )

    return (
      <Wrapper>
        <SelectWrapper onClick={this.toggleList}>
          <PageTitleLight>{selected ? selected.address : ''}</PageTitleLight>
          {this.props.properties.length > 1 ? <Arrow /> : null}
        </SelectWrapper>

        {this.state.isOpen ? (
          <OptionsList>
            {this.props.properties.map(this.renderOption)}
          </OptionsList>
        ) : null}
      </Wrapper>
    )
  }
}
