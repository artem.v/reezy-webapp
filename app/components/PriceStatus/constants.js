import { COLORS } from '../../constants'

const STATUS_COLORS = {
  0: COLORS.TEXT,
  1: COLORS.SUCCESS,
  2: COLORS.FAILED,
}

export { STATUS_COLORS }
