import React, { PureComponent } from 'react'
import formatter from 'utils/formatter'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { STATUS_COLORS } from './constants'
import { COLORS } from '../../constants'

const PriceStatusWrapper = styled.div`
  display: inline-flex;
  align-items: center;
  justify-content: center;
`
const PriceStatus = styled.span`
  font-weight: 600;
  margin-left: 4px;
  font-size: 14px;
  line-height: 18px;
  color: ${props => STATUS_COLORS[props.status] || COLORS.text};
`

const PriceStatusArrow = styled.span`
  width: 0;
  height: 0;
  border-left: 6px solid transparent;
  border-right: 6px solid transparent;

  border-top: 6px solid;
  display: ${props => (props.status ? 'flex' : 'none')};
  border-top-color: ${props => STATUS_COLORS[props.status] || COLORS.text};
  transform: rotate(${props => (props.status === 1 ? '180deg' : '0')});
`

export default class PriceStatusComponent extends PureComponent {
  static propTypes = {
    value: PropTypes.number,
  }

  getPriceStatus = price => {
    if (price === 0) {
      return 0
    }

    if (price > 0) {
      return 1
    }

    return 2
  }

  render() {
    const status = this.getPriceStatus(this.props.value)
    return (
      <PriceStatusWrapper>
        <PriceStatusArrow status={status} />
        <PriceStatus status={status}>
          {`$${formatter.separatePrice(this.props.value).replace('-', '')}`}
        </PriceStatus>
      </PriceStatusWrapper>
    )
  }
}
