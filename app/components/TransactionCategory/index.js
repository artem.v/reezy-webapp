import React from 'react'
import PropTypes from 'prop-types'
import { injectIntl, intlShape } from 'react-intl'
import messages from '../../containers/TransactionsList/messages'

import {
  Cancel,
  Input,
  Option,
  OptionsWrapper,
  SelectedValueInput,
  SelectedWrapper,
  Square,
  TableRowLabel,
  Wrapper,
} from './styled'

class TransactionCategory extends React.PureComponent {
  static propTypes = {
    options: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
        label: PropTypes.string,
      })
    ),
    onSelectCategory: PropTypes.func.isRequired,
    intl: intlShape.isRequired,
    selected: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    suggestedSelected: PropTypes.number,
    placeholder: PropTypes.string,
    width: PropTypes.number,
  }

  static defaultProps = {
    selected: '',
    placeholder: 'Select',
  }

  constructor(props) {
    super(props)
    this.wrapper = React.createRef()
  }

  state = {
    isOpen: false,
  }

  selectCategory = option => () => {
    this.props.onSelectCategory(option.id)
    window.removeEventListener('click', this.closeList)
  }

  removeCategory = () => {
    this.props.onSelectCategory(null)
  }

  confirmSuggested = id => {
    this.props.onSelectCategory(id)
  }

  openList = () => {
    this.setState({ isOpen: true })
    window.addEventListener('click', this.closeList)
  }

  closeList = event => {
    const wrapper = this.wrapper.current

    if (
      window.document.contains(event.target) &&
      wrapper &&
      !wrapper.contains(event.target)
    ) {
      this.setState({ isOpen: false })
    }
  }

  void = () => {}

  renderSelectedLabel = selectedCategory => (
    <React.Fragment>
      <SelectedWrapper width={this.props.width}>
        {selectedCategory.label}
      </SelectedWrapper>
      <Cancel onClick={this.removeCategory} />
    </React.Fragment>
  )

  renderSelectOptions = () => (
    <React.Fragment>
      <Input
        width={this.props.width}
        placeholder={this.props.placeholder}
        onFocus={this.openList}
        onBlur={this.closeList}
        onChange={this.void}
        value=""
      />
      {this.state.isOpen ? (
        <OptionsWrapper>
          {this.props.options.map(option => (
            <Option key={option.id} onClick={this.selectCategory(option)}>
              {option.label}
            </Option>
          ))}
        </OptionsWrapper>
      ) : null}
      <Square />
    </React.Fragment>
  )

  renderSuggestedCategory = suggestedCategory => (
    <React.Fragment>
      <TableRowLabel>
        {this.props.intl.formatMessage(messages.seemsLike)}
      </TableRowLabel>
      <SelectedValueInput
        onChange={this.void}
        value={`+ ${suggestedCategory.label}`}
        onFocus={() => this.confirmSuggested(+suggestedCategory.id)}
      />
    </React.Fragment>
  )

  render() {
    const selectedCategory =
      this.props.selected &&
      this.props.options.find(option => option.id === this.props.selected)

    const suggestedCategory =
      this.props.suggestedSelected &&
      this.props.options.find(
        option => option.id === this.props.suggestedSelected
      )

    let content = null

    if (selectedCategory) {
      content = this.renderSelectedLabel(selectedCategory)
    } else if (suggestedCategory) {
      content = this.renderSuggestedCategory(suggestedCategory)
    } else {
      content = this.renderSelectOptions()
    }

    return <Wrapper innerRef={this.wrapper}>{content}</Wrapper>
  }
}

export default injectIntl(TransactionCategory)
