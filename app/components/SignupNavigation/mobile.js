/**
 *
 * SignupMobileSteps
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { COLORS } from '../../constants'

const StepBar = styled.div`
  width: 100%;
  display: inline-block;
  align-items: center;
`
const StepDiv = styled.div`
  width: ${props => props.width};
  height: 5px;
  margin: 0 0.5%;
  display: inline-block;
  background: ${COLORS.GRAY};
  border-radius: 2px;
  &.step-active {
    background: ${COLORS.PRIMARY};
  }
  &.step-completed {
    background: ${COLORS.SUCCESS};
  }
`

class SignupMobileSteps extends React.Component {
  chooseStep = step => {
    this.props.chooseStep(step)
  }

  getStepStatus = index => {
    const { step } = this.props

    if (step === index) {
      return 'step-active'
    } else if (step > index) {
      return 'step-completed'
    }

    return 'step-normal'
  }
  getStepWidth = index => {
    const { step } = this.props

    if (step === index) {
      return `${(17 + (5 - step) * 10).toString()}%`
    } else if (step > index) {
      return '17%'
    }

    return '7%'
  }

  createSteps = () => {
    const steps = []
    for (let i = 0; i < 5; i += 1) {
      steps.push(
        <StepDiv
          onClick={() => {
            this.chooseStep(i)
          }}
          key={i}
          className={this.getStepStatus(i)}
          width={this.getStepWidth(i)}
        />
      )
    }
    return steps
  }

  render() {
    return <StepBar>{this.createSteps()}</StepBar>
  }
}

SignupMobileSteps.propTypes = {
  step: PropTypes.number,
  chooseStep: PropTypes.func,
}

SignupMobileSteps.defaultProps = {
  step: 0,
}

export default SignupMobileSteps
