/*
 * SignupSteps Messages
 *
 * This contains all the text for the SignupSteps component.
 */

import { defineMessages } from 'react-intl'
import { createIntlMessage } from 'common'

const SIGNUP_PAGE_ID = 'SignUpPage'

export default defineMessages({
  ...createIntlMessage(SIGNUP_PAGE_ID, 'creatingAccount'),
  ...createIntlMessage(SIGNUP_PAGE_ID, 'connectingBank'),
  ...createIntlMessage(SIGNUP_PAGE_ID, 'settingProperty'),
  ...createIntlMessage(SIGNUP_PAGE_ID, 'matchingTransactions'),
  ...createIntlMessage(SIGNUP_PAGE_ID, 'readyUse'),
  ...createIntlMessage(SIGNUP_PAGE_ID, 'alt'),
})
