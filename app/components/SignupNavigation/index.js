/**
 *
 * SignupSteps
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { injectIntl, intlShape } from 'react-intl'

import LabeledStep from '../LabeledStep'

import signupCreateAccountIcon from '../../images/signup-create-account.svg'
import signupConnectingIcon from '../../images/signup-connecting.svg'
import signupSetupPropertyIcon from '../../images/signup-setup-property.svg'
import signupMatchingIcon from '../../images/signup-matching.svg'
import signupReadyIcon from '../../images/signup-ready.svg'
import lineIcon from '../../images/signup-line.svg'
import arrowIcon from '../../images/signup-arrow.svg'

import messages from './messages'

const StepBar = styled.div`
  width: 1180px;
  display: inline-block;
  align-items: center;
  padding: 40px 0;
  @media only screen and (max-width: 770px) {
    padding: 20px 0;
  }
`
const StepArrow = styled.div`
  width: 18px;
  height: 100%;
  justify-content: center;
  align-items: center;
  display: inline-block;
`

class SignupSteps extends React.Component {
  renderArrow = ind => {
    const { step, intl } = this.props
    const v = step - ind

    if (v === 1) {
      return <img src={arrowIcon} alt={intl.formatMessage(messages.alt)} />
    } else if (v > 1) {
      return <img src={lineIcon} alt={intl.formatMessage(messages.alt)} />
    }

    return ''
  }

  chooseStep = step => {
    this.props.chooseStep(step)
  }

  getType = index => {
    const { step } = this.props
    const d = step - index + 1

    if (d > 3) {
      return 3
    } else if (d < 0) {
      return 0
    }

    return d
  }

  createSteps = () => {
    const { intl } = this.props
    const icons = [
      signupCreateAccountIcon,
      signupConnectingIcon,
      signupSetupPropertyIcon,
      signupMatchingIcon,
      signupReadyIcon,
    ]
    const labels = [
      intl.formatMessage(messages.creatingAccount),
      intl.formatMessage(messages.connectingBank),
      intl.formatMessage(messages.settingProperty),
      intl.formatMessage(messages.matchingTransactions),
      intl.formatMessage(messages.readyUse),
    ]
    const steps = []
    for (let i = 0; i < 4; i += 1) {
      steps.push(
        <React.Fragment key={`step-${i}`}>
          <LabeledStep
            chooseStep={() => {
              this.chooseStep(i)
            }}
            type={this.getType(i)}
            label={labels[i]}
            icon={icons[i]}
          />
          <StepArrow>{this.renderArrow(i)}</StepArrow>
        </React.Fragment>
      )
    }
    steps.push(
      <LabeledStep
        chooseStep={() => {
          this.chooseStep(4)
        }}
        key="step-4"
        type={this.getType(4)}
        label={labels[4]}
        icon={icons[4]}
      />
    )
    return steps
  }

  render() {
    return <StepBar>{this.createSteps()}</StepBar>
  }
}

SignupSteps.propTypes = {
  intl: intlShape.isRequired,
  step: PropTypes.number,
  chooseStep: PropTypes.func,
}

SignupSteps.defaultProps = {
  step: 0,
}

export default injectIntl(SignupSteps)
