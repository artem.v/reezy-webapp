import styled from 'styled-components'
import { COLORS } from '../../constants'

export const SubmitButton = styled.button`
  background-color: #5867dd;
  border-color: #5867dd;
  color: #ffffff;
  box-shadow: 0px 5px 10px 2px rgba(88, 103, 221, 0.36);
  border-radius: 60px;
  padding: 1.3rem 3.43rem;

  &:active {
    background-color: #2e40d4;
    border-color: #293ccc;
    outline: none;
  }

  &:disabled {
    opacity: 0.65;
  }
`

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  background-color: ${COLORS.BACKGROUND};
  font-family: PT Sans, sans-serif;
  min-height: 100vh;
`
export const Content = styled.div`
  padding: 0 40px 20px;
  max-width: 1270px;
  margin: 0px auto 0;
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  @media (max-width: 767px) {
    padding: 20px 18px 20px;
  }
`

export const PageHeader = styled.header`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  margin: 40px 0;
  @media (max-width: 767px) {
    margin: 0 0 22px;
    flex-direction: column;
    align-items: flex-start;
  }
`
export const PageTitle = styled.h1`
  font-size: 24px;
  line-height: 29px;
  font-weight: 600;
  color: ${COLORS.TITLE};
  padding-right: 10px;
  margin: 0;
  @media (max-width: 767px) {
    font-size: 18px;
    line-height: 30px;
  }
`
