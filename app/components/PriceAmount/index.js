import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import formatter from 'utils/formatter'
import styled from 'styled-components'
import { STATUS_COLORS } from './constants'
import { COLORS } from '../../constants'
const PriceStatusWrapper = styled.div`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  background-color: ${props => STATUS_COLORS[props.status] || COLORS.text};
  color: ${props => (STATUS_COLORS[props.status] ? COLORS.WHITE : COLORS.text)};
  font-size: 14px;
  font-weight: 600;
  line-height: normal;
  padding: 6px 15px;
  border-radius: 2px;
`
export default class PriceAmount extends PureComponent {
  static propTypes = {
    value: PropTypes.number,
  }

  render() {
    let status = 0

    if (this.props.value < 0) {
      status = 2
    }

    if (this.props.value > 0) {
      status = 1
    }

    return (
      <PriceStatusWrapper status={status}>
        {formatter.validPrice(`$${formatter.separatePrice(this.props.value)}`)}
      </PriceStatusWrapper>
    )
  }
}
