/* eslint-disable react-intl/string-is-marked-for-translation */
import React from 'react'
import { storiesOf } from '@storybook/react'
import {
  LoadingRow,
  Row,
  LoadingBlock,
  SmallLoadingBlock,
} from '../app/containers/TransactionsList/styled'

import createIcon from '../app/images/createIcon.svg'

import {
  Modal,
  FormHeader,
  IconDiv,
  LabelDiv,
  FormContent,
  ModalLabel,
  ModalBackdrop,
  ModalWrapper,
  ModalInput,
  CreatePropertyButton,
  CloseModal,
} from '../app/containers/PropertiesPage/styled'

import '../app/fontawesome'

storiesOf('Table Loading', module).add('Animation', () => (
  <LoadingRow>
    <Row>
      <LoadingBlock height="14px" />
      <SmallLoadingBlock height="14px" />
    </Row>
    <Row>
      <LoadingBlock height="30px" />
      <SmallLoadingBlock height="30px" />
    </Row>
  </LoadingRow>
))

storiesOf('Modals', module).add('Modal', () => (
  <ModalWrapper>
    <ModalBackdrop />
    <Modal>
      <CloseModal />
      <FormHeader>
        <IconDiv>
          <img src={createIcon} alt="Alt text" />
        </IconDiv>
        <LabelDiv>Title</LabelDiv>
      </FormHeader>
      <FormContent>
        <ModalLabel> Address </ModalLabel>
        <ModalInput value="Address" onChange={() => {}} />
        <CreatePropertyButton> Create </CreatePropertyButton>
      </FormContent>
    </Modal>
  </ModalWrapper>
))
