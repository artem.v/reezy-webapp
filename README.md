# Reezy Web

The frontend application is based on [react-boilerplate](https://github.com/react-boilerplate/react-boilerplate).

## Project core dependencies

### Tools 

- [Node.js](https://nodejs.org/en/) (recommended version is **8.11.3**)
- [Yarn](https://yarnpkg.com/en/docs/install#mac-stable) (recommended version is **1.7.0**)

### Node Libraries

- [react.js](https://reactjs.org/) - JavaScript Framework 
- [redux](https://github.com/reduxjs/redux) - State management library
- [redux-saga](https://github.com/redux-saga/redux-saga) - Side effect management for Redux (we don't use [thunk](https://github.com/reduxjs/redux-thunk))
- [seamless-immutable](https://github.com/rtfeldman/seamless-immutable) - Immutable data structures because immutability is good
- [apisauce](https://github.com/infinitered/apisauce) - HTTP client based on [Axios](https://github.com/axios/axios), used to call our API
- [styled-components](https://github.com/styled-components/styled-components) - For DOM styling instead of SASS
- [react-redux-form](https://github.com/davidkpiano/react-redux-form) - Redux-connected forms
- [reactstrap](https://reactstrap.github.io/) - React Bootstrap 4 components
- [react-i18next](https://github.com/i18next/react-i18next) - Internationalization

Most of the libraries that are used in the project had come out-of-box with react-boilerplate, however, [Immutable.js](https://github.com/facebook/immutable-js) library was replaced with [Seamless-immuttable](https://github.com/rtfeldman/seamless-immutable) as the latter offers better backward-compatibility with normal JS Arrays and Objects.


## Code style requirements

The code follows the standard [ECMAScript 2017](https://medium.freecodecamp.org/here-are-examples-of-everything-new-in-ecmascript-2016-2017-and-2018-d52fa3b5a70e). 

[ESLint](https://eslint.org/) is configured with rules (file `.eslint.js`) to catch most problems with code style, so make sure ESLint is installed globally (`npm i -g eslint`) on your machine and configured to work with your IDE, in this way you should see blocks of code highlighted if something is wrong with it.

## How to run the app

### Install node dependencies

*Run inside the project's folder*

```
yarn install
```

### Run the dev server
```
yarn start
```

**NOTE:** To enable mocked API responses, create `.env` file and set `MOCKS=true`, then restart the app for it to take effect.

## Generate new containers and components

The project includes a convenient tool to generate `components` and `containers` that came with react boilerplate.

Generator's command:
```
yarn generate
```

